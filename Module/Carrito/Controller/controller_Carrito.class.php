<?php
include_once($_SERVER['DOCUMENT_ROOT']."/productos/Module/Carrito/Model/DAOcarrito.php");
@session_start();

switch ($_GET['op']) {
	case 'listCarrito':
		//print_r("estic en listCarrito");
		//exit;
		if (isset($_GET["carrito"]) && $_GET["carrito"] == true) {
			if ((isset($_POST['datosCarrito']))) {
				$jsondata = array();
                $prodJson = json_decode($_POST["datosCarrito"], true);
                //print_r($prodJson);
                //exit;
                $jsondata['redirect'] = "index.php?module=Carrito&view=resultCarrito";
                @$_SESSION['listCarrito'] = $prodJson;
                $jsondata['datos'] = $prodJson;
                echo json_encode($jsondata);
                exit;
			}
		}

		///////// arrepleguem dades per a la llista del carrito i distruim la sessio per a que no torne al response ////////
    	if (isset($_GET["load"]) && $_GET["load"] == true){
            if (isset($_SESSION['listCarrito'])) {
                $jsondata['datos'] = @$_SESSION['listCarrito'];
                unset( $_SESSION['listCarrito'] );

            }    
            
            echo json_encode(@$jsondata);
            exit;
        }
    
	break;
    case 'pedido':
        //print_r("estic en pedido");
        //exit;
        if (isset($_GET["pedido"]) && $_GET["pedido"] == true){
            if ((isset($_POST['datosPedido']))) {
                //echo json_encode($_POST['datosPedido']);
                //exit;
                $jsondata = array();
                $pedido = json_decode($_POST["datosPedido"], true);
                //echo json_encode($pedido);
                //exit;
                
                ////////// comprobacion si esta logueado /////////////
                if ((@$_SESSION['login_admin'] == 1) || (@$_SESSION['login_client'] == 2)) {
                    try{
                        $rdo = DAOcarrito::insertPedido($pedido);
                        $rdo1 = DAOcarrito::pedido($_SESSION['name']);
                        
                    }catch(Exception $e){
                        header('HTTP/1.0 400 Bad error');
                        exit;
                    }
                    
                    if ($rdo1){
                        foreach ($rdo1 as $value) {
                            $precioTotal=$value['SUM(precio*cantidad)'];

                        }

                        try {
                            $rdo2 = DAOcarrito::insertPrecioTotal($precioTotal);
                        } catch (Exception $e) {
                            header('HTTP/1.0 400 Bad error');
                            exit;
                        }

                        if ($rdo2) {
                            $jsondata['success']=true;

                        }else{
                            header('HTTP/1.0 400 Bad error');
                            exit; 
                        }
                    }else{
                        header('HTTP/1.0 400 Bad error');
                        exit;
                    }

                    echo json_encode($jsondata);
                    exit;
                }else{
                    $jsondata['noUsu'] = "Registrese antes";
                    header('HTTP/1.0 400 Bad error');
                    echo json_encode($jsondata);
                    exit;
                }
                
            }
        }
    
    break;
    
}

