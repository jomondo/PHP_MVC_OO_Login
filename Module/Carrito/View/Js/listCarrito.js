function load_carrito() {
    $.get("Module/Carrito/Controller/controller_Carrito.class.php?op=listCarrito&load=true", function (data) {
        //alert(data);
        var json = JSON.parse(data);
        //pintaremos los prod que teniamos si recargamos la pagina
        if (json === null) {
            var carrito = datosCarrito();
            //si no hay productos agregados
            if (carrito === null){
                $( "input" ).remove( "#buttonPedido" );
                $( "div" ).remove( "#buttonPaypal" );
                $("#message").append("<p>No tienes ningun producto agregado a nuestro carrito</p>");
                return false;
            }
            //getCarrito
            pintarProductos(carrito);

            //Precio total
            var precioTotal = PrecioTotal();
            $(".total2").append("<p id='precio'>" + precioTotal+ "€</p>");

            // Limpiar carrito
            $("#buttonClearCard").append("<input type='button' name='buttonDelete' id='buttonDelete' value='Vaciar Carrito'></input>");
            vaciarCarrito();

            //Productos Totales en carrito
            totalProductos();

            /// eliminar prod
            eliminarProd();
            return false;
        }//end if json null

        if(!localStorage.getItem("datosCarrito")){
            //alert("no existe datosCarrito");
            localStorage.setItem("datosCarrito","[]");
        }

        var carrito = datosCarrito();
        //buscamos por id si el producto ya esta en el carrito
        for (i of carrito){
            if(i.datos.id === json.datos.id){
                console.log("el producto coincide: " + i.datos.id);
                for (var i in carrito){
                    //alert(carrito[i].datos.id);
                    if(carrito[i].datos.id === json.datos.id){
                        //alert("pos prod: "+i);
                        carrito.splice(i,1);
                        carrito.push(json);
                    
                    }
                }
                localStorage.setItem("datosCarrito",JSON.stringify(carrito));
                pintarProductos(carrito);
                
                //Precio total
                var precioTotal = PrecioTotal();
                $(".total2").append("<p id='precio'>" + precioTotal+ "€</p>");

                // Limpiar carrito
                $("#buttonClearCard").append("<input type='button' name='buttonDelete' id='buttonDelete' value='Vaciar Carrito'></input>");
                vaciarCarrito();

                //Productos Totales
                totalProductos();

                // eliminar prod
                eliminarProd();
                
                return false;
            }//end if prod repe

        }//end for

        //Agregamos producto al carrito
        carrito.push(json);
        console.log(carrito);
        localStorage.setItem("datosCarrito",JSON.stringify(carrito));
        pintarProductos(carrito);

        // Limpiar carrito
        $("#buttonClearCard").append("<input type='button' name='buttonDelete' id='buttonDelete' value='Vaciar Carrito'></input>");
        vaciarCarrito();
        
        //Precio total
        var precioTotal = PrecioTotal();
        $(".total2").append("<p id='precio'>" + precioTotal+ "€</p>");

        //Productos Totales
        totalProductos();
        
        // eliminar prod
        eliminarProd();

    });

    // Hacer pedido
    $("#buttonPedido").click(function(){
        var precioTotal = PrecioTotal();
        var carrito = datosCarrito();
        //alert(carrito);
        if (carrito=="") {
            vaciarCarrito();
            return false;
        }
        carrito.push({"precioTotal":precioTotal});            
        var data_prods_JSON = JSON.stringify(carrito);
        
        $.ajax({
            type: "POST",
            url: "Module/Carrito/Controller/controller_Carrito.class.php?op=pedido&pedido=true",
            data:{datosPedido:data_prods_JSON},
            success: function(message){
                //alert(message);
                var json = JSON.parse(message);
                //falta llevar productes
                if (json.success==true){
                    localStorage.clear();
                    $( "div" ).remove( ".item1" );
                    $( "div" ).remove( "#buttonClearCard" );
                    $( "input" ).remove( "#buttonPedido" );
                    $( "div" ).remove( "#buttonPaypal" );
                    $("#message").append("<p>El pedido se ha realizado satisfactoriamente. Le hemos enviado a su correo la verificacion y los detalles del pedido. Muchas gracias por confiar con nosotros.</p>");
                }

            },
            error: function(xhr){
                xhr.responseJSON = JSON.parse(xhr.responseText);
                //alert(xhr.responseJSON.noUsu);
                if (xhr.responseJSON.noUsu){
                    localStorage.clear();
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons:{
                            Ok: function(){
                                $(this).dialog("close");
                                window.location.href="index.php?module=Login&logop=registrar";
                                                    
                            }
                        },
                                          
                    });
                }else{
                    window.location.href='index.php?module=503';
                }
            }

        });
            
    });//buttonPedido

    //button paypal
    paypal.Button.render({
        env: 'sandbox',
        client: {
            sandbox:'ASjKl7PvLK2sG8X_OiXJ7jWKPqqFHI51QAiO7qhp_rJhkeq0JetFAvSNwQRDWA22Y2sky6fLNXSSEOya'
                
        },
        commit: true,
        payment: function(data, actions) {
            var precioTotal = PrecioTotal();
            var carrito = datosCarrito();
            if (carrito=="") {
                vaciarCarrito();
                return false;
            }
            return actions.payment.create({
                payment: {
                    transactions: [ {amount:{ total: precioTotal, currency: 'EUR'}} ]
                        
                }
            });
        },

        onAuthorize: function(data, actions) {
            // Make a call to the REST api to execute the payment
            return actions.payment.execute().then(function() {
                var precioTotal = PrecioTotal();
                var carrito = datosCarrito();
                if (carrito=="") {
                    vaciarCarrito();
                    return false;
                }
                carrito.push({"precioTotal":precioTotal});            
                var data_prods_JSON = JSON.stringify(carrito);
                
                $.ajax({
                    type: "POST",
                    url: "Module/Carrito/Controller/controller_Carrito.class.php?op=pedido&pedido=true",
                    data:{datosPedido:data_prods_JSON},
                    success: function(message){
                        //alert(message);
                        var json = JSON.parse(message);
                        if (json.success==true){
                            $( "div" ).remove( ".item1" );
                            $( "div" ).remove( "#buttonClearCard" );
                            $( "input" ).remove( "#buttonPedido" );
                            $( "div" ).remove( "#buttonPaypal" );
                            $("#message").append("<p>Su pago paypal con un valor de "+precioTotal+"€ y pedido se han realizado satisfactoriamente. Le hemos enviado a su correo la verificacion y los detalles del pedido. Muchas gracias por confiar con nosotros.</p>");
                            localStorage.clear();
                        }

                    },
                    error: function(xhr){
                        xhr.responseJSON = JSON.parse(xhr.responseText);
                        //alert(xhr.responseJSON.noUsu);
                        if (xhr.responseJSON.noUsu){
                            localStorage.clear();
                            $("#dialog-message").dialog({
                                modal: true,
                                buttons:{
                                    Ok: function(){
                                        $(this).dialog("close");
                                        window.location.href="index.php?module=Login&logop=registrar";
                                                            
                                    }
                                },
                                                  
                            });
                        }else{
                            window.location.href='index.php?module=503';
                        }
                    }

                });
            });
        }

    }, '#buttonPaypal');

    //funciones carrito
    function PrecioTotal(){
        var total = 0;
        var carrito = datosCarrito();
        if (carrito === null) {
            total = 0;
            return total;
        }
        for (i of carrito){
            //alert(i.datos.cantidad);
            //alert(i.datos.price);
            total += parseFloat(i.datos.cantidad) * parseFloat(i.datos.price);
        }

        return total;
    }

    function datosCarrito(){
        return this.getCarrito = JSON.parse(localStorage.getItem("datosCarrito"));
    }
    function totalProductos(){
        var carrito = datosCarrito();
        if (carrito === null) {
            return $("#totalProductos").html(0);
        }
        var total = carrito.length;
        console.log(total);
        $("#totalProductos").html(total);
    }
    function pintarProductos(carrito){
        carrito.forEach(function(elemento){
            //alert(elemento[0].datos.name);
            $(".items").append('<div class="item1"><div class="close1" id="'+elemento.datos.id+'"><div class="alert-close1"> </div><!-- //Remove-Item --><div class="image1"><img src="'+elemento.datos.media+'" alt="item1" width="45px" height="45px"></div>'
            +'<div class="title1"><p>'+elemento.datos.name+'</p></div><div class="quantity1"><form><input type="number" name="quantity" min="1" max="10" value="'+elemento.datos.cantidad+'" disabled></form>'
            +'</div><div class="price1"><p>'+elemento.datos.price+'</p></div><div class="clear"></div></div></div>');
            /*var spinner = $( "."+elemento.datos.id).spinner({
                disabled: true
            });
            spinner.spinner( "value", elemento.datos.cantidad );*/
                        
        });
    }
    function vaciarCarrito(){
        $(document).on('click', '#buttonDelete', function(event) {
            localStorage.clear();
            $( "div" ).remove( ".item1" );
            $( "div" ).remove( "#buttonClearCard" );
            $( "input" ).remove( "#buttonPedido" );
            $( "div" ).remove( "#buttonPaypal" );
            totalProductos();
            var precioTotal = PrecioTotal();
            $("#message").append("<p>No tienes ningun producto agregado a nuestro carrito</p>");
            $( "p" ).remove("#precio");
            $("#Total").append("<p id='precio'>" + precioTotal+ "€</p>");

        });
    }
    function eliminarProd(){
        $(document).on('click', '.close1', function(event) {
            var carrito = datosCarrito();
            var id = this.getAttribute('id');
            //alert("id prod:"+id);
            $( "div" ).remove( "#"+id );
                    
            for (var i in carrito) {
                //alert(carrito[i].datos.id);
                if(carrito[i].datos.id === id){
                    //alert("pos delete: "+i);
                    carrito.splice(i,1);
                }
            }

            localStorage.setItem("datosCarrito",JSON.stringify(carrito));
            var precioTotal = PrecioTotal();
            totalProductos();
            $( "p" ).remove("#precio");
            $(".total2").append("<p id='precio'>" + precioTotal+ "€</p>");

        });
    }
}//end function carrito

$(document).ready(function () {
    load_carrito();

});