<script src="https://www.paypalobjects.com/api/checkout.js"></script>
    <section id="carrito" class="wrap">
        <div class="row contact-wrap"> 
            <div id="content">
                <h1>LISTA CARRITO</h1>
                <!-- Content-Starts-Here -->
                <div class="container">

                    <!-- Mainbar-Starts-Here -->
                    <div class="main-bar wrap">
                        <div class="product">
                            <h3>Product</h3>
                        </div>
                        <div class="quantity">
                            <h3>Quantity</h3>
                        </div>
                        <div class="priceTotal">
                            <h3>Price</h3>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!-- //Mainbar-Ends-Here -->

                    <!-- Items-Starts-Here -->
                    <div class="items wrap">
                        <div id="message"></div>
                    </div>
                    <!-- //Items-Ends-Here -->

                    <!-- Total-Price-Starts-Here -->
                    <div class="total wrap">
                        <div class="total1">Total Price</div>
                        <div class="total2"></div>
                        <div class="clear"></div>
                    </div>
                    <!-- //Total-Price-Ends-Here -->

                    <!-- Checkout-Starts-Here -->
                    <div class="checkout wrap">
                        <div class="discount">
                            <div id="buttonClearCard"></div>
                        </div>
                        <div class="add">
                            <div id="buttonPaypal"></div>
                        </div>
                        <div class="checkout-btn">
                            <input type="button" id="buttonPedido" class="buttonPedido" value="Comprar" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!-- //Checkout-Ends-Here -->
                    <div class="modal fade" role="dialog">
                        <div id="dialog-message" title="Delete Complete">
                          <p>
                            <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
                            Tiene que registrarse antes para poder comprar.
                          </p>

                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript" src="Module/Carrito/View/Js/listCarrito.js" ></script>
        </div>
    </section>