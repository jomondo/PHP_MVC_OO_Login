<?php
include_once($_SERVER['DOCUMENT_ROOT']."/productos/Module/Home/Model/DAOdummiesHome.php");
@session_start();

switch ($_GET['op']) {
	case 'listDummiesHome':
		$json_arr = array();
		$display_json = array();
	
		try {
			$rdo = DAOdummiesHome::listDummiesHome();
		} catch (Exception $e) {
			header('HTTP/1.0 400 Bad error');
            exit;
		}
		if (!$rdo) {
			header('HTTP/1.0 400 Bad error');
            exit;
		}else{
			foreach ($rdo as $value) {
				$json_arr["media"] = $value['media'];
				$json_arr["name"] = $value['name'];
				$json_arr["price"] = $value['price'];
				$json_arr["id"] = $value['id'];
				array_push($display_json, $json_arr);
			}

			echo json_encode($display_json);
			exit;
		}
	break;
    case 'autocomplete';
    if (isset($_GET["auto"]) && $_GET["auto"] == true){
    	$prodJson = json_decode(@$_POST["gammas"], true);
		
		$display_json = array();
		$json_arr = array();
		
		try {
			$rdo = DAOdummiesHome::lookByBrand($prodJson);
			
		} catch (Exception $e) {
            header('HTTP/1.0 400 Bad error');
            exit;
		}
		
		if (!$rdo) {
			header('HTTP/1.0 400 Bad error');
            exit;
		}else{
			$contRows = mysqli_num_rows($rdo);
			//print_r($contRows);
			//exit;
			if($contRows>0){
				foreach ($rdo as $row) {
					$json_arr["redirect"] = "index.php?module=Home&view=resultAutocomplete";
					$json_arr["id"] = $row['id'];
					$json_arr["value"] = $row['name'];
					$json_arr["price"] = $row['price'];
					$json_arr["media"] = $row['media'];
					$json_arr["tipo"] = $row['tipo'];
					array_push($display_json, $json_arr);

				}

			@$_SESSION['nameProd']=$display_json;

			}else{
				$json_arr["redirect"] = "";
				$json_arr["value"] = "No Result Found !";
				array_push($display_json, $json_arr);

			}
		}
		
		echo json_encode($display_json);
		exit;
	}

	///////// arrepleguem dades del autocomplete ////////
	if (isset($_GET["load"]) && $_GET["load"] == true){
        $jsondata = array();
        
        if (isset($_SESSION['nameProd'])) {
           	$jsondata = $_SESSION['nameProd'];

        }    
        
        echo json_encode($jsondata);
        exit;
    }
    break;
    case 'filterListPrice':
		$json_arr = array();
		$display_json = array();
		
		if (isset($_POST['datosPrice'])){
			$prodJson = json_decode($_POST["datosPrice"], true);
			try {
				$rdo = DAOdummiesHome::filtroPrice($prodJson['priceMin'],$prodJson['priceMax']);
				$rdoCount = DAOdummiesHome::countListPrice($prodJson['priceMin'],$prodJson['priceMax']);
			} catch (Exception $e) {
				header('HTTP/1.0 400 Bad error');
	            exit;

			}

			if (!$rdo) {
				header('HTTP/1.0 400 Bad error');
	            exit;

			}else{
				foreach ($rdo as $row) {
					$json_arr["id"] = $row['id'];
					$json_arr["value"] = $row['name'];
					$json_arr["price"] = $row['price'];
					$json_arr["media"] = $row['media'];
					$json_arr["tipo"] = $row['tipo'];
					foreach ($rdoCount as $value) {
						$json_arr["countProd"] = $value['count(id)'];
						
					}
					array_push($display_json, $json_arr);
					
				}
				
				//$display_json['countProd'] = $count;
				echo json_encode($display_json);
				exit;
			}
		}
		
	break;
	case 'filterListColor':
		$json_arr = array();
		$display_json = array();
		
		if (isset($_POST['datosColor'])){
			$prodJson = json_decode($_POST["datosColor"], true);
			try {
				$rdo = DAOdummiesHome::filtroColor($prodJson['color']);
				$rdoCount = DAOdummiesHome::countListColor($prodJson['color']);

			} catch (Exception $e) {
				header('HTTP/1.0 400 Bad error');
	            exit;

			}

			if (!$rdo) {
				header('HTTP/1.0 400 Bad error');
	            exit;

			}else{
				foreach ($rdo as $row) {
					$json_arr["id"] = $row['id'];
					$json_arr["value"] = $row['name'];
					$json_arr["price"] = $row['price'];
					$json_arr["media"] = $row['media'];
					$json_arr["tipo"] = $row['tipo'];
					foreach ($rdoCount as $value) {
						$json_arr["countProd"] = $value['count(id)'];
						
					}
					array_push($display_json, $json_arr);
				}

				echo json_encode($display_json);
				exit;
			}
		}
		
	break;
	case 'filterListAvailability':
		$json_arr = array();
		$display_json = array();
		
		if (isset($_POST['datosAvailability'])){
			$prodJson = json_decode($_POST["datosAvailability"], true);
			try {
				$rdo = DAOdummiesHome::filtroAvailability($prodJson['availability']);
				$rdoCount = DAOdummiesHome::countListAvailability($prodJson['availability']);
				
			} catch (Exception $e) {
				header('HTTP/1.0 400 Bad error');
	            exit;

			}

			if (!$rdo) {
				header('HTTP/1.0 400 Bad error');
	            exit;

			}else{
				foreach ($rdo as $row) {
					$json_arr["id"] = $row['id'];
					$json_arr["value"] = $row['name'];
					$json_arr["price"] = $row['price'];
					$json_arr["media"] = $row['media'];
					$json_arr["tipo"] = $row['tipo'];
					foreach ($rdoCount as $value) {
						$json_arr["countProd"] = $value['count(id)'];
						
					}
					array_push($display_json, $json_arr);
				}

				echo json_encode($display_json);
				exit;
			}
		}
		
	break;
	
}

