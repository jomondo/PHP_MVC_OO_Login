$(document).ready(function () {
    loadCmpnentsAuto();
    filtrarPrecio ();
    filterAvailability();
    filterColor ();
    
});
//// productos autocomplete Home ///////
function loadCmpnentsAuto() {
    $.get("Module/Home/Controller/controller_Home.class.php?op=autocomplete&load=true", function (data) {
        //alert(data);
        var json = JSON.parse(data);
        //alert(json[0].value);
        pintarCmpnents(json);

    });
}

function pintarCmpnents(data) {
    //alert(data[0].value);
    var content = document.getElementById("content");
    var div_prod = document.createElement("div");
    
    data.forEach(function(elemento) {
        var component = document.createElement("div");
        component.innerHTML = "<div class='prodAutocomplete' id='prodAutocomplete' style='margin-bottom:80px;'><a href='preview.html'><img src="+elemento.media+" alt=''/></a><h2>"+elemento.value+"</h2>"+
        "<div class='price-details'><div class='price-number'><p><span class='rupees'>"+elemento.price+"€</span></p></div>"+
        "<input type='button' class='ButtonList' id="+elemento.id+" value='Details'></input></div>";

        div_prod.appendChild(component);
        
    });

    /////////// API ebay productos relacionados ////////////
    $.ajax({
        type: "GET",
        url: "https://api.ebay.com/buy/browse/v1/item_summary/search?q="+data[0].value+"&limit=4",
        crossDomain: true,
        contentType: "application/json",
                
        beforeSend: function (xhr) {
            //descomentar para que funcione la api
           xhr.setRequestHeader('Authorization', 'Bearer v^1.1#i^1#I^3#p^1#f^0#r^0#t^H4sIAAAAAAAAAOVXa2wUVRTudtuSgqUxopiKugyPBHVm78zs7M5OuitLC7JQ2pUtz6JlHnfo0NmZydy7LauCtQQSERMJVH5gAiQmihFfxB+agApifAQTiYTgH2MISlBISIyVqOid6VK2lfAsQuL+2dxzzz33fN/5zr1zQU9V9UPrZ6/vrwmMKt/RA3rKAwF2DKiuqnx4bLC8rrIMlDgEdvRM7qnoDZ6sR3LOdKT5EDm2hWBoVc60kOQbE1TetSRbRgaSLDkHkYRVKZua1yRxDJAc18a2aptUKN2YoGAsFmdjcSDoIKZyCiRW60LMVjtBKRzPghgnQCESgyonkHmE8jBtISxbOEFxgBVpwNNAbGUjEstJnMhE+NhSKrQQusiwLeLCACrppyv5a92SXC+fqowQdDEJQiXTqVnZllS6cWZza324JFayyEMWyziPho4abA2GFspmHl5+G+R7S9m8qkKEqHByYIehQaXUhWSuI32fajXGqgJUBJXlgSCy/IhQOct2czK+fB6exdBo3XeVoIUNXLgSo4QNZSVUcXHUTEKkG0Pe3+N52TR0A7oJauaM1JJUJkMlV9o529JsjVbtnJPH0KUz8xvpiMCqclSIElRRURdBHBY3GohWpHnYTg0kkOGRhkLNNp4BSdZwODeghBvi1GK1uCkdexmV+kUvcMiJS72iDlQxjzssr64wR4gI+cMrV2BwNcauoRCAgxGGT/gUJSjZcQyNGj7pa7Eon1UoQXVg7EjhcHd3N9PNM7a7IswBwIYXz2vKqh0wJ1PE1+v1AX/jygtow4eikjYm/hIuOCSXVUSrJAFrBZXkovE4KxR5H5pWcrj1X4YSzOGhHTFSHQLjPBeJ6EKcF9WoApSR6JBkUaRhLw+oyAU6J7udEDumrEKiVwvlc9A1NIkXdI4XdUhr0bhOR+K6TiuCFqVZHUIAoaKocfH/1ChXK/WsajswY5uGWhgRwY+Y2HlXy8guLmShaRLD1ar+kiCRB/Kmw/N6/ZogejEQCSI7BuNpmyGyCtsyOdQ8U7uf9Q3hNsh9eFsVlQAcQGpoAxcZ48NlUJfKuBDZeZfc4UyLd6632p3QIl2CXds0obuQvSEmRu5Ev0Wn+SVRqaZBaGy/3ZBd4zF5ndqW8S1EXdEbaLsEclbggMCKfES8IWwNfl1bC//BoXVNhZ1tIwy1m/ABEh76HEqW+T+2N/A+6A28S15UIAymsJPAxKrggorgHXXIwJAxZJ1BxgqLfOW7kOmEBUc23PKqQNuEd3a1lzzAdjwB7h18glUH2TEl7zEw4eJMJVs7voYVAQ9ENsJy5DoFky7OVrD3VIxrPrf2530bRo+LZrbmp06ePv33l17eCWoGnQKByjKijLIu6qmj0858q+ETx79ad/gsG5jfVb//6xmvvF1/POLeuWXjvjNzHouNY3/qH79ndPWR/SewkY4dmbO76tlTL/71S3CvVfPl1im/Lp9a9dqpPq3zg/KmjaMP6Q/UKss231X2+Vx69QHz5Lzzdc75yNFo74eu2M6MffXYd2tP7/7t+Jb7YvzyZZu4VndMf/8nSkdX27Fp55Y87XT+OOruJXNrn3vj04mL44vulzZ/Qx95PvhW7BFt9s4/Qn2dDUmTm3v2Y2XPmo+2JXLnOHbdwdPbP4uIX0x9sElqXH1+TSyz+syTdckFu0ad3PTen29+v72WiRz4u0y004ueOfTo6yu3HQz90FbRd/gF2Ldxb2agfP8AxwINRBoPAAA=');
        },
        success: function(message){
            //alert(message.itemSummaries[0].title);
            /*message.itemSummaries.forEach(function(elemento) {
                alert(elemento.price.value);
            });*/

            var content = document.getElementById("prodRelaEbay");
            var div_prod = document.createElement("div");
            //alert(message.itemSummaries[0].title);
            if (message.itemSummaries==undefined) {
                $("#prodRelaEbay").append("<p>No hay productos relacionados en ebay</p>");
                return false;

            }
            message.itemSummaries.forEach(function(elemento) {
                var component = document.createElement("div");
                component.innerHTML = "<div class='grid_1_of_4 images_1_of_4' id='' style='margin-left:16px;'><div class='prodEbay'>Ebay</div><a href="+elemento.itemWebUrl+"><img src="+elemento.image.imageUrl+" alt=''/></a><h2>"+elemento.title+"</h2>"+
                "<div class='price-details'><div class='price-number'><p><span class='rupees'>"+elemento.price.value+elemento.price.currency+"</span></p></div>"+
                "<a href="+elemento.itemWebUrl+"><input type='button' class='ButtonApi' id='' value='Ir al producto'></a></input></div>";

                div_prod.appendChild(component);
                
            });
   
            content.appendChild(div_prod);
                    
        },

        error: function(){
            alert("Error cargar api ebay");
                    
        }

    });
    
       
    content.appendChild(div_prod);   
        
}

function filtrarPrecio (){
    $(function(){
        $("#slider-range").slider({
            range: true,
            min: 0,
            max: 1000,
            values: [ 50, 120 ],
            slide: function( event, ui ){
                //alert(ui.values[0]);
                $( "#amount" ).val( "$" + ui.values[0] + " - $" + ui.values[1] );
                var data = {"priceMin":ui.values[0],"priceMax":ui.values[1],};
                var data_price_Json = JSON.stringify(data);
                $.ajax({
                    type: "POST",
                    url: "Module/Home/Controller/controller_Home.class.php?op=filterListPrice",
                    data:{datosPrice:data_price_Json},
                    success: function(message){
                        //alert(message);
                        var json = JSON.parse(message);
                        $('div#prodAutocomplete').remove();
                        json.forEach(function(elemento){
                            //alert(elemento.name);
                            $("#content").append("<div class='prodAutocomplete' id='prodAutocomplete' style='margin-bottom:80px;'><a href='#'><img src="+elemento.media+" alt=''/></a><h2>"+elemento.value+"</h2>"+
                                "<div class='price-details'><div class='price-number'><p><span class='rupees'>"+elemento.price+"€</span></p></div>"+
                                "<input type='button' class='ButtonList' id="+elemento.id+" value='Details'></input></div>");
                                            
                        });

                        contarProd(json);
                        
                        
                                
                    },
                    error: function(){
                        //alert("Error filter list_autocomplete");
                        window.location.href='index.php?module=503';
                                
                    }

                });
            
            }
        });

        $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
          " - $" + $( "#slider-range" ).slider( "values", 1 ) );

    });
}

function filterColor(){
    /////// dropdownlist color ///////////    
    var data = new Array();
    var firstNames = ["Negro", "Azul", "Rojo", "Amarillo", "Verde", "Violeta", "Plata", "Oro", "Marron"];

    var k = 0;
    for (var i = 0; i < firstNames.length; i++) {
        var row = {};
        row["firstname"] = firstNames[k];
        data[i] = row;
        k++;
    }
    var source ={localdata: data,datatype: "array"};
    var dataAdapter = new $.jqx.dataAdapter(source);
    $('.color').jqxDropDownList({ selectedIndex: 0,  source: dataAdapter, displayMember: "firstname", placeHolder: "Select...",itemHeight: 20, height: 25, width: 245,
        renderer: function (index, label, value) {
            var datarecord = data[index];
            var table = '<table style="min-width: 10px;"><td id="'+datarecord.firstname+'">' + datarecord.firstname + '</td></tr></table>';
                    
            return table;
        },
               
        ready: function () {
            $(".color").change(function(){
                var color = $(".color").jqxDropDownList('val');
                var data = {"color":color};
                var data_color_Json = JSON.stringify(data);
                $.ajax({
                    type: "POST",
                    url: "Module/Home/Controller/controller_Home.class.php?op=filterListColor",
                    data:{datosColor:data_color_Json},
                    success: function(message){
                        //alert(message);
                        var json = JSON.parse(message);
                        $('div#prodAutocomplete').remove();
                        json.forEach(function(elemento){
                            //alert(elemento.name);
                            $("#content").append("<div class='prodAutocomplete' id='prodAutocomplete' style='margin-bottom:80px;'><a href='#'><img src="+elemento.media+" alt=''/></a><h2>"+elemento.value+"</h2>"+
                                "<div class='price-details'><div class='price-number'><p><span class='rupees'>"+elemento.price+"€</span></p></div>"+
                                "<input type='button' class='ButtonList' id="+elemento.id+" value='Details'></input></div>");
                                            
                        });
                        contarProd(json);
                                
                    },
                    error: function(){
                        //alert("Error filter list_autocomplete");
                        window.location.href='index.php?module=503';
                                
                    }

                });
                          
            });
        }

    });
}

function filterAvailability (){
    $(function(){
        $("input[type=radio]").checkboxradio({icon: false,});
        $("input[name='radio-1']").change(function(){
            var availability = $('input[name="radio-1"]:checked').val();
            var data = {"availability":availability};
            var data_availability_Json = JSON.stringify(data);
            console.log(data_availability_Json);
            $.ajax({
                type: "POST",
                url: "Module/Home/Controller/controller_Home.class.php?op=filterListAvailability",
                data:{datosAvailability:data_availability_Json},
                success: function(message){
                    //alert(message);
                    var json = JSON.parse(message);
                    $('div#prodAutocomplete').remove();
                    json.forEach(function(elemento){
                        //alert(elemento.name);
                        $("#content").append("<div class='prodAutocomplete' id='prodAutocomplete' style='margin-bottom:80px;'><a href='#'><img src="+elemento.media+" alt=''/></a><h2>"+elemento.value+"</h2>"+
                            "<div class='price-details'><div class='price-number'><p><span class='rupees'>"+elemento.price+"€</span></p></div>"+
                            "<input type='button' class='ButtonList' id="+elemento.id+" value='Details'></input></div>");
                                            
                    });
                    contarProd(json);
                        
                                
                },
                error: function(){
                    //alert("Error filter list_autocomplete");
                    window.location.href='index.php?module=503';
                                
                }

            });
        });
    });

}

function contarProd (json){
    if (json[0]==undefined){
        $('div#counts').remove();
        $("#countProds").append("<div class='' id='counts'> Hay 0 productos filtrados</div>");
                                
    }else{
        $('div#counts').remove();
        $("#countProds").append("<div class='' id='counts'>Hay " + json[0].countProd + " productos filtrados</div>");

    }
}
