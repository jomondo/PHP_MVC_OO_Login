$(document).ready(function () {
    var marcas;
    var gammas;
    
    ////// busqueda en json///////
    var searchIntoJson = function (obj, column, value) {
        var results = [];
        var valueField;
        var searchField = column;
        for (var i = 0 ; i < obj.length ; i++) {
            valueField = obj[i][searchField].toString();
            if (valueField === value) {
                results.push(obj[i]);
            }
        }
        return results;
    };

    var loadGammas = function (){
        $("#gamma").empty();
        $("#gamma").append('<option value="" selected="selected">Selecciona una Gamma</option>');
        $.each(gammas, function (i, valor) {
            $("#gamma").append("<option value='" + valor.gammaId + "'>" + valor.nombreGamma + "</option>");
        });
    };

    var loadMarcas = function (gammas){
        var gammasMarcas = searchIntoJson(marcas, "gammaId", gammas);
        $("#marca").empty();
        $("#marca").append('<option value="" selected="selected">Selecciona una marca</option>');
        $.each(gammasMarcas, function (i, valor) {
            $("#marca").append('<option value="' + valor.nombreMarca + '">' + valor.nombreMarca + '</option>');
        });
    };
    
   /////////// carga json externos /////////////////
    $.getJSON("Resources/gammas.json", function (data){
        gammas = data;

    });

    $.getJSON("Resources/marcas.json", function (data){
        marcas = data;
        setTimeout(function () {
            if (marcas !== undefined) {
                loadGammas();
            }
        }, 1000);
    });
    
    $("#gamma").change(function () {
        var gammas = $("#gamma").val();
        //alert(gammas);
        loadMarcas(gammas);

    });
    
    ///////////// iniciamos peticion cuando tecleamos en el input ///////////      
    $("#dd_user_input").keyup(function(){
        var dd_user_input = $("#dd_user_input").val();
        var marca = $("#marca").val();
        
        var data = {"marca":marca,"userInput":dd_user_input};
        var data_value_JSON = JSON.stringify(data);
        console.log("json debug: "+data_value_JSON);
                   
        $.ajax({
            type: "POST",
            url: "Module/Home/Controller/controller_Home.class.php?op=autocomplete&auto=true",
            data: {gammas:data_value_JSON},
            success: function(data){
               // alert(data);
                var json = JSON.parse(data);
                       
                $("#dd_user_input").autocomplete({
                    source: json,
                    minLength: 2,
                    select: function(event, ui) {
                        var getUrl = ui.item.redirect;
                        if(getUrl != '#') {
                            location.href = getUrl;
                        }
                    },
                    html: true, 
                    open: function(event, ui){
                        $(".ui-autocomplete").css("z-index", 1000);
                    }
                });

            },
            error: function(){
                //alert("Error");
                window.location.href='index.php?module=503';
            }

        });

    });

    /////////// dummies productos home ////////////
    $.ajax({
        type: "GET",
        url: "Module/Home/Controller/controller_Home.class.php?op=listDummiesHome",
        success: function(message){
            //alert(message);
            var json = JSON.parse(message);

            json.forEach(function(elemento){
                $("#dummiesHome").append('<div class="grid_1_of_4 images_1_of_4" id="" style="margin-left:108px;"><a href=""><img src='+elemento.media+' alt="" /></a><h2>'+elemento.name+'</h2>'
                    +'<div class="price-details"><div class="price-number"><p><span class="rupees">'+elemento.price+'€</span></p></div>'                         
                    +'<button type="submit" class="ButtonDetails" id="'+elemento.id+'">Details</button></div></div>');
                                
            });
                    
        },
        error: function(){
            alert("Error cargar dummiesHome");
            window.location.href='index.php?module=503';        
        }

    });

    /////////// API ebay productos interesantes ////////////
    $.ajax({
        type: "GET",
        url: "https://api.ebay.com/buy/browse/v1/item_summary/search?q=iphone&limit=4",
        crossDomain: true,
        contentType: "application/json",
                
        beforeSend: function (xhr) {
            //descomentar para que funcione la api
           xhr.setRequestHeader('Authorization', 'Bearer v^1.1#i^1#I^3#p^1#f^0#r^0#t^H4sIAAAAAAAAAOVXa2wUVRTudtuSgqUxopiKugyPBHVm78zs7M5OuitLC7JQ2pUtz6JlHnfo0NmZydy7LauCtQQSERMJVH5gAiQmihFfxB+agApifAQTiYTgH2MISlBISIyVqOid6VK2lfAsQuL+2dxzzz33fN/5zr1zQU9V9UPrZ6/vrwmMKt/RA3rKAwF2DKiuqnx4bLC8rrIMlDgEdvRM7qnoDZ6sR3LOdKT5EDm2hWBoVc60kOQbE1TetSRbRgaSLDkHkYRVKZua1yRxDJAc18a2aptUKN2YoGAsFmdjcSDoIKZyCiRW60LMVjtBKRzPghgnQCESgyonkHmE8jBtISxbOEFxgBVpwNNAbGUjEstJnMhE+NhSKrQQusiwLeLCACrppyv5a92SXC+fqowQdDEJQiXTqVnZllS6cWZza324JFayyEMWyziPho4abA2GFspmHl5+G+R7S9m8qkKEqHByYIehQaXUhWSuI32fajXGqgJUBJXlgSCy/IhQOct2czK+fB6exdBo3XeVoIUNXLgSo4QNZSVUcXHUTEKkG0Pe3+N52TR0A7oJauaM1JJUJkMlV9o529JsjVbtnJPH0KUz8xvpiMCqclSIElRRURdBHBY3GohWpHnYTg0kkOGRhkLNNp4BSdZwODeghBvi1GK1uCkdexmV+kUvcMiJS72iDlQxjzssr64wR4gI+cMrV2BwNcauoRCAgxGGT/gUJSjZcQyNGj7pa7Eon1UoQXVg7EjhcHd3N9PNM7a7IswBwIYXz2vKqh0wJ1PE1+v1AX/jygtow4eikjYm/hIuOCSXVUSrJAFrBZXkovE4KxR5H5pWcrj1X4YSzOGhHTFSHQLjPBeJ6EKcF9WoApSR6JBkUaRhLw+oyAU6J7udEDumrEKiVwvlc9A1NIkXdI4XdUhr0bhOR+K6TiuCFqVZHUIAoaKocfH/1ChXK/WsajswY5uGWhgRwY+Y2HlXy8guLmShaRLD1ar+kiCRB/Kmw/N6/ZogejEQCSI7BuNpmyGyCtsyOdQ8U7uf9Q3hNsh9eFsVlQAcQGpoAxcZ48NlUJfKuBDZeZfc4UyLd6632p3QIl2CXds0obuQvSEmRu5Ev0Wn+SVRqaZBaGy/3ZBd4zF5ndqW8S1EXdEbaLsEclbggMCKfES8IWwNfl1bC//BoXVNhZ1tIwy1m/ABEh76HEqW+T+2N/A+6A28S15UIAymsJPAxKrggorgHXXIwJAxZJ1BxgqLfOW7kOmEBUc23PKqQNuEd3a1lzzAdjwB7h18glUH2TEl7zEw4eJMJVs7voYVAQ9ENsJy5DoFky7OVrD3VIxrPrf2530bRo+LZrbmp06ePv33l17eCWoGnQKByjKijLIu6qmj0858q+ETx79ad/gsG5jfVb//6xmvvF1/POLeuWXjvjNzHouNY3/qH79ndPWR/SewkY4dmbO76tlTL/71S3CvVfPl1im/Lp9a9dqpPq3zg/KmjaMP6Q/UKss231X2+Vx69QHz5Lzzdc75yNFo74eu2M6MffXYd2tP7/7t+Jb7YvzyZZu4VndMf/8nSkdX27Fp55Y87XT+OOruJXNrn3vj04mL44vulzZ/Qx95PvhW7BFt9s4/Qn2dDUmTm3v2Y2XPmo+2JXLnOHbdwdPbP4uIX0x9sElqXH1+TSyz+syTdckFu0ad3PTen29+v72WiRz4u0y004ueOfTo6yu3HQz90FbRd/gF2Ldxb2agfP8AxwINRBoPAAA=');
        },
        success: function(message){
            //alert(message.itemSummaries[0].title);
            /*message.itemSummaries.forEach(function(elemento) {
                alert(elemento.price.value);
            });*/

            var content = document.getElementById("contProdEbay");
            var div_prod = document.createElement("div");
            
            message.itemSummaries.forEach(function(elemento) {
                var component = document.createElement("div");
                component.innerHTML = "<div class='grid_1_of_4 images_1_of_4' id='' style='margin-left:10px;'><div class='prodEbay'>Ebay</div><a href="+elemento.itemWebUrl+"><img src="+elemento.image.imageUrl+" alt=''/></a><h2>"+elemento.title+"</h2>"+
                "<div class='price-details'><div class='price-number'><p><span class='rupees'>"+elemento.price.value+elemento.price.currency+"</span></p></div>"+
                "<a href="+elemento.itemWebUrl+"><input type='button' class='ButtonApi' id='' value='Ir al producto'></a></input></div>";

                div_prod.appendChild(component);
                
            });
   
            content.appendChild(div_prod);
                    
        },

        error: function(){
            alert("Error cargar api ebay");
                    
        }

    });
});//en ready




