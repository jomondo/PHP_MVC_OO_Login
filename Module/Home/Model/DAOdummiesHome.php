<?php
include_once($_SERVER['DOCUMENT_ROOT']."/productos/Model/connect.php");

class DAOdummiesHome{
	function listDummiesHome(){
		$sql = "SELECT * FROM cmpoinfrmatics LIMIT 10 ";
			
		$conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);
            
        return $res;
		
	}
	function lookByBrand($prodJson){
		@$_SESSION['datosProd']=$prodJson;

		$sql = 'SELECT id,name,price,media,tipo FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" ORDER BY price DESC';
	
		$conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);
            
        return $res;
		
	}

	function filtroPrice($priceMin,$priceMax){
		@$prodJson=$_SESSION['datosProd'];
		@$_SESSION['priceMin']=$priceMin;
		@$_SESSION['priceMax']=$priceMax;
		//print_r($prodJson);
		//exit;

		$sql = 'SELECT id,name,price,media,tipo FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.'';
		
		$conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);
            
        return $res;
	}
	function filtroColor($color){
		@$prodJson=$_SESSION['datosProd'];
		@$priceMin=$_SESSION['priceMin'];
		@$priceMax=$_SESSION['priceMax'];
		@$_SESSION['color']=$color;
		//print_r($prodJson);
		//exit;
		if (empty($_SESSION['priceMin']) && empty($_SESSION['priceMax'])) {
			@$priceMin=50;
			@$priceMax=120;
		}
		$sql = 'SELECT id,name,price,media,tipo FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.' and color = "'.$color.'"';
		
		$conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);
            
        return $res;
	}

	function filtroAvailability($availability){
		@$prodJson=$_SESSION['datosProd'];
		@$priceMin=$_SESSION['priceMin'];
		@$priceMax=$_SESSION['priceMax'];
		@$color=$_SESSION['color'];
		@$_SESSION['availability']=$availability;

		if (empty($_SESSION['priceMin']) && empty($_SESSION['priceMax']) && empty($_SESSION['color'])) {
			@$priceMin=50;
			@$priceMax=120;
			
			$sql = 'SELECT id,name,price,media,tipo FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.' and availability = "'.$availability.'"';

		}else if (empty($_SESSION['priceMin']) && empty($_SESSION['priceMax'])){
			@$priceMin=50;
			@$priceMax=120;

			$sql = 'SELECT id,name,price,media,tipo FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.' and color = "'.$color.'" and availability = "'.$availability.'"';

		}else if (empty($_SESSION['color'])){
			$sql = 'SELECT id,name,price,media,tipo FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.' and availability = "'.$availability.'"';

		}else{
			$sql = 'SELECT id,name,price,media,tipo FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.' and color = "'.$color.'" and availability = "'.$availability.'"';

		}

		$conexion = conexion::open();
	    $res = mysqli_query($conexion, $sql);
	    conexion::close($conexion);
	            
	    return $res;

	}

	function countListPrice($priceMin,$priceMax){
		@$prodJson=$_SESSION['datosProd'];
		//@$_SESSION['priceMin']=$priceMin;
		//@$_SESSION['priceMax']=$priceMax;
		//print_r($color);
		//exit;

		$sql = 'SELECT count(id) FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.'';
		
		$conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);
            
        return $res;
	}

	function countListAvailability($availability){
		@$prodJson=$_SESSION['datosProd'];
		@$priceMin=$_SESSION['priceMin'];
		@$priceMax=$_SESSION['priceMax'];
		@$color=$_SESSION['color'];

		if (empty($_SESSION['priceMin']) && empty($_SESSION['priceMax']) && empty($_SESSION['color'])) {
			@$priceMin=50;
			@$priceMax=120;
			
			$sql = 'SELECT count(id) FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.' and availability = "'.$availability.'"';

			
		}else if (empty($_SESSION['priceMin']) && empty($_SESSION['priceMax'])){
			@$priceMin=50;
			@$priceMax=120;
			
			$sql = 'SELECT count(id) FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.' and color = "'.$color.'" and availability = "'.$availability.'"';

		}else if (empty($_SESSION['color'])){
			$sql = 'SELECT count(id) FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.' and availability = "'.$availability.'"';

		}else{
			$sql = 'SELECT count(id) FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.' and color = "'.$color.'" and availability = "'.$availability.'"';
		
			
	       
		}

		$conexion = conexion::open();
	    $res = mysqli_query($conexion, $sql);
	    conexion::close($conexion);
	            
	    return $res;

		
	}

	function countListColor($color){
		@$prodJson=$_SESSION['datosProd'];
		@$priceMin=$_SESSION['priceMin'];
		@$priceMax=$_SESSION['priceMax'];

		if (empty($_SESSION['priceMin']) && empty($_SESSION['priceMax'])) {
			@$priceMin=50;
			@$priceMax=120;
		}

		$sql = 'SELECT count(id) FROM cmpoinfrmatics WHERE brand = "'.$prodJson['marca'].'" and name LIKE "%'.$prodJson['userInput'].'%" and price >='.$priceMin.' and price <='.$priceMax.' and color = "'.$color.'"';
		
		$conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);
            
        return $res;
	}




}