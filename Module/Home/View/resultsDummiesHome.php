<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<script type="text/javascript" src="Module/Slider/Model/slider.js"></script>
<script type="text/javascript" src="Module/Home/Model/Home.js"></script>
<section>
  <div class="wrap">
    <div class="header"> 
    <div class="header_slide">
            <div class="header_bottom_left">                
                <div class="categories">
                  <ul>
                    <h3>Categories</h3>
                      <li><a href="#">Mobile Phones</a></li>
                      <li><a href="#">Desktop</a></li>
                      <li><a href="#">Laptop</a></li>
                      <li><a href="#">Accessories</a></li>
                      <li><a href="#">Software</a></li>
                       <li><a href="#">Sports &amp; Fitness</a></li>
                       <li><a href="#">Footwear</a></li>
                       <li><a href="#">Jewellery</a></li>
                       <li><a href="#">Clothing</a></li>
                       <li><a href="#">Home Decor &amp; Kitchen</a></li>
                       <li><a href="#">Beauty &amp; Healthcare</a></li>
                       <li><a href="#">Toys, Kids &amp; Babies</a></li>
                  </ul>
                </div>                  
             </div>
        <!--slider dinamic-->
        <div class="header_bottom_right"></div>
        <div class="clear"></div>
        </div>
   </div>
   <!--dependent dropdown-->
    <div class="content_top">
        <div class="heading">
            <h3>Busca productos por calidad</h3>
        </div>
        <div class="col_1_of_4">
            <select class="caja" id="gamma" ></select>
            <select class="caja" id="marca" ></select>
                
            <input id="dd_user_input" type="text" class="search_form" onblur="if(this.value=='')this.value=this.defaultValue;" onfocus="if(this.value==this.defaultValue)this.value='';" value="Search your product"/> 
                
        </div>
        <br/>
    </div>
    
    <div class="content_top">
      <div class="heading">
        <h3>New Products</h3>
      </div>
      <div class="see">
        <p><a href="#">See all Products</a></p>
      </div>
      <div class="clear"></div>
    </div>
    <div id="dummiesHome"></div>

    <div id="contProdEbay">
      <div class="clear"></div>
      <div class="content_top">
        <div class="heading">
          <h3 class="titleEbay">Productos Ebay</h3>
        </div>
        <div class="clear"></div>
      </div>
    </div>

 </div>
</section>

