    <link rel="stylesheet" href="View/lib/jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="View/lib/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxsplitter.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxdata.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxdropdownlist.js"></script>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<section class="wrap">
        <div class="center">        
            <h2>RESULT AUTOCOMPLETE</h2>
            <p class="lead"></p>
        </div> 
        <div class="row contact-wrap"> 
            <div class="status alert alert-success" style="display: none"></div>

            <div id="content">
                <div id="countProds"></div>
                <div class="cuadroFilter conf_cuadroFilter col_1_of_3">
                    <h3 class="titleFilter">Filters Products</h3><br/>
                    <label for="amount">Price range:</label>
                    <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
                    <div id="slider-range"></div><br/><br/>
                    <label for="amount">Color:</label><div id="" class="color"></div><br/><br/>
                    <fieldset>
                        <legend>Select Availability: </legend>
                        <label for="radio-1">Stock</label>
                        <input type="radio" name="radio-1" id="radio-1" value="Stock">
                        <label for="radio-2">Fuera de stock</label>
                        <input type="radio" name="radio-1" id="radio-2" value="Fuera de stock">
                    </fieldset>
                </div>
            </div>
            <div id="prodRelaEbay">
                <div class="clear"></div>
                <div class="content_top">
                    <div class="heading">
                        <h3>Productos Relacionados</h3>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <script type="text/javascript" src="Module/Home/Model/listAutocomplete.js" ></script>
        </div>
</section>