<?php
include_once($_SERVER['DOCUMENT_ROOT']."/productos/Module/Details/Model/DAOdetails.php");

@session_start();

switch ($_GET['op']) {
	case 'details':
		@$id=$_GET['id'];
		if (isset($_GET["detail"]) && $_GET["detail"] == true) {
			try {
				$rdo = DAOdetails::showDetails($id);//posar-ho en un try catch
				//print_r($rdo);
				//exit;
			}catch(Exception $e) {
				header('HTTP/1.0 400 Bad error');
	            exit;
			}
			if(!$rdo){
	            header('HTTP/1.0 400 Bad error');
	            exit;
	        }else{
	            $prod=get_object_vars($rdo);
	            @$_SESSION['datos']=$prod;
	            echo json_encode(@$_SESSION['datos']);
	            exit;
	        }
		
		}
        ///////// arrepleguem dades del prod ////////
	if (isset($_GET["load"]) && $_GET["load"] == true){
        $jsondata = array();
        //echo json_encode($_SESSION['datos']);
	    //exit;
        if (isset($_SESSION['datos'])) {
           	$jsondata = $_SESSION['datos'];

        }    
        
        echo json_encode($jsondata);
        exit;
    }
	break;
	case 'comentarios':
		//echo json_encode($_POST['comentario']);
		//exit;
		include_once($_SERVER['DOCUMENT_ROOT']."/productos/Utils/checkUser.php");
		if (isset($_POST['comentario'])) {
			$commentJson = json_decode(@$_POST["comentario"], true);
			$rdoUser = checkUser();
			$jsondata = array();

			if ($rdoUser['resultUser']){
				try{
					$rdo = DAOdetails::insertComment($_SESSION['name'],$commentJson['id_prod'],$commentJson['mensaje']);
				}catch(Exception $e){
					$jsondata['redirect']="index.php?module=503";
					header('HTTP/1.0 400 Bad error');
	            	exit;
				}

				if($rdo){
                    $jsondata['mensaje']= 'Su mensaje ha sido publicado satisfactoriamente';
                    echo json_encode($jsondata);
                    exit;
                }else{
                	$jsondata['redirect']="index.php?module=503";
                    header('HTTP/1.0 400 Bad error');
                    echo json_encode($jsondata);
                    exit;
                }
			}else{
				header('HTTP/1.0 400 Bad error');
				echo json_encode($rdoUser['error']);
	            exit;
			}
			

		}
		
	break;
	case 'getComentarios':
		//echo json_encode($_GET['idProd']);
		//exit;
		if (isset($_GET['idProd'])) {
			$jsondata = array();
			$display_json = array();

			try{
				$rdo = DAOdetails::getComments($_GET['idProd']);
			}catch(Exception $e){
				$jsondata['redirect']="index.php?module=503";
				header('HTTP/1.0 400 Bad error');
		        exit;
			}

			if($rdo){
				foreach ($rdo as $row) {
					$jsondata["id_usuario"] = $row['id_usuario'];
					$jsondata["mensaje"] = $row['mensaje'];
					$jsondata["comentario"] = $row['id_comentario'];
					array_push($display_json, $jsondata);
					
				}
	            
	            echo json_encode($display_json);
	            exit;
	        }else{
	            $jsondata['redirect']="index.php?module=503";
	            header('HTTP/1.0 400 Bad error');
	            echo json_encode($jsondata);
	            exit;
	        }
		}
		
		
		
	break;
	case 'getAvatar':
		if (isset($_POST['nameUsu'])) {
			$jsondata = array();
			$display_json = array();
			$UsuJson = json_decode(@$_POST["nameUsu"], true);

			try{
				$rdo = DAOdetails::getImg($UsuJson['nameUsu']);
			}catch(Exception $e){
				$jsondata['redirect']="index.php?module=503";
				header('HTTP/1.0 400 Bad error');
		        exit;
			}

			if($rdo){
				foreach ($rdo as $row) {
					$jsondata["id_usuario"] = $row['name'];
					$jsondata["avatar"] = $row['media'];
	
					array_push($display_json, $jsondata);
					
				}
	            
	            echo json_encode($display_json);
	            exit;
	        }else{
	            $jsondata['redirect']="index.php?module=503";
	            header('HTTP/1.0 400 Bad error');
	            echo json_encode($jsondata);
	            exit;
	        }
		}
		
		
		
	break;
	case 'like':
		if (isset($_POST['id'])) {
			//print_r($_POST['id']);
			//exit;
			$jsondata = array();
			if (isset($_SESSION['name'])) {
				
				$prodJson = json_decode($_POST["id"], true);

				try{
					$rdo = DAOdetails::insertLike($_SESSION['name'],$prodJson['id_prod']);

				}catch(Exception $e){
					$jsondata['redirect']="index.php?module=503";
					header('HTTP/1.0 400 Bad error');
			        exit;
				}

				if($rdo){
					$jsondata['id_prod'] = "Te gusta este producto";
		            echo json_encode($jsondata);
		            exit;

		        }else{
		            $jsondata['error']="Ya te gusta este producto";
		            header('HTTP/1.0 400 Bad error');
		            echo json_encode($jsondata);
		            exit;

		        }
		        
			}else{
				$jsondata['error']="Tienes que registrarte";
				header('HTTP/1.0 400 Bad error');
				echo json_encode($jsondata);
		        exit;

			}
			
		}
		
		
		
	break;
    
}

