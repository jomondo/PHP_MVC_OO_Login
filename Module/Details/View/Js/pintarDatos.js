function load_prods() {
    $.get("Module/Details/Controller/controller_Details.class.php?op=details&load=true", function (data) {
        //alert(data);
        var json = JSON.parse(data);
        //alert(json.prod.name);

        //crear widget accordion para comentarios
        $( function() {
            $( "#accordion" ).accordion({
              collapsible: true
            });
        });

        $("#imgDetail").append("<a href='#' target='_blank'><img src="+json.media+" alt=''/></a>");
        $("#nomDetail").append("<h2>"+json.name+"</h2>");
        $("#marca").append("<p>Marca: "+json.brand+"</p>");
        $("#price").append("<p class='price'>Precio: "+json.price+"</p>");
        $("#buttonComent").append("<input type='button' class='enviarComentario' id='"+json.id+"' value='Enviar' />");

        loadComments ();
        
        //button añadir al carrito
        $("#enviarCarrito").click(function(){
            loadCarrito(json);
        });

        //button añadir like
        $("#like").click(function(){
            //alert(json.id);
            var data = {"id_prod":json.id}
            var data_JSON = JSON.stringify(data);
            $.ajax({
                type: "POST",
                url: "Module/Details/Controller/controller_Details.class.php?op=like",
                data:{id:data_JSON},
                success: function(message){
                    alert(message);
                    //var json = JSON.parse(message);
                    //alert(json.msg);
                    
                    
                },

                error: function(xhr){
                    xhr.responseJSON = JSON.parse(xhr.responseText);
                    if (xhr.responseJSON.error){
                       //alert(xhr.responseJSON.error);
                        $.amaran({
                        'message'           :xhr.responseJSON.error,
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });

                    }
                    if (xhr.responseJSON.redirect){
                        window.location.href=xhr.responseJSON.redirect;
                                    
                    }
                }

            });

        });

    });
}

function loadComments (){
    var id_prod = $("input.enviarComentario").attr('id');
    $.ajax({
        type: "GET",
        url: "Module/Details/Controller/controller_Details.class.php?op=getComentarios&idProd="+id_prod,
        success: function(message){
            //alert(message);
            var json = JSON.parse(message);
            //alert(json);
            //var comments = $("div#comments").attr('id');
            if (json[0]===undefined) {
                $("#comentarios").append('<div id="comments" class="cuadro conf_cuadro">No hay comentarios en este producto. Sé el primero en comentar.</div>');
                return false;
            }
            json.forEach(function(elemento){
                $("#comentarios").append('<div id="comments" class="cuadro conf_cuadro"><div id="'+elemento.id_usuario+'"><div class="commentsUsu" id="'+elemento.comentario+'"></div></div><div >'+elemento.id_usuario+':</div><p class="comments">'+elemento.mensaje+'<p></div>');
                
                if (elemento.id_usuario) {
                    var data = {"nameUsu":elemento.id_usuario};
                    var data_value_JSON = JSON.stringify(data);
                    $.ajax({
                        type: "POST",
                        url: "Module/Details/Controller/controller_Details.class.php?op=getAvatar",
                        data:{nameUsu:data_value_JSON},
                        success: function(message){
                            //alert(message);
                            var json = JSON.parse(message);

                            json.forEach(function(elementos){
                                //alert($("div#"+elemento.comentario+"img.imgRedonda").length);
                                if ($("div#"+elemento.comentario+"img.imgRedonda").length==0) {
                                    $("div#"+elemento.comentario).append('<img class="imgRedonda" src="'+elementos.avatar+'" width="35px" height="35px"/>');
                                }
                                
                            });
                                        
                        },
                        error: function(xhr){
                            xhr.responseJSON = JSON.parse(xhr.responseText);
                            if (xhr.responseJSON.redirect){
                                window.location.href=xhr.responseJSON.redirect;
                                    
                            }
                        }

                    });
                }
                                   
            });
                      
        },
        error: function(xhr){
            xhr.responseJSON = JSON.parse(xhr.responseText);
            if (xhr.responseJSON.redirect){
                window.location.href=xhr.responseJSON.redirect;
                    
            }
        }

    });
}

function loadCarrito(data) {
    //alert(data.name);
    //pasar dades al controlador del carrito
    var cantidad = document.getElementById('comboCantidad').value;
    var data = {"id":data.id,"name":data.name,"brand":data.brand,"price":data.price,"media":data.media,"cantidad":cantidad};
    var data_value_JSON = JSON.stringify(data);
    console.log("json debug carrito: "+data_value_JSON);

    $.ajax({
        type: "POST",
        url: "Module/Carrito/Controller/controller_Carrito.class.php?op=listCarrito&carrito=true",
        data: {datosCarrito:data_value_JSON},
        success: function(data){
            //alert(data);
            var json = JSON.parse(data);
            window.location.href=json.redirect;
                       
        },
        error: function(){
            //alert("Errorloadcarrito");
            window.location.href='index.php?module=503';
        }

    });

}

$(document).ready(function () {
    load_prods();

});
