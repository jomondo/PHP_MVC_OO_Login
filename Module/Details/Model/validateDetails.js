$(document).ready(function () {
    $(document).on('click', '.ButtonDetails', function(event) {
        //alert("entra en script detail");
        var id = this.getAttribute('id');

        $.ajax({
            type: "GET",
            url: "Module/Details/Controller/controller_Details.class.php?op=details&detail=true&id=" + id,
            success: function(message){
                //alert(message);
                var json = JSON.parse(message);
                //alert(json.price);
                
                window.location.href="index.php?module=Details&view=DetailsProduct";
                
            },

            error: function(){
                //alert("Error_redirect");
                window.location.href='index.php?module=503';
            }

        });
    });//end click modal

    //necessitem el on al ser el button dinamic
    $(document).on('click', '.ButtonList', function(event) {
        //let id = this.id;
        var id = this.getAttribute('id');

        $.ajax({
            type: "GET",
            url: "Module/Details/Controller/controller_Details.class.php?op=details&detail=true&id=" + id,
            success: function(message){
                //alert(message);
                var json = JSON.parse(message);
                //alert(json.price);
                
                window.location.href="index.php?module=Details&view=DetailsProduct";
                
            },

            error: function(){
                //alert("Error");
                window.location.href='index.php?module=503';
            }

        });
    });

    //boton envia comentario
    $(document).on('click', '.enviarComentario', function(event) {
        var rdo = true;
        var descrireg = /^[A-Za-z0-9\sñÑ]{10,500}$/;

        if($("#description").val() == ""){
            $.amaran({
                'message'           :'Ingrese su descripcion',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
                
            rdo=false;
            return false;
        }else if(!descrireg.test($("#description").val())){
            $.amaran({
                'message'           :'La descripcion debe tener entre 10 y 500 caracteres.',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            rdo=false;
            return false;
        }
        
        if (rdo) {
            var id_prod = this.getAttribute('id');
            var mensaje = $('#description').val();
            var data = {"id_prod":id_prod,"mensaje":mensaje};
            var data_comment_JSON = JSON.stringify(data);

            $.ajax({
                type: "POST",
                url: "Module/Details/Controller/controller_Details.class.php?op=comentarios",
                data:{comentario:data_comment_JSON},
                success: function(message){
                    //alert(message);
                    $('#description').remove();
                    progressBar();
                    $("div#comments").remove();
                    loadComments ();
                    $('#textarea').append('<textarea cols="40" rows="8" id="description" name="description" value=""></textarea>');
                    
                },
                error: function(xhr){
                    xhr.responseJSON = JSON.parse(xhr.responseText);
                    
                    if (xhr.responseJSON.redirect){
                        window.location.href=xhr.responseJSON.redirect;
                    
                    }else if (xhr.responseJSON.name){
                        $.amaran({
                            'message'           :'Tienes que registrarte para poder comentar',
                            'cssanimationIn'    :'tada',
                            'cssanimationOut'   :'rollOut',
                            'closeButton'       :true,
                            'closeOnClick'      :true
                        });
                        return false;
                    }
                }

            });
        }
        
    });
    
});

// widget barra de progreso
function progressBar () {
    $( function() {
        var progressbar = $( "#progressbar" ),
        progressLabel = $( ".progress-label" );
             
        progressbar.progressbar({
            value: false,
            change: function() {
                progressLabel.text( progressbar.progressbar( "value" ) + "%" );
            },
            complete: function() {
                progressLabel.text( "Enviado!" );
            }
        });
             
        function progress() {
            var val = progressbar.progressbar( "value" ) || 0;
             
            progressbar.progressbar( "value", val + 8 );
             
            if ( val < 99 ) {
                setTimeout( progress, 80 );
            }
        }
             
        setTimeout( progress, 2000 );
    });
}