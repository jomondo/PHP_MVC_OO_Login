$(document).ready(function () {
	$("#btnDummies").click(function(){
		// name 	model 	 fabricationDate 	discounts 	color 	brand 	availability 	quantity 	agency 	price 	description
		data = { 
			"data1" : {"name": "Smartphone BQ", "model": "H4467", "fabricationDate": "20/07/2016", "discounts":"0","color":"black", "brand":"BQ", "availability": "Stock", "quantity": "45", "agency": "MRW",
					"price": "165", "description": "Buen Smartphone","media":"View/Imagenes/bq.jpg"},
			"data2" : {"name": "Memoria RAM KG", "model": "G4278", "fabricationDate": "26/07/2016", "discounts":"6","color":"gold", "brand":"Kingston", "availability": "Stock", "quantity": "45", "agency": "Correos",
					"price": "65", "description": "Buen Smartphone", "media":"View/Imagenes/ram.jpg"},
			"data3" : {"name": "Placa Base Dark", "model": "G9275", "fabricationDate": "26/07/2016", "discounts":"6","color":"red", "brand":"MSI", "availability": "Stock", "quantity": "45", "agency": "MRW",
					"price": "105", "description": "Buen Smartphone", "media":"View/Imagenes/placabase.jpg"},
			"data4" : {"name": "Ventiladores Cpu", "model": "V4578", "fabricationDate": "14/02/2017", "discounts":"0","color":"black", "brand":"Cooler Master", "availability": "Stock", "quantity": "22", "agency": "MRW",
					"price": "42", "description": "Buen Smartphone", "media":"View/Imagenes/ventiladorCpu.jpg"},
			"data5" : {"name": "Teclado cherry XONE", "model": "T4578", "fabricationDate": "11/03/2017", "discounts":"0","color":"white", "brand":"Cherry", "availability": "Fuera de stock", "quantity": "0", "agency": "MRW",
					"price": "65", "description": "Buen Tecladito", "media":"View/Imagenes/teclado.jpg"},
			"data6" : {"name": "Cpu i9 ultimate generation", "model": "C1578", "fabricationDate": "11/03/2017", "discounts":"0","color":"white", "brand":"Intel", "availability": "stock", "quantity": "70", "agency": "MRW",
					"price": "350", "description": "Buena cpu", "media":"View/Imagenes/cpu.jpg"},
			"data7" : {"name": "pantalla hd", "model": "K1578", "fabricationDate": "22/05/2017", "discounts":"0","color":"black", "brand":"Samsung", "availability": "stock", "quantity": "20", "agency": "MRW",
					"price": "285", "description": "Buena pantalla", "media":"View/Imagenes/pantalla.jpg"},
		};

		var data_prods_JSON = JSON.stringify(data);
		console.log("json debug: "+data_prods_JSON);

		$.post('Dummies/Controller/controllerDummies.php?op=createDummies',{alta_prods_json: data_prods_JSON},function (response){
		    alert(response);
		    var json = JSON.parse(response);//important!!
		    //alert(json.success);
		    console.log("success: "+json.success);
		    if (json.success) {
		        window.location.href = json.redirect;
		    }
		                
		}).fail(function(){
		    alert("error dummies");

		});//end fail

	});//end click

});//end read