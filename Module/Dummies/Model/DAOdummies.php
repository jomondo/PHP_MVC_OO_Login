<?php
include_once($_SERVER['DOCUMENT_ROOT']."/productos/Model/connect.php");

class DAOdummies{
	function insertDummies($cmponent){
		//print_r($cmponent['data1']['name']);
		//exit;
		// name 	model 	 fabricationDate 	discounts 	color 	brand 	availability 	quantity 	agency 	price 	description
		$data1=$cmponent['data1'];
		$data2=$cmponent['data2'];
		$data3=$cmponent['data3'];
		$data4=$cmponent['data4'];
		$data5=$cmponent['data5'];
		$data6=$cmponent['data6'];
		$data7=$cmponent['data7'];
		//print_r($data1);
		//exit;
        $sql =" INSERT INTO dummies (name, model, fabricationDate, discounts, color, brand, availability, quantity, agency, price, description, media)"
        	. " VALUES ('$data1[name]', '$data1[model]', '$data1[fabricationDate]', '$data1[discounts]', '$data1[color]', '$data1[brand]', '$data1[availability]', '$data1[quantity]','$data1[agency]', '$data1[price]', '$data1[description]','$data1[media]'),
        	('$data2[name]', '$data2[model]', '$data2[fabricationDate]', '$data2[discounts]', '$data2[color]', '$data2[brand]', '$data2[availability]', '$data2[quantity]','$data2[agency]', '$data2[price]', '$data2[description]','$data2[media]'),
        	('$data3[name]', '$data3[model]', '$data3[fabricationDate]', '$data3[discounts]', '$data3[color]', '$data3[brand]', '$data3[availability]', '$data3[quantity]','$data3[agency]', '$data3[price]', '$data3[description]','$data3[media]'),
        	('$data4[name]', '$data4[model]', '$data4[fabricationDate]', '$data4[discounts]', '$data4[color]', '$data4[brand]', '$data4[availability]', '$data4[quantity]','$data4[agency]', '$data4[price]', '$data4[description]','$data4[media]'),
        	('$data5[name]', '$data5[model]', '$data5[fabricationDate]', '$data5[discounts]', '$data5[color]', '$data5[brand]', '$data5[availability]', '$data5[quantity]','$data5[agency]', '$data5[price]', '$data5[description]','$data5[media]'),
        	('$data6[name]', '$data6[model]', '$data6[fabricationDate]', '$data6[discounts]', '$data6[color]', '$data6[brand]', '$data6[availability]', '$data6[quantity]','$data6[agency]', '$data6[price]', '$data6[description]','$data6[media]'),
        	('$data7[name]', '$data7[model]', '$data7[fabricationDate]', '$data7[discounts]', '$data7[color]', '$data7[brand]', '$data7[availability]', '$data7[quantity]','$data7[agency]', '$data7[price]', '$data7[description]','$data7[media]')";
            
        $conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);

		return $res;
	}
	
	function listDummies(){
		$sql = "SELECT * FROM cmpoinfrmatics LIMIT 10";
			
		$conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);
            
        return $res;
		
	}
}