<?php
	class connection{
		public static function mysqli(){
			$mysqli = new mysqli("127.0.0.1", "root", "", "crudcmpoinfrmatics");
			/* check connection */
			if (mysqli_connect_errno()){
				printf("Connect failed: %s\n", mysqli_connect_error());
				exit();
			}
			return $mysqli;
		}
		public static function close($mysqli){
			$mysqli->close();
		}
	}