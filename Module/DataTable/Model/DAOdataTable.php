<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/productos/Module/DataTable/Model/connection.php");
	
	class DAOdataTable{
		function count_rows($mysqli,$start, $pagesize){
			//echo '<script language="javascript">console.log("Entra rows")</script>';
			//exit;
			$query = "SELECT SQL_CALC_FOUND_ROWS name, brand, price, model FROM cmpoinfrmatics LIMIT ?, ?";
			$result = $mysqli->prepare($query);
			$result->bind_param('ii', $start, $pagesize);

			return $result;
		}		
		function count_rows_where($mysqli,$where){
            $query = "SELECT SQL_CALC_FOUND_ROWS name, brand, price, model FROM cmpoinfrmatics " . $where . " LIMIT ?, ?";
			$result = $mysqli->prepare($query);
            
            return $result;
		}
		
		function count_rows_sortfield($mysqli,$sortorder,$sortfield,$start, $pagesize){
            if ($sortorder == "desc"){
				$query = "SELECT SQL_CALC_FOUND_ROWS name, brand, price, model FROM cmpoinfrmatics ORDER BY" . " " . $sortfield . " DESC LIMIT ?, ?";
			}else if ($sortorder == "asc"){
				$query = "SELECT SQL_CALC_FOUND_ROWS name, brand, price, model FROM cmpoinfrmatics ORDER BY" . " " . $sortfield . " ASC LIMIT ?, ?";
			}
			$result = $mysqli->prepare($query);
			$result->bind_param('ii', $start, $pagesize);
            
            return $result;
		}

		function count_rows_filterquery($mysqli,$filterquery){
			
            $query = $filterquery;
			$result = $mysqli->prepare($query);

            return $result;
		}

		function sql_execute_orders($result){
            
            $result->execute();
			/* bind result variables */
			$result->bind_result($name, $brand, $price, $model);
			$orders = null;
			// get data and store in a json array
			while ($result->fetch()){
				$orders[] = array(
					'name' => $name,
					'brand' => $brand,
					'price' => $price,
					'model' => $model
				);
			}
			return $orders;
		}

		function found_rows($mysqli,$orders){
            $new_total_rows = 0;
            $result = $mysqli->prepare("SELECT FOUND_ROWS()");//numero de files
			$result->execute();
			$result->bind_result($total_rows);
			$result->fetch();

			if ($new_total_rows > 0) $total_rows = $new_total_rows;
				$data[] = array(
					'TotalRows' => $total_rows,
					'Rows' => $orders
			);

			$result->close();
			connection::close($mysqli);

            return $data;
		}
		
}