$(document).ready(function () {
	// prepare the data

	var source = {
		datatype: "json",
		datafields: [
			{ name: 'name', type: 'string'},
			{ name: 'brand', type: 'string' },
			{ name: 'price', type: 'float' },
			{ name: 'model', type: 'string' }
		],

		cache: false,
		url: 'Module/DataTable/Controller/controller_DataTable.class.php',
		filter: function(){
			// update the grid and send a request to the server.
			$("#jqxgrid").jqxGrid('updatebounddata', 'filter');
		},
		sort: function(){
			// update the grid and send a request to the server.
			$("#jqxgrid").jqxGrid('updatebounddata', 'sort');
		},
		root: 'Rows',
		beforeprocessing: function(data){		
			
			//alert(data);
			if (data != null){
				source.totalrecords = data[0].TotalRows;					
			}

		}
	};
				
	var dataadapter = new $.jqx.dataAdapter(source, {

		loadError: function(xhr, status, error){
			xhr.responseJSON = JSON.parse(xhr.responseText);
			if (xhr.responseJSON.redirect) {
                window.location.href = xhr.responseJSON.redirect;
            }
		}
	});
	
	// initialize jqxGrid
	$("#jqxgrid").jqxGrid({		
		source: dataadapter,
		filterable: true,
		sortable: true,
		autoheight: true,
		pageable: true,
		virtualmode: true,
		rendergridrows: function(obj){
			return obj.data;    
		},
		columns: [
			{ text: 'name', datafield: 'name', width: 200 },
			{ text: 'brand', datafield: 'brand', width: 180 },
			{ text: 'price', datafield: 'price', width: 100 },
			{ text: 'model', datafield: 'model'}
		]
	});
	
});//end ready