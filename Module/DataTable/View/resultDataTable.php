<!-- dataTable -->
    <link rel="stylesheet" href="View/lib/jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="View/lib/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxmenu.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxgrid.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxgrid.selection.js"></script>   
    <script type="text/javascript" src="View/lib/jqwidgets/jqxgrid.filter.js"></script>  
    <script type="text/javascript" src="View/lib/jqwidgets/jqxgrid.sort.js"></script>        
    <script type="text/javascript" src="View/lib/jqwidgets/jqxdata.js"></script> 
    <script type="text/javascript" src="View/lib/jqwidgets/jqxlistbox.js"></script>  
    <script type="text/javascript" src="View/lib/jqwidgets/jqxgrid.pager.js"></script>       
    <script type="text/javascript" src="View/lib/jqwidgets/jqxdropdownlist.js"></script> 
    <script type="text/javascript" src="Module/DataTable/Model/dataTable.js"></script>
    <!-- end dataTable -->

    <section class="wrap">
        <h3>LISTA PRODUCTOS </h3>
        <div id="jqxgrid"></div>
    </section>
    