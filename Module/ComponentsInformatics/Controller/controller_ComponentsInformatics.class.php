<?php
    include_once($_SERVER['DOCUMENT_ROOT']."/productos/Module/ComponentsInformatics/Model/DAOCmpoInfrmatics.php");
    include_once($_SERVER['DOCUMENT_ROOT']."/productos/Module/Dummies/Model/DAOdummies.php");
    @session_start();
    switch($_GET['op']){
        case 'list';//admin
            try{
                echo '<script language="javascript">console.log("Entra en list try")</script>';
                $rdo= DAOCmpoInfrmatics::showCmponents();
            }catch (Exception $e){
                $jsondata['redirect']="index.php?module=503";
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;

            }
            
            if(!$rdo){
			    $jsondata['redirect']="index.php?module=503";
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;

            }else
                include("Module/ComponentsInformatics/View/listAllCmpts.php");

        break;

        case 'create';
            include($_SERVER['DOCUMENT_ROOT']."/productos/Module/ComponentsInformatics/Model/validateServer.php");
            include($_SERVER['DOCUMENT_ROOT']."/productos/Module/ComponentsInformatics/Model/checkName.php");
            
            if ((isset($_POST['alta_prods_json']))) {
                //echo json_encode("entra en alta_prods_json");
                //exit;

                $jsondata = array();
                $prodJson = json_decode($_POST["alta_prods_json"], true);
                $rdophp=validateProdPhp($prodJson);
                //print_r($rdophp);
                //exit;
                $rdoNamephp=validateNamePhp($rdophp['datos']['name']);
                //echo json_encode($rdoNamephp);
                //exit;

                if ($rdophp['result'] && $rdoNamephp['result']){
                    //echo '<script language="javascript">console.log("Ha sigut validat jquery i php")</script>';
                    //exit;
                    @$_SESSION['prod']=$rdophp['datos'];
                    try{
                        $rdo = DAOCmpoInfrmatics::newCmponent($rdophp['datos']);
                    }catch (Exception $e){
                        $jsondata['redirect']="index.php?module=503";//cuidaet la redireccio!!!!
                        header('HTTP/1.0 400 Bad error');
                        echo json_encode($jsondata);
                        exit;
                    }
                        
                    if($rdo){
                        $jsondata["success"] = true;
                        $jsondata['redirect']= 'index.php?module=ComponentsInformatics&view=resultsComponents';
                        echo json_encode($jsondata);
                        exit;
                    }else{
                        $jsondata['redirect']="index.php?module=503";
                        header('HTTP/1.0 400 Bad error');
                        echo json_encode($jsondata);
                        exit;
                    }
                }else{
                    $jsondata["error"] = $rdophp['error'];
                    $jsondata["error1"] = $rdoNamephp['error'];
                    header('HTTP/1.0 400 Bad error');
                    echo json_encode($jsondata);
                    exit;
                }

            }

            //////////////////////////// load prod to list_prod///////////////////////////
            if (isset($_GET["load"]) && $_GET["load"] == true){
                $jsondata = array();
                if (isset($_SESSION['prod'])) {
                    $jsondata["prod"] = $_SESSION['prod'];
                }
                
                echo json_encode($jsondata);
                exit;
            }

            include($_SERVER['DOCUMENT_ROOT']."/productos/Module/ComponentsInformatics/View/createComponents.php");
        break;
        case 'readModal';
            $id=$_GET['modal'];
            try{
                $rdo = DAOCmpoInfrmatics::showComponent($id);

            }catch (Exception $e){
                header('HTTP/1.0 400 Bad error');
                exit;
            }
            if(!$rdo){
                header('HTTP/1.0 400 Bad error');
                exit;
            }else{
                $prod=get_object_vars($rdo);
                echo json_encode($prod);
                exit;
            }
            
            
        break;

        case 'update';
            include($_SERVER['DOCUMENT_ROOT']."/productos/Module/ComponentsInformatics/Model/validateServer.php");
            @$id=$_GET['id'];
            //print_r($id);
            //exit;
            $jsondata = array();

            if (isset($_GET["datos"]) && $_GET["datos"] == true) {
                try{
                    $rdo = DAOCmpoInfrmatics::showComponent($id);
                    $fila=get_object_vars($rdo);
                }catch (Exception $e){
                    $jsondata['redirect']="index.php?module=503";
                    header('HTTP/1.0 400 Bad error');
                    echo json_encode($jsondata);
                    exit;
                }

                if(!$rdo){
                    $jsondata['redirect']="index.php?module=503";
                    header('HTTP/1.0 400 Bad error');
                    echo json_encode($jsondata);
                    exit;
                }
            }
                
            if (isset($_GET["datosUp"]) && $_GET["datosUp"] == true) {
                $prodJson = json_decode(@$_POST["alta_prods_json"], true);
                $rdophp=validateProdPhp($prodJson);

                if ($rdophp['result']){
                    try{
                        $rdoupdate = DAOCmpoInfrmatics::updateCmponents($rdophp['datos'],$prodJson['id']);
                    }catch (Exception $e){
                        $jsondata['redirect']="index.php?module=503";
                        header('HTTP/1.0 400 Bad error');
                        echo json_encode($jsondata);
                        exit;
                    }
                        
                    if($rdoupdate){
                        $jsondata["success"] = true;
                        $jsondata['redirect']= 'index.php?module=ComponentsInformatics&op=list';
                        echo json_encode($jsondata);
                        exit;
                    }else{
                        $jsondata['redirect']="index.php?module=503";
                        header('HTTP/1.0 400 Bad error');
                        echo json_encode($jsondata);
                        exit;
                    }
                        
                }else{
                    $jsondata["error"] = $rdophp['error'];
                    header('HTTP/1.0 400 Bad error');
                    echo json_encode($jsondata);
                    exit;
                }
            }   

            include($_SERVER['DOCUMENT_ROOT']."/productos/Module/ComponentsInformatics/View/updateComponents.php");
        break;

        case 'deleteModal';
            $id=$_GET['modal'];
            if(isset($_GET["name"]) && $_GET["name"] == true){
                try{
                    $rdo = DAOCmpoInfrmatics::showComponent($id);

                }catch (Exception $e){
                    header('HTTP/1.0 400 Bad error');
                    exit;
                }
                if(!$rdo){
                    header('HTTP/1.0 400 Bad error');
                    exit;
                }else{
                    $prod=get_object_vars($rdo);
                    echo json_encode($prod);
                    exit;
                }
            }

            if(isset($_GET["delete"]) && $_GET["delete"] == true){
                try{
                    $rdoDelete = DAOCmpoInfrmatics::deleteComponents($id);

                }catch (Exception $e){
                    header('HTTP/1.0 400 Bad error');
                    exit;
                }
                if(!$rdoDelete){
                    header('HTTP/1.0 400 Bad error');
                    exit;
                }else{
                    $callback = 'index.php?module=ComponentsInformatics&op=list';
                    echo json_encode($callback);
                    exit;
                    
                }
                 
            }
            
        break;
        default;
            include("Module/404/Controller/controller_404.class.php");
        break;
    }
    