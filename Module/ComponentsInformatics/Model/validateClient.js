
    $(document).ready(function () {

        var namereg = /^[A-Za-z\s]{4,15}$/;
        var modelreg = /^[A-Z]{1}[0-9]{4}$/;
        var discounts = /^[0-9]{1,2}$/;//100% ??
        var datereg = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
        var quantityreg = /^[0-9]{1,2}$/;
        var pricereg = /^[0-9]{1,3}$/;
        var descrireg = /^[A-Za-z0-9\sñÑ]{1,55}$/;

        //fadeOuts errors
            $("#name").keyup(function(){
                if ( $(this).val() != "" && namereg.test($(this).val()) ){
                    $(".error").fadeOut("slow");
                    return false;
                }
            });
            
            $("#model").keyup(function(){   
                if ( $(this).val() != ""  && modelreg.test($(this).val()) ){
                    $(".error").fadeOut("slow");
                    return false;
                }
            });

            $("#fabricationDate").keyup(function(){
                if ( $(this).val() != ""  && datereg.test($(this).val()) ){
                    $(".error").fadeOut("slow");
                    return false;
                }
            });

            $("#discounts").keyup(function(){
                if ( $(this).val() != ""  && quantityreg.test($(this).val()) ){
                    $(".error").fadeOut("slow");
                    return false;
                }
            });

            $("#quantity").keyup(function(){
                if ( $(this).val() != ""  && quantityreg.test($(this).val()) ){
                    $(".error").fadeOut("slow");
                    return false;
                }
            });

            $("#price").keyup(function(){
                if ( $(this).val() != ""  && pricereg.test($(this).val()) ){
                    $(".error").fadeOut("slow");
                    return false;
                }
            });

            $("#description").keyup(function(){
                if ( $(this).val() != ""  && descrireg.test($(this).val()) ){
                    $(".error").fadeOut("slow");
                    return false;
                }
            });
        //var rdo = true;
        var cont = 0;
        $("#create").click(function(){
            var rdo = true;
            
            var namereg = /^[A-Za-z\s]{4,15}$/;
            var modelreg = /^[A-Z]{1}[0-9]{4}$/;
            var datereg = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
            var quantityreg = /^[0-9]{1,2}$/;
            var pricereg = /^[0-9]{1,3}$/;
            var discountsreg = /^[0-9]{1,2}$/;
            var descrireg = /^[A-Za-z0-9\sñÑ]{1,55}$/;
            
            var name = document.getElementById('name').value;
            var model = document.getElementById('model').value;
            var fabricationDate = document.getElementById('fabricationDate').value;
            var discounts = document.getElementById('discounts').value;
            var color = $(".color").jqxDropDownList('val');
            var brand = $(".brand").jqxDropDownList('val');
            var tipo = $(".tipo").jqxDropDownList('val');
            let availability = $('input[name="availability"]:checked').val();
            var quantity = document.getElementById('quantity').value;
            var agency = [];
            var inputElements = document.getElementsByClassName('envioCheckbox');
            var price = document.getElementById('price').value;
            var description = document.getElementById('description').value;
            var j = 0;
            for (var i = 0; i < inputElements.length; i++) {
                if (inputElements[i].checked) {
                    agency[j] = inputElements[i].value;
                    j++;
                }
            }

            $(".error").remove();
            //comprovem camps buits i patterns

            if( $("#name").val() == ""){
                $("#name").focus().after("<span class='error'>Ingrese su nombre</span>");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }else if(!namereg.test($("#name").val())){
                $("#name").focus().after("<span class='error'>Mínimo 4 carácteres y maximo 15 para el nombre.</span>");
                cont++;
                rdo=false;
                return false;
            }if( $("#model").val() == "" ){
                $("#model").focus().after("<span class='error'>Ingrese su model</span>");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }else if(!modelreg.test($("#model").val())){
                $("#model").focus().after("<span class='error'>1 letra mayuscula y 4 numeros.</span>");
                rdo=false;
                return false;
            }if( $("#fabricationDate").val() == ""){
                $("#fabricationDate").focus().after("<span class='error'>Ingrese su fecha de fabricacion</span>");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }else if(!datereg.test($("#fabricationDate").val())){
                $("#fabricationDate").focus().after("<span class='error'>date incorrect(dd/mm/aaaa)</span>");
                cont++;
                rdo=false;
                return false;
            }if( $("#discounts").val() == ""){
                $("#discounts").focus().after("<span class='error'>Ingrese su descuento</span>");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }else if(!discountsreg.test($("#discounts").val())){
                $("#discounts").focus().after("<span class='error'>2 numeros como maximo.</span>");
                cont++;
                rdo=false;
                return false;
            }if( $(".color").val() == "" ){
                //console.log($("#color option:selected").text());
                $(".color").focus().after("<span class='error'>Seleccione un color</span>");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }if( $(".brand").val() == "" ){
                $(".brand").focus().after("<span class='error'>Seleccione una marca</span>");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }if( $(".tipo").val() == "" ){
                $(".tipo").focus().after("<span class='error'>Seleccione un tipo</span>");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }if( $("#availability").val() == "" ){
                $("#availability").focus().after("<span class='error'>Seleccione una disponibilidad</span>");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }if( $("#quantity").val() == ""){
                $("#quantity").focus().after("<span class='error'>Ingrese su cantidad</span>");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }else if(!quantityreg.test($("#quantity").val())){
                $("#quantity").focus().after("<span class='error'>La cantidad esta entre 1 o 2 numeros.</span>");
                cont++;
                rdo=false;
                return false;
            }if( $("#agency").val() == "" ){
                $("#agency").focus().after("<span class='error'>Seleccione una agencia</span>");//que salte en validateServer
                //console.log("entra en false tipo");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }if($("#price").val() == ""){
                $("#price").focus().after("<span class='error'>Ingrese su precio</span>");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }else if(!pricereg.test($("#price").val())){
                $("#price").focus().after("<span class='error'>El precio esta entre 1 o 3 numeros.</span>");
                cont++;
                rdo=false;
                return false;
            }if( $("#description").val() == ""){
                $("#description").focus().after("<span class='error'>Ingrese su descripcion</span>");
                cont++;
                if(cont>3) {
                    $.amaran({
                        'message'           :'Cuidado! Fijate mejor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                }
                rdo=false;
                return false;
            }else if(!descrireg.test($("#description").val())){
                $("#description").focus().after("<span class='error'>La descripcion debe tener entre 1 y 55 caracteres.</span>");
                cont++;
                rdo=false;
                return false;
            }
     
            if (rdo) {
            console.log("el rdo es true en js");
            var data={"name":name,"model":model,"fabricationDate":fabricationDate,"discounts":discounts,"color":color,"brand":brand,"tipo":tipo,"availability":availability, 
                    "quantity":quantity,"agency":agency,"price":price,"description":description};
            var data_prods_JSON = JSON.stringify(data);
            console.log("json debug: "+data_prods_JSON);

            $.post('Module/ComponentsInformatics/Controller/controller_ComponentsInformatics.class.php?op=create',{alta_prods_json: data_prods_JSON},function (response){
                alert(response);
                var json = JSON.parse(response);//important!!
                //alert(json.success);
                //alert(data_prods_JSON);
                console.log("success: "+json.success);
                if (json.success) {
                    window.location.href = json.redirect;
                }
                
               }).fail(function(xhr){
                    xhr.responseJSON = JSON.parse(xhr.responseText);
                    $.amaran({
                        'message'           :'Ha habido algun error en el servidor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });

                    if (xhr.responseJSON.error.name){
                        $("#name").focus().after("<span  class='error1'>" + xhr.responseJSON.error.name + "</span>");
                        
                    }
                    if (xhr.responseJSON.error1.name){
                        $("#name").focus().after("<span  class='error1'>" + xhr.responseJSON.error1.name + "</span>");
                        $("#name").keyup(function(){   
                            $(".error1").fadeOut("slow");
                        });
                        
                    }

                    if (xhr.responseJSON.error.model){
                        $("#model").focus().after("<span  class='error1'>"+xhr.responseJSON.error.model+"</span>");
                     
                    }
                    if (xhr.responseJSON.error.fabricationDate){
                        $("#fabricationDate").focus().after("<span  class='error1'>" + xhr.responseJSON.error.fabricationDate + "</span>");
                        
                    }
                    if (xhr.responseJSON.error.discounts){
                        $("#discounts").focus().after("<span  class='error1'>" + xhr.responseJSON.error.discounts + "</span>");
                        
                    }
                    if (xhr.responseJSON.error.color){
                        $(".color").focus().after("<span  class='error1'>" + xhr.responseJSON.error.color + "</span>");
                        
                    }

                    if (xhr.responseJSON.error.brand){
                        console.log("entra php error brand: "+xhr.responseJSON.error.brand);
                        $(".brand").focus().after("<span  class='error1'>" + xhr.responseJSON.error.brand + "</span>");
                        
                    }

                    if (xhr.responseJSON.error.tipo){
                        console.log("entra php error tipo: "+xhr.responseJSON.error.tipo);
                        $(".tipo").focus().after("<span  class='error1'>" + xhr.responseJSON.error.tipo + "</span>");
                        
                    }

                    if (xhr.responseJSON.error.availability){
                        $("#availability").focus().after("<span  class='error1'>" + xhr.responseJSON.error.availability + "</span>");
                        
                    }

                    if (xhr.responseJSON.error.quantity){
                        $("#quantity").focus().after("<span  class='error1'>" + xhr.responseJSON.error.quantity + "</span>");
                        
                    }
                    if (xhr.responseJSON.error.agency){
                        $("#error_agency").focus().after("<span  class='error1'>" + xhr.responseJSON.error.agency + "</span>");
                        
                    }

                    if (xhr.responseJSON.error.price){
                        $("#price").focus().after("<span class='error1'>" + xhr.responseJSON.error.price + "</span>");
                        
                    }

                    if (xhr.responseJSON.error.description){
                        $("#description").focus().after("<span  class='error1'>" + xhr.responseJSON.error.description + "</span>");
                        
                    }
                    if (xhr.responseJSON.redirect) {
                        window.location.href = xhr.responseJSON.redirect;
                    }


                });//end fail

            }else{
               
            }

        });//end click

        $("#update").click(function(){
            var rdo = true;

            var namereg = /^[A-Za-z\s]{4,15}$/;
            var modelreg = /^[A-Z]{1}[0-9]{4}$/;
            var datereg = /^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/;
            var quantityreg = /^[0-9]{1,2}$/;
            var pricereg = /^[0-9]{1,3}$/;
            var discountsreg = /^[0-9]{1,2}$/;
            var descrireg = /^[A-Za-z0-9\sñÑ]{1,55}$/;
            
            var name = document.getElementById('name').value;
            var model = document.getElementById('model').value;
            var fabricationDate = document.getElementById('fabricationDate').value;
            var discounts = document.getElementById('discounts').value;
            var color = $(".colorUp").jqxDropDownList('val');
            var brand = $(".brandUp").jqxDropDownList('val');
            var tipo = $(".tipoUp").jqxDropDownList('val');
            let availability = $('input[name="availability"]:checked').val();
            var quantity = document.getElementById('quantity').value;
            var agency = [];
            var inputElements = document.getElementsByClassName('envioCheckbox');
            var price = document.getElementById('price').value;
            var description = document.getElementById('description').value;
            var j = 0;
            for (var i = 0; i < inputElements.length; i++) {
                if (inputElements[i].checked) {
                    agency[j] = inputElements[i].value;
                    j++;
                }
            }
            var id = $("div.id").attr('id');
            //alert(id);

            $(".error").remove();
            //comprovem camps buits i patterns

            if( $("#name").val() == ""){
                $("#name").focus().after("<span class='error'>Ingrese su nombre</span>");
                rdo=false;
                return false;
            }else if(!namereg.test($("#name").val())){
                $("#name").focus().after("<span class='error'>Mínimo 4 carácteres y maximo 15 para el nombre.</span>");
                rdo=false;
                return false;
            }if( $("#model").val() == "" ){
                $("#model").focus().after("<span class='error'>Ingrese su model</span>");
                rdo=false;
                return false;
            }else if(!modelreg.test($("#model").val())){
                $("#model").focus().after("<span class='error'>1 letra mayuscula y 4 numeros.</span>");
                rdo=false;
                return false;
            }if( $("#fabricationDate").val() == ""){
                $("#fabricationDate").focus().after("<span class='error'>Ingrese su model</span>");
                rdo=false;
                return false;
            }else if(!datereg.test($("#fabricationDate").val())){
                $("#fabricationDate").focus().after("<span class='error'>date incorrect(dd/mm/aaaa)</span>");
                rdo=false;
                return false;
            }if( $("#discounts").val() == "" ){
                $("#discounts").focus().after("<span class='error'>Ingrese su descuento</span>");
                rdo=false;
                return false;
            }else if(!discountsreg.test($("#discounts").val())){
                $("#discounts").focus().after("<span class='error'>2 numeros como maximo.</span>");
                rdo=false;
                return false;
            }if( $(".colorUp").val() == "" ){
                $(".colorUp").focus().after("<span class='error'>Seleccione un color</span>");
                rdo=false;
                return false;
            }if( $(".brandUp").val() == "" ){
                $(".brandUp").focus().after("<span class='error'>Seleccione una marca</span>");
                rdo=false;
                return false;
            }if( $(".tipoUp").val() == "" ){
                $(".tipoUp").focus().after("<span class='error'>Seleccione un tipo</span>");
                rdo=false;
                return false;
            }if( $("#availability").val() == "" ){
                $("#availability").focus().after("<span class='error'>Seleccione una disponibilidad</span>");
                rdo=false;
                return false;
            }if( $("#quantity").val() == "" ){
                $("#quantity").focus().after("<span class='error'>Ingrese su cantidad</span>");
                rdo=false;
                return false;
            }else if(!quantityreg.test($("#quantity").val())){
                $("#quantity").focus().after("<span class='error'>La cantidad esta entre 1 o 2 numeros.</span>");
                rdo=false;
                return false;
            }if( $("#agency").val() == "" ){
                $("#agency").focus().after("<span class='error'>Seleccione una agencia</span>");
                rdo=false;
                return false;
            }if($("#price").val() == "" ){
                $("#price").focus().after("<span class='error'>Ingrese su precio</span>");
                rdo=false;
                return false;
            }else if(!pricereg.test($("#price").val())){
                $("#price").focus().after("<span class='error'>El precio esta entre 1 o 3 numeros.</span>");
                rdo=false;
                return false;
            }if( $("#description").val() == ""){
                $("#description").focus().after("<span class='error'>Ingrese su descripcion</span>");
                rdo=false;
                return false;
            }else if(!descrireg.test($("#description").val())){
                $("#description").focus().after("<span class='error'>La descripcion debe tener entre 1 y 55 caracteres.</span>");
                rdo=false;
                return false;
            }

            if (rdo) {
                console.log("el rdo es true en js");
                var data={"id":id,"name":name,"model":model,"fabricationDate":fabricationDate,"discounts":discounts,"color":color,"brand":brand,"tipo":tipo,"availability":availability, 
                        "quantity":quantity,"agency":agency,"price":price,"description":description};
    
                var data_prods_JSON = JSON.stringify(data);
                console.log("json debug: "+data_prods_JSON);

                $.post('Module/ComponentsInformatics/Controller/controller_ComponentsInformatics.class.php?op=update&datosUp=true',{alta_prods_json: data_prods_JSON},function (response){
                    alert(response);
                    var json = JSON.parse(response);//important!!
                    //alert(json.success);
                    console.log("success: "+json.success);
                    if (json.success) {
                        window.location.href = json.redirect;
                    }else{
                        window.location.href = json.redirectError;
                    }
                    
                   }).fail(function(xhr){
                        xhr.responseJSON = JSON.parse(xhr.responseText);
                        $.amaran({
                            'message'           :'Ha habido algun error en el servidor',
                            'cssanimationIn'    :'tada',
                            'cssanimationOut'   :'rollOut',
                            'closeButton'       :true,
                            'closeOnClick'      :true
                        });
                        if (xhr.responseJSON.error.name){
                            $("#name").focus().after("<span  class='error1'>" + xhr.responseJSON.error.name + "</span>");
                            
                        }

                        if (xhr.responseJSON.error.model){
                            $("#model").focus().after("<span  class='error1'>"+xhr.responseJSON.error.model+"</span>");
                         
                        }
                        if (xhr.responseJSON.error.fabricationDate){
                            $("#fabricationDate").focus().after("<span  class='error1'>" + xhr.responseJSON.error.fabricationDate + "</span>");
                            
                        }
                        if (xhr.responseJSON.error.discounts){
                            $("#discounts").focus().after("<span  class='error1'>" + xhr.responseJSON.error.discounts + "</span>");
                            
                        }
                        if (xhr.responseJSON.error.color){
                            $(".colorUp").focus().after("<span  class='error1'>" + xhr.responseJSON.error.color + "</span>");
                            
                        }

                        if (xhr.responseJSON.error.brand){
                            console.log("entra php error brand: "+xhr.responseJSON.error.brand);
                            $(".brandUp").focus().after("<span  class='error1'>" + xhr.responseJSON.error.brand + "</span>");
                            
                        }

                        if (xhr.responseJSON.error.tipo){
                            console.log("entra php error tipo: "+xhr.responseJSON.error.tipo);
                            $(".tipoUp").focus().after("<span  class='error1'>" + xhr.responseJSON.error.tipo + "</span>");
                            
                        }

                        if (xhr.responseJSON.error.availability){
                            $("#availability").focus().after("<span  class='error1'>" + xhr.responseJSON.error.availability + "</span>");
                            
                        }

                        if (xhr.responseJSON.error.quantity){
                            $("#quantity").focus().after("<span  class='error1'>" + xhr.responseJSON.error.quantity + "</span>");
                            
                        }
                        if (xhr.responseJSON.error.agency){
                            $("#error_agency").focus().after("<span  class='error1'>" + xhr.responseJSON.error.agency + "</span>");
                            
                        }

                        if (xhr.responseJSON.error.price){
                            $("#price").focus().after("<span class='error1'>" + xhr.responseJSON.error.price + "</span>");
                            
                        }

                        if (xhr.responseJSON.error.description){
                            $("#description").focus().after("<span  class='error1'>" + xhr.responseJSON.error.description + "</span>");
                            
                        }
                        if (xhr.responseJSON.redirect) {
                            window.location.href = xhr.responseJSON.redirect;
                        }

                    });//end fail
            }//end rdo

        });//end click
    
    });//end read

$(document).ready(function () {
    ////////// details Modal ////////////////
    $(".Button_blue").click(function(){
        var id = this.getAttribute('id');

        $.ajax({
            type: "GET",
            url: "Module/ComponentsInformatics/Controller/controller_ComponentsInformatics.class.php?op=readModal&modal=" + id,

            success: function(message){
                alert(message);
                var json = JSON.parse(message);

                $("#name").html(json.name);
                $("#model").html(json.model);
                $("#fabricationDate").html(json.fabricationDate);
                $("#discounts").html(json.discounts);
                $("#color").html(json.color);
                $("#brand").html(json.brand);
                $("#tipo").html(json.tipo);
                $("#availability").html(json.availability);
                $("#quantity").html(json.quantity);
                $("#agency").html(json.agency);
                $("#price").html(json.price);
                $("#description").html(json.description);
                       
                $("#modal-dialog").dialog({
                    title: "Informacion del componente",
                    width: 850,
                    height: 450,
                    modal: "true",
                    buttons: { 
                        Close: function () { 
                            $(this).dialog("close"); 
                        },
                        Update: function () { 
                            window.location.href="index.php?module=ComponentsInformatics&op=update&datos=true&id="+id; 
                        } 
                    },
                    show: "fold",
                    hide: "scale"                    
                });
            },

            error: function(){
                //alert("Error_readModal");
                window.location.href='index.php?module=503';
            }

        });
    });//end click modal


    /////////// delete Modal /////////////
    $(".Button_red").click(function(){
        var id = this.getAttribute('id');

       $.ajax({
            type: "GET",
            url: "Module/ComponentsInformatics/Controller/controller_ComponentsInformatics.class.php?op=deleteModal&name=true&modal=" + id,

            success: function(message){
                alert(message);
                var json = JSON.parse(message);
                
                $("#name1").html(json.name);
                       
                $( function() {
                    $.amaran({
                        'message'           :'Cuidado! Si le das a aceptar se borrara el producto',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });

                    $("#dialog-confirm").dialog({
                        height: "auto",
                        width: "auto",
                        modal: true,
                        buttons: {
                            "Aceptar": function() {
                                $.ajax({
                                    type: "GET",
                                    url: "Module/ComponentsInformatics/Controller/controller_ComponentsInformatics.class.php?op=deleteModal&delete=true&modal=" + id,
                                    success: function(message){
                                        alert(message);
                                        $("#dialog-message").dialog({
                                            modal: true,
                                            buttons:{
                                                Ok: function(){
                                                    $(this).dialog("close");
                                                    var json = JSON.parse(message);
                                                    window.location.href=json;
                                                    
                                                }
                                            },
                                            
                                        });
                                         
                                    },
                                    error: function(){
                                        alert("Error");
                                        window.location.href='index.php?module=503';
                                    }
                                });

                            },
                            Cancel: function() {
                                $( this ).dialog( "close" );
                            }
                        
                        },
                      
                    });
                });
            },
            
            error: function(){
                alert("Error");
                window.location.href='index.php?module=503';
            }

        });

    });//end click modal

    ////// datepicker //////////
    $('#fabricationDate').datepicker({
        dateFormat: 'dd/mm/yy',
        defaultDate: '01/01/2017', 
        changeMonth: true, 
        changeYear: true, 
        yearRange: '1995:2017',
    });
           

});//end read