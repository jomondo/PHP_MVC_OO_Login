<?php
    function validateProdPhp($value) {
    //echo '<script language="javascript">console.log("Entra en validateProdPhp")</script>';
    $error = array();
    $rdo = true;
    $filtro = array(
        'name' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[A-Za-z\s]{4,15}$/')
        ),
        'model' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[A-Z]{1}[0-9]{4}$/')
        ),
        'fabricationDate' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^([012][1-9]|3[01])(\/)(0[1-9]|1[012])\2(\d{4})$/')
        ),
        'discounts' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9]{1,2}$/')
        ),
        'quantity' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9]{1,2}$/')
        ),
        'price' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9]{1,4}$/')
        ),
        'description' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-zA-Z0-9\sñÑ]{1,55}$/')
        ),
    );

    $result = filter_var_array($value, $filtro);
    //print_r($result);
    //exit;

    if ($result != "" && $result) {
       // echo '<script language="javascript">console.log("Entra en resultado true")</script>';
        $result['color']=$value['color'];
        //print_r("estem en validateProdPhp".$result['color']);
        //exit;
        $result['brand']=$value['brand'];
        $result['tipo']=$value['tipo'];
        $result['availability']=$value['availability'];
        $result['agency']=$value['agency'];

        if (!$result['name']) {
            $error['name'] = 'Name must be 4 to 15 letters';
            $rdo = false;
            echo '<script language="javascript">console.log("nom incorrecte php")</script>';
        }

        if (!$result['model']) {
            $error['model'] = 'model must be 1 one letter and 4 numbers';
            $rdo = false;
        }

        if (!$result['fabricationDate']) {
            $error['fabricationDate'] = 'error format date (dd/mm/aaaa)';
            $rdo = false;
        }

        if ($result['color']=="") {
            echo '<script language="javascript">console.log("color no seleccionat")</script>';
            $error['color'] = 'Select colour';
            $rdo = false;
        }

        if (count($result['agency']) <= 1) {
            $error['agency'] = "Select 2 or more.";
            $rdo =  false;
        }

        if ($result['brand']=="") {
            $error['brand'] = "Select brand";
            $rdo = false;
        }

        if ($result['tipo']=="") {
            $error['tipo'] = "Select tipo";
            $rdo = false;
        }

        if ($result['availability']=="") {
            $error['availability'] = 'Select availability';
            $rdo = false;
        }
        if (!$result['quantity']) {
            $error['quantity'] = 'quantity must be 1 or 2 numbers';
            $rdo = false;
        }
        
        if (!$result['price']) {
            $error['price'] = 'price must be 1 or 4 numbers';
            $rdo = false;
        }

        if (!$result['description']) {
            $error['description'] = 'description must be 1 to 55 caracters';
            $rdo = false;
        }
        
    }else {
        $rdo = false;
    };

    return $return = array('result' => $rdo, 'error' => $error, 'datos' => $result);
}



    

