    <link rel="stylesheet" href="View/lib/jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="View/lib/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxsplitter.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxdata.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="Module/ComponentsInformatics/View/Js/dropdownlist.js"></script>

    <section class="wrap">
        <form autocomplete="on" method="post" id="form_prod" name="form_prod">
            <h1>Insert news components</h1>
            <table>
                <tr>
                    <td>Name: </td>
                    <td><input type="text" id="name" name="name" placeholder="write name" value=""/></td>
                </tr>
            
                <tr>
                    <td>Model: </td>
                    <td><input type="texto" id="model" name="model" placeholder="write model" value=""/></td>
                </tr>
                
                <tr>
                    <td>Fabrication Date: </td>
                    <td><input id="fabricationDate" type="text" name="fabricationDate" placeholder="dd/mm/yyyy" value=""/></td>
                </tr>

                <tr>
                    <td>Discounts: </td>
                    <td><input type="text" id="discounts" name="discounts" placeholder="write discounts" value=""/></td>
                </tr>

                <tr>
                    <td>Color:</td>
                    <td><div name="list" id="dropdownlist" class="color"></div></td>   
                </tr>
                
                <tr>
                    <td>Brand:</td>
                    <td><div name="list" id="dropdownlist1" class="brand"></div></td>   
                </tr>

                <tr>
                    <td>Tipo:</td>
                    <td><div name="list" id="dropdownlist2" class="tipo"></div></td>   
                </tr>

                <tr>
                    <td>Availability: </td>
                    <td>
                        <input type="radio" id="availability" name="availability" placeholder="availability" value="Stock" checked="" />Stock
                        <input type="radio" id="availability" name="availability" placeholder="availability" value="Fuera de stock"/>No
                    </td>
                </tr>
                
                <tr>
                    <td>Quantity: </td>
                    <td><input type="text" id= "quantity" name="quantity" placeholder="write quantity" value=""/></td>
                </tr>
                
                <tr>
                    <td>Agency: </td>
                    <td>
                        Correos<input type="checkbox" name="agency[]" class="envioCheckbox" value="Correos">
                        MRW<input type="checkbox" name="agency[]" class="envioCheckbox" value="MRW" checked="">
                        DHL<input type="checkbox" name="agency[]" class="envioCheckbox" value="DHL">
                        Seur<input type="checkbox" name="agency[]" class="envioCheckbox" value="Seur">
                    </td>
                    <td>
                        <div id="error_agency"></div>
                    </td>
                </tr>

                <tr>
                    <td>Price: </td>
                    <td><input type="text" id= "price" name="price" placeholder="write price" value=""/>€</td>
                </tr>

                <tr>
                    <td>Description: </td>
                    <td>
                        <textarea cols="30" rows="5" id="description" name="description" placeholder="write your description" value=""></textarea>
                    </td>
                </tr>

                <tr>
                    <td><input type="button" name="create" id="create" value="Registrar Producto"></input></td>
                    <td align="right"><a href="index.php?module=ComponentsInformatics&op=list">Volver</a></td>
                </tr>
            </table>
        </form>
    </section>