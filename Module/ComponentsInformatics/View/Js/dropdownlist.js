$(document).ready(function () {  

  /////// dropdownlist color ///////////    
  var data = new Array();
  var firstNames = ["Negro", "Azul", "Rojo", "Amarillo", "Verde", "Violeta", "Plata", "Oro", "Marron"];

  var k = 0;
  for (var i = 0; i < firstNames.length; i++) {
    var row = {};
    row["firstname"] = firstNames[k];
    data[i] = row;
    k++;
  }
  var source ={localdata: data,datatype: "array"};
  var dataAdapter = new $.jqx.dataAdapter(source);
  $('.color').jqxDropDownList({ selectedIndex: 0,  source: dataAdapter, displayMember: "firstname", placeHolder: "Select...",itemHeight: 70, height: 25, width: 270,
    renderer: function (index, label, value) {
      var datarecord = data[index];
      var imgurl = 'media/' + label.toLowerCase() + '.png';
      var img = '<img height="50" width="45" src="' + imgurl + '"/>';
      var table = '<table style="min-width: 150px;"><tr><td style="width: 55px;" rowspan="2">' + img + '</td><td id="'+datarecord.firstname+'">' + datarecord.firstname + '</td></tr></table>';
                    
      return table;
    },
               
    ready: function () {
      $(".color").change(function(){
        var a = $(".color").jqxDropDownList('val');
        alert(a);
                          
      });
    }

  });

  ////////// dropdownlist brand ////////
  var data1 = new Array();
  var brandNames = ["LG", "Samsung", "Sarp", "BQ", "Xiaomi", "Benq", "Philips", "MSI", "Kinstong"];
            
  var k = 0;
  for (var i = 0; i < brandNames.length; i++) {
    var row = {};
    row["brandName"] = brandNames[k];
    data1[i] = row;
    k++;
  }
  var source ={localdata: data1,datatype: "array"};
  var dataAdapter = new $.jqx.dataAdapter(source);
  $('.brand').jqxDropDownList({ selectedIndex: 0,  source: dataAdapter, displayMember: "brandName", placeHolder: "Select...",itemHeight: 70, height: 25, width: 270,
    renderer: function (index, label, value) {
      var datarecord = data1[index];
      var imgurl = 'media/' + label.toLowerCase() + '.png';
      var img = '<img height="50" width="45" src="' + imgurl + '"/>';
      var table = '<table style="min-width: 150px;"><tr><td style="width: 55px;" rowspan="2">' + img + '</td><td id="'+datarecord.brandName+'">' + datarecord.brandName + '</td></tr></table>';
                    
      return table;
    },
    
    ready: function () {
      $(".brand").change(function(){
        var a = $(".brand").jqxDropDownList('val');
        alert(a);
                          
      });
    }

  });

  ////////// dropdownlist tipo ////////
  var data2 = new Array();
  var tipos = ["Placas base", "Discos duro", "Memorias ram", "TV", "Ratones", "Monitores", "Carcasas", "Procesadores", "Tarjetas graficas", "Fuentes alimentacion", "Ventilacion", "Multilectores", "Grabadoras DVD/BluRay", "Tarjetas de sonido", "Accesorios"];
            
  var k = 0;
  for (var i = 0; i < tipos.length; i++) {
    var row = {};
    row["tipo"] = tipos[k];
    data2[i] = row;
    k++;
  }
  var source ={localdata: data2,datatype: "array"};
  var dataAdapter = new $.jqx.dataAdapter(source);
  $('.tipo').jqxDropDownList({ selectedIndex: 0,  source: dataAdapter, displayMember: "tipo", placeHolder: "Select...",itemHeight: 70, height: 25, width: 270,
    renderer: function (index, label, value) {
      var datarecord = data2[index];
      var imgurl = 'media/' + label.toLowerCase() + '.png';
      var img = '<img height="50" width="45" src="' + imgurl + '"/>';
      var table = '<table style="min-width: 150px;"><tr><td style="width: 55px;" rowspan="2">' + img + '</td><td id="'+datarecord.tipo+'">' + datarecord.tipo + '</td></tr></table>';
                    
      return table;
    },
    
    ready: function () {
      $(".tipo").change(function(){
        var a = $(".tipo").jqxDropDownList('val');
        alert(a);
                          
      });
    }

  });

});//end ready