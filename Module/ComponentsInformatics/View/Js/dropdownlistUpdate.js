$(document).ready(function () {  
            // prepare dropdownlist color
            
            var data = new Array();
            var firstNames = ["Negro", "Azul", "Rojo", "Amarillo", "Verde", "Violeta", "Plata", "Oro", "Marron"];
            
            var k = 0;
            for (var i = 0; i < firstNames.length; i++) {
              var row = {};
              row["firstname"] = firstNames[k];
              data[i] = row;
              k++;
            }
            var source =
            {
              localdata: data,
              datatype: "array"
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            var color = $("div.color").attr('id');
            //alert("color update"+color);

            if (color==="Negro") {
              var index = 0;
              DropDownListColor(index);

            }else if (color==="Azul") {
              var index = 1;
              DropDownListColor(index);

            }else if (color==="Rojo") {
              var index = 2;
              DropDownListColor(index);

            }else if (color==="Amarillo") {
              var index = 3;
              DropDownListColor(index);

            }else if (color==="Verde") {
              var index = 4;
              DropDownListColor(index);

            }else if (color==="Violeta") {
              var index = 5;
              DropDownListColor(index);

            }else if (color==="Plata") {
              var index = 6;
              DropDownListColor(index);

            }else if (color==="Oro") {
              var index = 7;
              DropDownListColor(index);

            }else if (color==="Marron") {
              var index = 8;
              DropDownListColor(index);

            }

            function DropDownListColor(index) {
              $('.colorUp').jqxDropDownList({ selectedIndex: index,  source: dataAdapter, displayMember: "firstname", placeHolder: "Select...",itemHeight: 70, height: 25, width: 270,
                renderer: function (index, label, value) {
                  var datarecord = data[index];
                  var imgurl = 'media/' + label.toLowerCase() + '.png';
                  var img = '<img height="50" width="45" src="' + imgurl + '"/>';
                  var table = '<table style="min-width: 150px;"><tr><td style="width: 55px;" rowspan="2">' + img + '</td><td id="'+datarecord.firstname+'">' + datarecord.firstname + '</td></tr></table>';
                  
                  return table;
                },
                
                ready: function () {
                  $(".colorUp").change(function(){
                    var a = $(".colorUp").jqxDropDownList('val');
                    alert(a);
                    
                  });
                }

              });
            }
            // prepare dropdownlist brand

            var data1 = new Array();
            var brandNames = ["LG", "Samsung", "Sarp", "BQ", "Xiaomi", "Benq", "Philips", "MSI", "Kinstong"];
            
            var k = 0;
            for (var i = 0; i < brandNames.length; i++) {
              var row = {};
              row["brandName"] = brandNames[k];
              data1[i] = row;
              k++;
            }
            var source =
            {
              localdata: data1,
              datatype: "array"
            };
            var dataAdapter = new $.jqx.dataAdapter(source);

            var brand = $("div.brand").attr('id');
            //alert("marca update: "+brand);
            if (brand==="LG") {
              var index = 0;
              dropDownListBrand (index);

            }else if (brand==="Samsung") {
              var index = 1;
              dropDownListBrand (index);

            }else if (brand==="Sarp") {
              var index = 2;
              dropDownListBrand (index);

            }else if (brand==="BQ") {
              var index = 3;
              dropDownListBrand (index);

            }else if (brand==="Xiaomi") {
              var index = 4;
              dropDownListBrand (index);

            }else if (brand==="Benq") {
              var index = 5;
              dropDownListBrand (index);

            }else if (brand==="Philips") {
              var index = 6;
              dropDownListBrand (index);

            }else if (brand==="MSI") {
              var index = 7;
              dropDownListBrand (index);

            }else if (brand==="Kinstong") {
              var index = 8;
              dropDownListBrand (index);

            }

            function dropDownListBrand (index){
              $('.brandUp').jqxDropDownList({ selectedIndex: index,  source: dataAdapter, displayMember: "brandName", placeHolder: "Select...",itemHeight: 70, height: 25, width: 270,
                renderer: function (index, label, value) {
                  var datarecord = data1[index];
                  var imgurl = 'media/' + label.toLowerCase() + '.png';
                  var img = '<img height="50" width="45" src="' + imgurl + '"/>';
                  var table = '<table style="min-width: 150px;"><tr><td style="width: 55px;" rowspan="2">' + img + '</td><td id="'+datarecord.brandName+'">' + datarecord.brandName + '</td></tr></table>';
                  
                  return table;
                },
                
                ready: function () {
                  $(".brandUp").change(function(){
                    var a = $(".brandUp").jqxDropDownList('val');
                    alert(a);
                    
                  });
                }

              });

            }
  ////////// dropdownlist tipo ////////
  var data2 = new Array();
  var tipos = ["Placas base", "Discos duro", "Memorias ram", "TV", "Ratones", "Monitores", "Carcasas", "Procesadores", "Tarjetas graficas", "Fuentes alimentacion", "Ventilacion", "Multilectores", "Grabadoras DVD/BluRay", "Tarjetas de sonido", "Accesorios"];
  
  var k = 0;
  for (var i = 0; i < tipos.length; i++) {
    var row = {};
    row["tipo"] = tipos[k];
    data2[i] = row;
    k++;
  }
  var source ={localdata: data2,datatype: "array"};
  var dataAdapter = new $.jqx.dataAdapter(source);
  var tipo = $("div.tipo").attr('id');

  if (tipo==="Placas base") {
    var index = 0;
    dropDownListTipo(index);
    
  }else if (tipo==="Discos duro") {
    var index = 1;
    dropDownListTipo(index);

  }else if (tipo==="Memorias ram") {
    var index = 2;
    dropDownListTipo(index);

  }else if (tipo==="TV") {
    var index = 3;
    dropDownListTipo(index);

  }else if (tipo==="Ratones") {
    var index = 4;
    dropDownListTipo(index);

  }else if (tipo==="Monitores") {
    var index = 5;
    dropDownListTipo(index);

  }else if (tipo==="Carcasas") {
    var index = 6;
    dropDownListTipo(index);

  }else if (tipo==="Procesadores") {
    var index = 7;
    dropDownListTipo(index);

  }else if (tipo==="Tarjetas graficas") {
    var index = 8;
    dropDownListTipo(index);

  }else if (tipo==="Fuentes alimentacion") {
    var index = 9;
    dropDownListTipo(index);

  }else if (tipo==="Ventilacion") {
    var index = 10;
    dropDownListTipo(index);

  }else if (tipo==="Multilectores") {
    var index = 11;
    dropDownListTipo(index);

  }else if (tipo==="Grabadoras DVD/BluRay") {
    var index = 12;
    dropDownListTipo(index);

  }else if (tipo==="Tarjetas de sonido") {
    var index = 13;
    dropDownListTipo(index);

  }else if (tipo==="Accesorios electronicos") {
    var index = 14;
    dropDownListTipo(index);

  }

  function dropDownListTipo(index){
    $('.tipoUp').jqxDropDownList({ selectedIndex: index,  source: dataAdapter, displayMember: "tipo", placeHolder: "Select...",itemHeight: 70, height: 25, width: 270,
      renderer: function (index, label, value) {
        var datarecord = data2[index];
        var imgurl = 'media/' + label.toLowerCase() + '.png';
        var img = '<img height="50" width="45" src="' + imgurl + '"/>';
        var table = '<table style="min-width: 150px;"><tr><td style="width: 55px;" rowspan="2">' + img + '</td><td id="'+datarecord.tipo+'">' + datarecord.tipo + '</td></tr></table>';
        
        return table;
      },
      
      ready: function () {
        $(".tipoUp").change(function(){
          var a = $(".tipoUp").jqxDropDownList('val');
          alert(a);
          
        });
      }

    });
  }

});//end ready