function load_prods() {
    var jqxhr = $.get("Module/ComponentsInformatics/Controller/controller_ComponentsInformatics.class.php?op=create&load=true", function (data) {
        //alert(data);
        var json = JSON.parse(data);
        //alert(json.prod.name);
        
        pintar_prods(json);
    });
}

$(document).ready(function () {
    load_prods();
});

function pintar_prods(data) {
    /*name  model    fabricationDate    discounts   color   brand   availability    quantity    agency  price   description
        
    */
    var content = document.getElementById("content");
    var div_prod = document.createElement("div");
    var parrafo = document.createElement("p");

    /*var msje = document.createElement("div");
    msje.innerHTML = "msje = ";
    msje.innerHTML += data.msje;*/
    
    var name = document.createElement("div");
    name.innerHTML = "name = ";
    name.innerHTML += data.prod.name;
    
    var model = document.createElement("div");
    model.innerHTML = "model = ";
    model.innerHTML += data.prod.model;
    
    var fabricationDate = document.createElement("div");
    fabricationDate.innerHTML = "fabricationDate = ";
    fabricationDate.innerHTML += data.prod.fabricationDate;

    var discounts = document.createElement("div");
    discounts.innerHTML = "discounts = ";
    discounts.innerHTML += data.prod.discounts;
    
    var color = document.createElement("div");
    color.innerHTML = "color = ";
    color.innerHTML += data.prod.color;
    
    var brand = document.createElement("div");
    brand.innerHTML = "brand = ";
    brand.innerHTML += data.prod.brand;

    var tipo = document.createElement("div");
    tipo.innerHTML = "tipo = ";
    tipo.innerHTML += data.prod.tipo;
    
    var availability = document.createElement("div");
    availability.innerHTML = "availability = ";
    availability.innerHTML += data.prod.availability;

    var quantity = document.createElement("div");
    quantity.innerHTML = "quantity = ";
    quantity.innerHTML += data.prod.quantity;
    
    var agency = document.createElement("div");
    agency.innerHTML = "agency = ";
    for(var i =0;i < data.prod.agency.length;i++){
        agency.innerHTML += ""+data.prod.agency[i];
    }

    var price = document.createElement("div");
    price.innerHTML = "price = ";
    price.innerHTML += data.prod.price;

    var description = document.createElement("div");
    description.innerHTML = "description = ";
    description.innerHTML += data.prod.description;

    div_prod.appendChild(parrafo);
    //parrafo.appendChild(msje);
    parrafo.appendChild(name);
    parrafo.appendChild(model);
    parrafo.appendChild(fabricationDate);
    parrafo.appendChild(discounts);
    parrafo.appendChild(color);
    parrafo.appendChild(brand);
    parrafo.appendChild(tipo);
    parrafo.appendChild(availability);
    parrafo.appendChild(quantity);
    parrafo.appendChild(agency);   
    parrafo.appendChild(price);
    parrafo.appendChild(description);
    content.appendChild(div_prod);
    
    
}
