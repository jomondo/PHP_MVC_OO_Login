    <link rel="stylesheet" href="View/lib/jqwidgets/styles/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="View/lib/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxsplitter.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxlistbox.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxdata.js"></script>
    <script type="text/javascript" src="View/lib/jqwidgets/jqxdropdownlist.js"></script>
    <script type="text/javascript" src="View/lib/scripts/demos.js"></script>
    <script type="text/javascript" src="Module/ComponentsInformatics/View/Js/dropdownlistUpdate.js"></script>
    <section>
        <div class="wrap">
            <form autocomplete="on" method="post" name="form_update" id="form_update">
                <h2>Update components</h2>
                <table>
                    <div class="id" id="<?php echo @$fila['id']; ?>"></div>
                    <div class="color" id="<?php echo @$fila['color']; ?>"></div>
                    <div class="brand" id="<?php echo @$fila['brand']; ?>"></div>
                    <div class="tipo" id="<?php echo @$fila['tipo']; ?>"></div>

                    <tr>
                        <td>Name: </td>
                        <td><input type="text" id="name" name="name" placeholder="write name" value="<?php echo @$fila['name']; ?>"/></td>
                    </tr>
                
                    <tr>
                        <td>Model: </td>
                        <td><input type="texto" id="model" name="model" placeholder="write model" value="<?php echo @$fila['model']; ?>"/></td>
                    </tr>
                    
                    <tr>
                        <td>Fabrication Date: </td>
                        <td><input id="fabricationDate" type="text" name="fabricationDate" placeholder="dd/mm/yyyy" value="<?php echo @$fila['fabricationDate']; ?>"/></td>
                    </tr>

                    <tr>
                        <td>Discounts: </td>
                        <td><input type="text" id="discounts" name="discounts" placeholder="write discounts" value="<?php echo @$fila['discounts']; ?>"/></td>
                    </tr>

                    <tr>
                        <td>Color:</td>
                        <td>
                            <div name="list" id="dropdownlist" class="colorUp"></div>
                        </td>
                    </tr>
                    <tr>
                        <td>Brand:</td>
                        <td>
                            <div name="list" id="dropdownlist1" class="brandUp"></div>
                        </td>   
                    </tr>
                    <tr>
                        <td>Tipo:</td>
                        <td><div name="list" id="dropdownlist2" class="tipoUp"></div></td>   
                    </tr>
                    <tr>
                        <td>Availability: </td>
                        <td>
                            <?php
                                if(@$fila['availability']==="Stock"){
                            ?>
                            <input type="radio" id="availability" name="availability" placeholder="availability" value="Stock" checked="" />Stock
                            <input type="radio" id="availability" name="availability" placeholder="availability" value="Fuera de stock"/>No
                            <?php
                                }else if(@$fila['availability']==="Fuera de stock"){
                            ?>
                            <input type="radio" id="availability" name="availability" placeholder="availability" value="Stock" />Stock
                            <input type="radio" id="availability" name="availability" placeholder="availability" value="Fuera de stock" checked=""/>No
                            <?php
                                }
                            ?>
                        </td>
                        
                    </tr>
                    
                    <tr>
                        <td>Quantity: </td>
                        <td><input type="text" id= "quantity" name="quantity" placeholder="write quantity" value="<?php echo @$fila['quantity']; ?>"/></td>
                    </tr>
                    
                    <tr>
                        <td>Agency: </td>
                        <td>
                            <?php
                                $agen=explode(":", @$fila['agency']);
                                $busca_array=in_array("Correos", $agen);
                                if($busca_array){
                            ?>
                            Correos<input type="checkbox" name="agency[]" class="envioCheckbox" value="Correos" checked="">
                            <?php
                                }else{
                            ?>
                            Correos<input type="checkbox" name="agency[]" class="envioCheckbox" value="Correos">
                            <?php
                                }
                                $busca_array=in_array("MRW", $agen);
                                if($busca_array){
                            ?>
                            MRW<input type="checkbox" name="agency[]" class="envioCheckbox" value="MRW" checked="">
                            <?php
                                }else{
                            ?>
                            MRW<input type="checkbox" name="agency[]" class="envioCheckbox" value="MRW">
                            <?php
                                }
                                $busca_array=in_array("DHL", $agen);
                                if($busca_array){
                            ?>
                            DHL<input type="checkbox" name="agency[]" class="envioCheckbox" value="DHL" checked="">
                            <?php
                                }else{
                            ?>
                            DHL<input type="checkbox" name="agency[]" class="envioCheckbox" value="DHL">
                            <?php
                                }
                                $busca_array=in_array("Seur", $agen);
                                if($busca_array){
                            ?>
                            Seur<input type="checkbox" name="agency[]" class="envioCheckbox" value="Seur" checked="">
                            <?php
                                }else{
                            ?>
                            Seur<input type="checkbox" name="agency[]" class="envioCheckbox" value="Seur">
                            <?php
                                }
                            ?>
                        </td>
                        <td>
                            <div id="error_agency"></div>
                        </td>
                    </tr>

                    <tr>
                        <td>Price: </td>
                        <td><input type="text" id= "price" name="price" placeholder="write price" value="<?php echo @$fila['price']; ?>"/>€</td>
                    </tr>

                    <tr>
                        <td>Description: </td>
                        <td><textarea cols="30" rows="5" id="description" name="description" placeholder="write your description" value="<?php echo @$fila['description']; ?>"><?php echo @$fila['description']; ?></textarea></td>
                    </tr>

                    <tr>
                        <td><input type="button" name="update" id="update" value="Actualizar Producto"></input></td>
                        <td align="right"><a href="index.php?module=ComponentsInformatics&op=list">Volver</a></td>
                    </tr>
                </table>
            </form>
        </div>
    </section>