<section class="wrap">
    <h3 data-tr="LISTA COMPONENTES" class="text-center">LISTA COMPONENTES</h3>
    <p><a href="index.php?module=ComponentsInformatics&op=create"><img src="media/añadir.png"></a></p> 

    <div class="row" > 
            <?php
                if ($rdo->num_rows === 0){
                    echo 'NO HAY NINGUN COMPONENTE EN NUESTRA BASE DE DATOS';
                        
                }else{
            ?>
                    <table class="table-striped" style="width: 50%;text-align:center;">
                        <tr>
                            <th><b data-tr="Nombre">Nombre</b></th>
                            <td><b data-tr="Marca">Marca</b></td>
                            <td><b data-tr="Precio por unidad">Precio por unidad</b></td>
                            <td align="center"><b data-tr="Opciones">Opciones</b></td>
                        </tr>
                    <?php
                        while ($fila=$rdo->fetch_assoc()){
                            echo '<tr>';
                            echo '<td width=125>'. $fila['name'] . '</td>';
                            echo '<td width=125>'. $fila['brand'] . '</td>';
                            echo '<td width=125>'. $fila['price'] . '€</td>';
                            echo '<td width=350>';
                            echo '<button type="submit" class="Button_blue" id="'.$fila['id'].'">Details</button>';
                            echo '&nbsp;';
                            echo '<button type="submit" class="Button_red" id="'.$fila['id'].'">Delete</button>';
                            echo '</td>';
                            echo '</tr>';
                        }
                }
                    ?>
            </table>
    </div>

<!-- details -->
<div id="feedback-modal" class="modal fade" role="dialog">
    <section id="modal-dialog">
        <div class="modal-body">
            Name: <div id="name"></div></br>
            Model: <div id="model"></div></br>
            Fabrication Date: <div id="fabricationDate"></div></br>
            Discounts: <div id="discounts"></div></br>
            Color: <div id="color"></div></br>
            Brand: <div id="brand"></div></br>
            Tipo: <div id="tipo"></div></br>
            Availability: <div id="availability"></div></br>
            Quantity: <div id="quantity"></div></br>
            Agency: <div id="agency"></div></br>
            Price: <div id="price"></div></br>
            Description: <div id="description"></div></br>
        </div>
    </section>
</div> 
<!-- delete -->
<div class="modal fade" role="dialog">
    <div id="dialog-confirm" title="Eliminar componente">
      <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Quieres eliminar el componente <div id="name1"></div>?</p>
    </div>
    <div id="dialog-message" title="Delete Complete">
      <p>
        <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
        Su componente ha sido eliminado satisfactoriamente.
      </p>

    </div>
</div>
</section>