<script type="text/javascript" src="Module/Perfil/View/Js/perfilUsu.js" ></script>
<link href="View/css/dropzone.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="View/js/dropzone.js" ></script>
<section id="">

<div class="wrap">
        <nav class="navbar-default navbar-static-top" role="navigation">
             
                
            <h1> <a class="navbar-brand" href="">PROFILE</a></h1>         
               
            <div class=" border-bottom">
        
            <div class="clearfix"></div>
      
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Mi cuenta</span> </a>
                        </li>
                        <li>
                            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i> <span class="nav-label">Mis Pedidos</span><span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li><a href="#" class=" hvr-bounce-to-right" id="pedidos"><i class="fa fa-align-left nav_icon"></i>Pedidos</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="" class=" hvr-bounce-to-right" id="likes"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Productos que me gustan</span> </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="page-wrapper" class="">
       
    <!--grid-->
    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Datos Usuario</h3>
        
            <div class="form-group">
                <div id="datos">
                    <table>
                        <tr>
                            <td><div id="avatar"></div></td>
                        </tr>
                        <tr>
                            <td>Nombre:</td>
                            <td><div id="nomUsu"></div></td>
                        </tr>
                        <tr>
                            <td>Nº Pedidos:</td>
                            <td><div id="nPedidos"></div></td>
                        </tr>
                        <tr>
                            <td>Total Gastado:</td>
                            <td><div id="money"></div></td>
                        </tr>
                    </table>
                </div>
            </div>
            <label>¿Quiere modificar su nombre o password?</label>
            <input type="button" class="btn btn-default" id="updateProfile" value="Update Password"></input>
            <input type="button" class="btn btn-default" id="updateProfileName" value="Update Name"></input>

        </div>
    </div>
    
    <div id="tablaUpdate"></div>
    <div id="tablaPedidos" class="grid-form">
        
    </div>
    <!-- details modal-->
    <div id="feedback-modal" class="modal fade" role="dialog">
        <section id="modal-dialog">
            <div class="modal-body"></div>
        </section>
    </div>
    
    <!-- dropzone-->
    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Subir Avatar</h3>
            <div class="msg"></div>
            <div id="dropzone"></div>
            <!--<input type="button" class="btn btn-default" id="buttonSubirImagen" value="Subir"></input>-->
        </div>
    </div>

    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Productos que me gustan</h3>
            <div id="tablaLikes"></div>
        </div>
    </div>

    <div class="grid-form">
        <div class="grid-form1">
            <h3 id="forms-example" class="">Cancelar cuenta</h3>
        
            <div class="form-group">
                <div id="datos">
                    <table>
                        <tr>
                            <td>¿Quiere suprimir su cuenta de usuario?</td>
                            <td><input type="button" class="btn btn-default" id="deleteUsuario" value="Delete"></input></td>
                        </tr>
                    </table>
                </div>

            </div>
            <!-- delete -->
            <div class="modal fade" role="dialog">
                <div id="dialog-confirm" title="Eliminar usuario">
                  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Quiere eliminar su cuenta <div id="name1"></div>?</p>
                </div>
                <div id="dialog-message" title="Delete Complete">
                  <p>
                    <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
                    La cuenta ha sido eliminada satisfactoriamente. Sentimos que se haya ido.
                  </p>

                </div>
            </div>
        </div>
    </div>

</div>
</div>
      
</section>