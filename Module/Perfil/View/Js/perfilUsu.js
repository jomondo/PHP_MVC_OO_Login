$(document).ready(function () {
	$.ajax({
        type: "GET",
        url: "Module/Perfil/Controller/controller_Perfil.class.php?op=getDatosPerfil&datos=true",
        success: function(message){
            //alert(message);
            var json = JSON.parse(message);
            $("#nomUsu").html(json.name);
            $("#nPedidos").html(json.nPedidos);
            $("#money").html(json.money+"€");
            $("#avatar").append('<img src="'+json.media+'" class="imgRedonda" width="100px" height="100px">');

            
            $(document).on('click', '#updateProfile', function(event) {
                var form = $("div.confirmaPasswd").attr('id');
                if (form==undefined) {
                    $("#tablaUpdate").append('<div class="confirmaPasswd" id="confirmaPasswd"><div class="grid-form"><div class="grid-form1"><h3 id="forms-example" class="">Confirma Usuario</h3><form><div class="form-group"><label>Name:</label><input type="text" class="form-control" id="namePerfil" placeholder="Name" value="'+json.name+'"></div>'
                    +'<div class="form-group"><label>Password</label><input type="password" class="form-control" id="passwdPerfil" placeholder="Password"></div><input type="button" id="confirmPasswd" class="btn btn-default" value="Enviar"></input></form></div></div></div>');
                }

            });

           $(document).on('click', '#updateProfileName', function(event) {
                var form = $("div.confirmaUsu").attr('id');
                if (form==undefined) {
                    $("#tablaUpdate").append('<div class="confirmaUsu" id="confirmaUsu"><div class="grid-form"><div class="grid-form1"><h3 id="forms-example" class="">Confirma Usuario</h3><form><div class="form-group"><label>Name:</label><input type="text" class="form-control" id="namePerfil" placeholder="Name" value="'+json.name+'"></div>'
                    +'<div class="form-group"><label>Password</label><input type="password" class="form-control" id="passwdPerfil" placeholder="Password"></div><input type="button" id="confirmaUsu1" class="btn btn-default" value="Enviar"></input></form></div></div></div>');
                }
            });

           $(document).on('click', '#deleteUsuario', function(event) {
                var form = $("div.tablaDelete").attr('id');
                if (form==undefined) {
                    $("#tablaUpdate").append('<div class="tablaDelete" id="confirmaDelete"><div class="grid-form"><div class="grid-form1"><h3 id="forms-example" class="">Confirma Usuario</h3><form><div class="form-group"><label>Name:</label><input type="text" class="form-control" id="namePerfil" placeholder="Name" value="'+json.name+'"></div>'
                    +'<div class="form-group"><label>Password</label><input type="password" class="form-control" id="passwdPerfil" placeholder="Password"></div><input type="button" id="confirmaUsu2" class="btn btn-default" value="Enviar"></input></form></div></div></div>');
                }
            });

        },

        error: function(xhr){
            xhr.responseJSON = JSON.parse(xhr.responseText);
            if (xhr.responseJSON.redirect) {
                window.location.href = xhr.responseJSON.redirect;
            }
        }

    });
    

    //obtener productos que me gustan
    $.ajax({
        type: "GET",
        url: "Module/Perfil/Controller/controller_Perfil.class.php?op=getLikesProd",
            success: function(message){
                //alert(message);
                var json = JSON.parse(message);
                //alert(json.prod)
                
                json.forEach(function(elemento){
                    alert(elemento.prod);
                    var data={"prod":elemento.prod};
                    var data_JSON = JSON.stringify(data);

                    $.ajax({
                        type: "POST",
                        url: "Module/Perfil/Controller/controller_Perfil.class.php?op=getdatosProd",
                        data:{prod:data_JSON},
                            success: function(message){
                                alert(message);
                                var json = JSON.parse(message);
                                $("#tablaLikes").append("<p>"+json[0].prod+"</p>");
                                
                            },
                            error: function(xhr){
                                alert(xhr.responseText);
                                
                            }

                        });
                });

                
                
            },
            error: function(xhr){
                alert(xhr.responseText);
                
            }

        });

    $(document).on('click', '#confirmPasswd', function(event){
        var name = $("#namePerfil").val();
        var passwd = $("#passwdPerfil").val();
        var data={"name":name,"passwd":passwd};
        var data_prods_JSON = JSON.stringify(data);
        
        $.post('Module/Perfil/Controller/controller_Perfil.class.php?op=confirmaPasswd',{datosUsu:data_prods_JSON},function (response){
            //alert(response);
            var json = JSON.parse(response);
            console.log("success: "+json.success);
            if (json.success) {
                //alert("OK");
                $("#confirmaPasswd").remove();
                $("#tablaUpdate").append('<div id="updateUsu"><div class="grid-form"><div class="grid-form1"><h3 id="forms-example" class="">Update Password</h3><form><div class="form-group"><label>Password antiguo:</label><input type="password" class="form-control" id="passwdPerfilUp1" placeholder="Password"></div>'
                +'<div class="form-group"><label>Password nuevo:</label><input type="password" class="form-control" id="passwdPerfilUp2" placeholder="Password"></div><input type="button" id="confirmUpdate" class="btn btn-default" value="Enviar"></input></form></div></div></div>');

            }
                
        }).fail(function(xhr){
            xhr.responseJSON = JSON.parse(xhr.responseText);
            $.amaran({
                'message'           :'Su password o usuario no es correcto',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            if (xhr.responseJSON.redirect) {
                window.location.href = xhr.responseJSON.redirect;
            }

        });//end fail
                
    });

    //button confirmar usu para el nombre
    $(document).on('click', '#confirmaUsu1', function(event){
        var name = $("#namePerfil").val();
        var passwd = $("#passwdPerfil").val();
        var data={"name":name,"passwd":passwd};
        var data_prods_JSON = JSON.stringify(data);
        
        $.post('Module/Perfil/Controller/controller_Perfil.class.php?op=confirmaPasswd',{datosUsu:data_prods_JSON},function (response){
            //alert(response);
            var json = JSON.parse(response);
            console.log("success: "+json.success);
            if (json.success) {
                //alert("OK name");
                $("#confirmaUsu").remove();
                $("#tablaUpdate").append('<div id="updateName"><div class="grid-form"><div class="grid-form1"><h3 id="forms-example" class="">Update Name</h3><form><div class="form-group"><label>Name antiguo:</label><input type="text" class="form-control" id="name1" placeholder="Name"></div>'
                +'<div class="form-group"><label>Name nuevo:</label><input type="text" class="form-control" id="name2" placeholder="Name"></div><input type="button" id="confirmUpdateName" class="btn btn-default" value="Enviar"></input></form></div></div></div>');

               
            }
                
        }).fail(function(xhr){
            xhr.responseJSON = JSON.parse(xhr.responseText);
            $.amaran({
                'message'           :'Su password o usuario no es correcto',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            if (xhr.responseJSON.redirect) {
                window.location.href = xhr.responseJSON.redirect;
            }

        });//end fail
                
    });

    //delete usuario
    $(document).on('click', '#confirmaUsu2', function(event){
        var name = $("#namePerfil").val();
        var passwd = $("#passwdPerfil").val();
        var data={"name":name,"passwd":passwd};
        var data_prods_JSON = JSON.stringify(data);
        
        $.post('Module/Perfil/Controller/controller_Perfil.class.php?op=confirmaPasswd',{datosUsu:data_prods_JSON},function (response){
            //alert(response);
            var json = JSON.parse(response);
            console.log("success: "+json.success);
            if (json.success) {
                $(".tablaDelete").remove();
                $("#name1").html(name);
                $("#dialog-confirm").dialog({
                    height: "auto",
                    width: "auto",
                    modal: true,
                    buttons: {
                        "Aceptar": function() {
                            $(this).dialog("close");
                            $.ajax({
                                type: "GET",
                                url: "Module/Perfil/Controller/controller_Perfil.class.php?op=deleteUsuario&name="+name,
                                success: function(message){
                                    //alert(message);
                                    $("#dialog-message").dialog({
                                        modal: true,
                                        buttons:{
                                            Ok: function(){
                                                $(this).dialog("close");
                                                var json = JSON.parse(message);
                                                window.location.href=json.redirect;
                                                        
                                            }
                                        },
                                                
                                    });
                                             
                                },
                                error: function(){
                                    //alert("Error");
                                    window.location.href='index.php?module=503';
                                }
                            });

                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                            
                    },
                          
                });
               
            }
                
        }).fail(function(xhr){
            xhr.responseJSON = JSON.parse(xhr.responseText);
            $.amaran({
                'message'           :'Su password o usuario no es correcto',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            if (xhr.responseJSON.redirect) {
                window.location.href = xhr.responseJSON.redirect;
            }

        });//end fail
                
    });

    // Update passwd
    $(document).on('click', '#confirmUpdate', function(event){
        var rdo = true;
        var passreg = /^[a-z0-9]{6,20}$/;
            
        var passwd1 = $("#passwdPerfilUp1").val();
        var passwd2 = $("#passwdPerfilUp2").val();

        $(".error").remove();
        //comprovem camps buits i patterns

        if( $("#passwdPerfilUp1").val() == ""){
            $.amaran({
                'message'           :'Ingrese su password antiguo',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            rdo=false;
            return false;
        }else if(!passreg.test($("#passwdPerfilUp1").val())){
            $.amaran({
                'message'           :'6 a 20 caracteres para el password',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            rdo=false;
            return false;
        }if( $("#passwdPerfilUp2").val() == "" ){
            $.amaran({
                'message'           :'Ingrese su password nuevo',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            rdo=false;
            return false;
        }else if(!passreg.test($("#passwdPerfilUp2").val())){
            $.amaran({
                'message'           :'6 a 20 caracteres para el password',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            rdo=false;
            return false;
        }

        if (rdo) {
            var data={"passwd1":passwd1,"passwd2":passwd2};
            var data_prods_JSON = JSON.stringify(data);
            
            $.post('Module/Perfil/Controller/controller_Perfil.class.php?op=updatePasswd',{datosUsu:data_prods_JSON},function (response){
                //alert(response);
                var json = JSON.parse(response);
                console.log("success: "+json.success);
                if (json.success) {
                    $("#updateUsu").remove();
                    $("#tablaUpdate").append('<div class="grid-form1"><p>Sus datos se han actualizado de manera correcta. Ya puede utilizar su nueva contraseña</p></div>');

                }
                    
            }).fail(function(xhr){
                xhr.responseJSON = JSON.parse(xhr.responseText);
                if (xhr.responseJSON.error.password){
                    $.amaran({
                        'message'           :'El password antiguo es incorrecto',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                        
                }
                if (xhr.responseJSON.redirect) {
                    window.location.href = xhr.responseJSON.redirect;
                }

            });//end fail
        }
                       
    });

    // Update name
    $(document).on('click', '#confirmUpdateName', function(event){
        var rdo = true;
        var namereg = /^[A-Za-z\s]{4,15}$/;
            
        var name1 = $("#name1").val();
        var name2 = $("#name2").val();

        $(".error").remove();
        //comprovem camps buits i patterns

        if( $("#name1").val() == ""){
            $.amaran({
                'message'           :'Ingrese su nombre antiguo',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            rdo=false;
            return false;
        }else if(!namereg.test($("#name1").val())){
            $.amaran({
                'message'           :'4 a 15 caracteres para el nombre antiguo',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            rdo=false;
            return false;
        }if( $("#name2").val() == "" ){
            $.amaran({
                'message'           :'Ingrese su nombre nuevo',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            rdo=false;
            return false;
        }else if(!namereg.test($("#name2").val())){
            $.amaran({
                'message'           :'4 a 15 caracteres para el nombre nuevo',
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            rdo=false;
            return false;
        }

        if (rdo) {
            var data={"name1":name1,"name2":name2};
            var data_names_JSON = JSON.stringify(data);
            
            $.post('Module/Perfil/Controller/controller_Perfil.class.php?op=updateName',{datosUsu:data_names_JSON},function (response){
                //alert(response);
                var json = JSON.parse(response);
                //console.log("success: "+json.success);
                if (json.success) {
                    $("#updateName").remove();
                    $("#tablaUpdate").append('<div class="grid-form1"><p>Sus datos se han actualizado de manera correcta. Ya puede utilizar su nuevo nombre. Actualice la pagina para ver los cambios.</p></div>');

                }
                    
            }).fail(function(xhr){
                xhr.responseJSON = JSON.parse(xhr.responseText);
                if (xhr.responseJSON.error.name){
                    $.amaran({
                        'message'           :'El nombre antiguo es incorrecto',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });
                        
                }
                if (xhr.responseJSON.redirect) {
                    window.location.href = xhr.responseJSON.redirect;
                }

            });//end fail
        }

    });

    //pintar pedidos
    $(document).on('click', '#pedidos', function(event){
        $(".grid-form1").remove();
        $.ajax({
            type: "GET",
            url: "Module/Perfil/Controller/controller_Perfil.class.php?op=getPedidos",
            success: function(message){
                var json = JSON.parse(message);
                var pedidos = $("tr#numPedidos").attr('id');
                
                if (pedidos==undefined) {
                    $("#tablaPedidos").append('<table class="table-striped" id="idPedidos" style="width: 50%;text-align:center;"><tr><td>Nº Pedido</td><td>Fecha</td><td>Opcion</td></tr></table>');
                    json.forEach(function(elemento){
                        $("#idPedidos").append('<tr id="numPedidos"><td>'+elemento.id_pedido+'</td><td>'+elemento.fecha+'</td><td><input type="button" class="btn btn-default detailsPedido" id="'+elemento.id_pedido+'" value="Details"></input></td></tr>');
                                
                    });
                } 

            },
            error: function(xhr){
                xhr.responseJSON = JSON.parse(xhr.responseText);
                if (xhr.responseJSON.redirect) {
                    window.location.href = xhr.responseJSON.redirect;
                }
            }

        });

    });

    //get productos de cada pedido
    $(document).on('click', '.detailsPedido', function(event){
        var idPedido = this.getAttribute('id');
        var data = {"idPedido":idPedido};
        var dataIdPedido = JSON.stringify(data);

        $.ajax({
            type: "POST",
            url: "Module/Perfil/Controller/controller_Perfil.class.php?op=getProductosPedido",
            data:{pedidoId:dataIdPedido},
            success: function(message){
                $("div#prod").remove();
                var json = JSON.parse(message);
                //alert(message);
                json.forEach(function(elemento){
                    $(".modal-body").append('<div id="prod"><img src="'+elemento.media+'" width="25px" height="25px"></img></br><div id="name">Name: '+elemento.name+'</div></br><div id="brand">Brand: '+elemento.brand+'</div></br><div id="price">Price: '+elemento.price+'€</div></br><hr/></div>');

                });

                $(".modal-body").append('<div id="prod">Precio Total: '+json[0].precio_total+'€</div><div id="prod">Cantidad Total: '+json[0].cantidad+'</div>');

                

                $("#modal-dialog").dialog({
                    title: "Informacion del pedido",
                    width: 850,
                    height: 450,
                    modal: "true",
                    buttons: { 
                        Close: function () { 
                            $(this).dialog("close");
                            $("div#prod").remove(); 
                        }
                    },
                    show: "fold",
                    hide: "scale"                    
                });
                
            },
            error: function(xhr){
                xhr.responseJSON = JSON.parse(xhr.responseText);
                if (xhr.responseJSON.redirect) {
                    window.location.href = xhr.responseJSON.redirect;
                }
            }

        });


    });




    //dropzone
    Dropzone.autoDiscover = false;
    $("#dropzone").dropzone({
        url: "Module/Perfil/Controller/controller_Perfil.class.php?op=upload",
        method: "post",
        withCredentials: false,
        parallelUploads: 1, //Cuanto archivos subir al mismo tiempo
        uploadMultiple: false,
        maxFilesize: 5, //Maximo Tamaño del archivo expresado en mbyts
        paramName: "file",//Nombre con el que se envia el archivo
        createImageThumbnails: true,
        maxThumbnailFilesize: 1, //Limite para generar imagenes (Previsualizacion)
        thumbnailWidth: 154, //Medida de largo de la Previsualizacion
        thumbnailHeight: 154,//Medida alto Previsualizacion
        filesizeBase: 1000,
        maxFiles: 1,//define cuantos archivos se cargan al dropzone. Si se excede, se llamara al evento maxfilesexceeded.
        params: {}, //Parametros adicionales al formulario de envio ejemplo {tipo:"imagen"}
        clickable: true,
        ignoreHiddenFiles: true,
        acceptedFiles: "image/*", //extensiones
        acceptedMimeTypes: null,//Ya no se utiliza, paso a ser AceptedFiles
        autoProcessQueue: true,//True sube las imagenes automaticamente, si es false se tiene que llamar a myDropzone.processQueue(); para subirlas
        autoQueue: true,
        addRemoveLinks: true,//Habilita la posibilidad de eliminar/cancelar un archivo. Las opciones dictCancelUpload, dictCancelUploadConfirmation y dictRemoveFile se utilizan para la redacción.
        previewsContainer: null,//define dónde mostrar las previsualizaciones de archivos. Puede ser un HTMLElement liso o un selector de CSS. El elemento debe tener la estructura correcta para que las vistas previas se muestran correctamente.
        capture: null,
        dictDefaultMessage: "Arrastra los archivos aqui para subirlos",//missatges errors
        dictFallbackMessage: "Su navegador no soporta arrastrar y soltar para subir archivos.",
        dictFallbackText: "Por favor utilize el formuario de reserva de abajo como en los viejos tiempos.",
        dictFileTooBig: "La imagen supera el tamaño permitido ({{filesize}}MiB). Tam. Max : {{maxFilesize}}MiB. No se subira a nuestro servidor",
        dictInvalidFileType: "No se puede subir este tipo de archivos.",
        dictResponseError: "Server responded with {{statusCode}} code.",
        dictCancelUpload: "Cancel subida",
        dictCancelUploadConfirmation: "¿Seguro que desea cancelar esta subida?",
        dictRemoveFile: "Eliminar archivo",
        dictRemoveFileConfirmation: null,
        dictMaxFilesExceeded: "Se ha excedido el numero de archivos permitidos. No se subira a nuestro servidor",
        success: function(file, message){
            //alert(file.status);
            /*if (file.status=="success") {
                $('.msg').text('El archivo '+file.name+' se ha subido satisfactoriamente a nuestra base de datos');
            }*/
            //alert(message);
            var json = JSON.parse(message);
            if (json.success) {
                $('.msg').text('El archivo '+file.name+' se ha subido satisfactoriamente a nuestra base de datos. Refresque la pagina para ver los cambios');
                $('.dz-remove').remove();

            }
            
        },
        error: function(file,message) {
            //alert(file.status);
            //alert(message);
            $.amaran({
                'message'           :message,
                'cssanimationIn'    :'tada',
                'cssanimationOut'   :'rollOut',
                'closeButton'       :true,
                'closeOnClick'      :true
            });
            if (message) {
                var node, _i, _len, _ref, _results;
                if (file.previewElement) {
                  file.previewElement.classList.add("dz-error");
                  if (typeof message !== "String" && message.error) {
                    message = message.error;
                  }
                  _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                  _results = [];
                  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i];
                    _results.push(node.textContent = message);
                  }
                  return _results;
                }
                
            }

        }
    });

});