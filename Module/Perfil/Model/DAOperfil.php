<?php
include_once($_SERVER['DOCUMENT_ROOT']."/productos/Model/connect.php");

class DAOperfil{
	function selectDatosUser($name){
		$sql = "SELECT name,media FROM user WHERE name = '$name'";
			
		$conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);
            
        return $res;
		
	}
	function selectPedidos($name){
		$sql = "SELECT COUNT(id_pedido) FROM pedidos WHERE id_usuario = '$name'";
			
		$conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);
            
        return $res;
		
	}

	function selectMoneyTotal($name){
		$sql = "SELECT SUM(precio*cantidad) FROM detalle_pedido INNER JOIN pedidos ON detalle_pedido.id_pedido=pedidos.id_pedido WHERE pedidos.id_usuario='$name'";
			
		$conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);
            
        return $res;
		
	}

	function selectPasswd($name){
		$sql = "SELECT name,password FROM user WHERE name = '$name'";
		$conexion = conexion::open();
	    $res = mysqli_query($conexion, $sql);
	    conexion::close($conexion);

		return $res;

	}

	function updateUsuario($name,$passwd){
		//print_r($passwd);
		//exit;

        $sql = " UPDATE user SET password='$passwd' WHERE name='$name'";
            
        $conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);

		return $res;
	}

	function updateName($name,$nameNuevo){
        $sql = " UPDATE user SET name='$nameNuevo' WHERE name='$name'";
            
        $conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);

		return $res;
	}

	function updateAvatar($name,$avatar){
        $sql = " UPDATE user SET media='$avatar' WHERE name='$name'";
            
        $conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);

		return $res;
	}

	function deleteUsuario($name){
        $sql = "DELETE FROM user WHERE name='$name'";
            
        $conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);

		return $res;
	}

	function selectIdpedidos($name){
        $sql = "SELECT id_pedido,fecha FROM pedidos WHERE id_usuario='$name'";
            
        $conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);

		return $res;
	}

	function selectProdPedido($idPedido){
        $sql = "SELECT * FROM cmpoinfrmatics WHERE cmpoinfrmatics.id IN (SELECT id_prod FROM detalle_pedido WHERE id_pedido='$idPedido')";
            
        $conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);

		return $res;
	}

	function selectPriceTotal($idPedido){
        $sql = "SELECT precio_total FROM pedidos WHERE id_pedido='$idPedido' ";
            
        $conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);

		return $res;
	}

	function selectCantidad($idPedido){
        $sql = "SELECT sum(cantidad) FROM detalle_pedido WHERE id_pedido='$idPedido' ";
            
        $conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);

		return $res;
	}

	function selectLikes($name){
		//print_r($name);
		//exit;
        $sql = "SELECT prod FROM likes WHERE id_usuario='$name' ";
            
        $conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);

		return $res;
	}

	function selectDatosProd($id_prod){
		//print_r($name);
		//exit;
        $sql = "SELECT * FROM cmpoinfrmatics WHERE id='$id_prod' ";
            
        $conexion = conexion::open();
        $res = mysqli_query($conexion, $sql);
        conexion::close($conexion);

		return $res;
	}

}