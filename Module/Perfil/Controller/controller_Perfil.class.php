<?php
	include_once($_SERVER['DOCUMENT_ROOT']."/productos/Module/Perfil/Model/DAOperfil.php");
	include_once($_SERVER['DOCUMENT_ROOT']."/productos/Utils/upload.php");
	session_start();
	switch ($_GET['op']) {
	case 'getDatosPerfil':
	//echo json_encode($_SESSION['name']);
	//exit;
		if (isset($_GET['datos']) && $_GET['datos']==true) {
			
			try{
				$rdo = DAOperfil::selectDatosUser($_SESSION['name']);
				$rdoPedidos = DAOperfil::selectPedidos($_SESSION['name']);
				$rdoMoney = DAOperfil::selectMoneyTotal($_SESSION['name']);

			}catch(Exception $e) {
				$jsondata['redirect']="index.php?module=503";//canviar a module
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;
			}

			if (!$rdo || !$rdoPedidos || !$rdoMoney) {
				$jsondata['redirect']="index.php?module=503";
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;

			}else{
				foreach ($rdo as $row) {
					@$n=$row['name'];
					@$img=$row['media'];

				}
				foreach ($rdoPedidos as $row) {
					@$p=$row['COUNT(id_pedido)'];
				}
				foreach ($rdoMoney as $row) {
					@$m=$row['SUM(precio*cantidad)'];
				}

				$jsondata['name']=$n;
				$jsondata['nPedidos']=$p;
				$jsondata['money']=$m;
				$jsondata['media']=$img;
				echo json_encode($jsondata);
                exit;
			}
		}
	break;
	case 'confirmaPasswd':
		include($_SERVER['DOCUMENT_ROOT']."/productos/Utils/validatePasswd.php");
		//echo json_encode($_POST);
		//exit;
		if ((isset($_POST['datosUsu']))){
			$jsondata = array();
            $usuJson = json_decode($_POST["datosUsu"], true);

			try{
				$rdo = DAOperfil::selectPasswd($usuJson['name']);
				//echo json_encode($rdo);
				//exit;
			}catch(Exception $e) {
				$jsondata['redirect']="index.php?module=503";//canviar a module
	            header('HTTP/1.0 400 Bad error');
	            echo json_encode($jsondata);
	            exit;
			}

			if (!$rdo) {
				$jsondata['redirect']="index.php?module=503";
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;

			}else{
				$countP = mysqli_num_rows($rdo);
				foreach ($rdo as $row) {
					@$p=$row['password'];//encripta
					@$n=$row['name'];;
				}
				$rdopsswd=validatePasswd($countP,$usuJson['passwd'], @$p);
				if ($rdopsswd['resultPsswd']){
					$jsondata['success']=true;
					$jsondata['name']=$n;
	                echo json_encode($jsondata);
	                exit;
				}else{
					$jsondata['error']=$rdopsswd['error'];
					header('HTTP/1.0 400 Bad error');
	                echo json_encode($jsondata);
	                exit;
				}
			}
		}

	break;
	case 'updatePasswd':
		//echo json_encode($_POST['datosUsu']);
		//exit;
		include($_SERVER['DOCUMENT_ROOT']."/productos/Utils/validatePasswd.php");
		if ((isset($_POST['datosUsu']))){
			$jsondata = array();
            $usuJson = json_decode($_POST["datosUsu"], true);

            try{
				$rdo = DAOperfil::selectPasswd($_SESSION['name']);
				//echo json_encode($rdo);
				//exit;
			}catch(Exception $e) {
				$jsondata['redirect']="index.php?module=503";//canviar a module
	            header('HTTP/1.0 400 Bad error');
	            echo json_encode($jsondata);
	            exit;
			}
			if (!$rdo) {
				$jsondata['redirect']="index.php?module=503";
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;

			}else{
				$countP = mysqli_num_rows($rdo);
				foreach ($rdo as $row) {
					@$p=$row['password'];//encripta
					@$n=$row['name'];
				}
				$rdopsswd=validatePasswd($countP,$usuJson['passwd1'], @$p);
				if ($rdopsswd['resultPsswd']){
	                try{
	                	$passEncri = password_hash($usuJson['passwd2'], PASSWORD_BCRYPT);
						$rdo = DAOperfil::updateUsuario($n,$passEncri);
						//echo json_encode($rdo);
						//exit;
					}catch(Exception $e) {
						$jsondata['redirect']="index.php?module=503";//canviar a module
			            header('HTTP/1.0 400 Bad error');
			            echo json_encode($jsondata);
			            exit;
					}

					if (!$rdo){
						$jsondata['redirect']="index.php?module=503";
		                header('HTTP/1.0 400 Bad error');
		                echo json_encode($jsondata);
		                exit;

					}else{
						$jsondata['success']=true;
		                echo json_encode($jsondata);
		                exit;
					}

				}else{
					$jsondata['error']=$rdopsswd['error'];
					header('HTTP/1.0 400 Bad error');
	                echo json_encode($jsondata);
	                exit;
				}
			}
            
		}
	break;
	case 'updateName':
		//echo json_encode($_POST['datosUsu']);
		//exit;
		include($_SERVER['DOCUMENT_ROOT']."/productos/Module/Perfil/Model/validaName.php");
		if ((isset($_POST['datosUsu']))){
			$jsondata = array();
            $usuJson = json_decode($_POST["datosUsu"], true);
            $rdoName = checkName($usuJson['name1']);

            if ($rdoName['resultName']){
            	try{
					$rdo = DAOperfil::updateName($usuJson['name1'],$usuJson['name2']);
					//echo json_encode($rdo);
					//exit;
				}catch(Exception $e) {
					$jsondata['redirect']="index.php?module=503";//canviar a module
			        header('HTTP/1.0 400 Bad error');
			        echo json_encode($jsondata);
			        exit;
				}

				if (!$rdo){
					$jsondata['redirect']="index.php?module=503";
		            header('HTTP/1.0 400 Bad error');
		            echo json_encode($jsondata);
		            exit;

				}else{
					$jsondata['success']=true;
					$_SESSION['name']=$usuJson['name2'];
		            echo json_encode($jsondata);
		            exit;
				}

            }else{
            	$jsondata['error']=$rdoName['error'];
            	header('HTTP/1.0 400 Bad error');
		        echo json_encode($jsondata);
		        exit;
            }
            
		}
	break;
	case 'deleteUsuario':
		//echo json_encode("estic en deleteUsuario");
		//exit;
		if ((isset($_GET['name']))){
			$jsondata = array();
            //echo json_encode($_GET['name']);
			//exit;
            try {
            	$rdo = DAOperfil::deleteUsuario($_GET['name']);

            } catch (Exception $e) {
            	$jsondata['redirect']="index.php?module=503";//canviar a module
			    header('HTTP/1.0 400 Bad error');
			    echo json_encode($jsondata);
			    exit;
            }
            
            if (!$rdo){
				$jsondata['redirect']="index.php?module=503";
		        header('HTTP/1.0 400 Bad error');
		        echo json_encode($jsondata);
		        exit;

			}else{
				$jsondata['success']=true;
				$jsondata['redirect']="index.php";
				session_destroy();
		        echo json_encode($jsondata);
		        exit;

			}
		}
	break;
	case 'getPedidos':
		//echo json_encode("estic en getPedidos");
		//exit;
		$jsondata = array();
		$display_json = array();

        try{
        	$rdo = DAOperfil::selectIdpedidos($_SESSION['name']);

        }catch (Exception $e) {
            $jsondata['redirect']="index.php?module=503";//canviar a module
			header('HTTP/1.0 400 Bad error');
			echo json_encode($jsondata);
			exit;
        }
            
        if (!$rdo){
			$jsondata['redirect']="index.php?module=503";
		    header('HTTP/1.0 400 Bad error');
		   	echo json_encode($jsondata);
		    exit;

		}else{
			foreach ($rdo as $row) {
				$jsondata["id_pedido"] = $row['id_pedido'];
				$jsondata["fecha"] = $row['fecha'];
				array_push($display_json, $jsondata);
				
			}
		    echo json_encode($display_json);
		    exit;

		}
	break;
	case 'getProductosPedido':
		$jsondata = array();
		$display_json = array();
		
		if ((isset($_POST['pedidoId']))){
			$jsondata = array();
            $idPedido_Json = json_decode($_POST["pedidoId"], true);

	        try{
	        	$rdo = DAOperfil::selectProdPedido($idPedido_Json['idPedido']);
	        	$rdoPrice = DAOperfil::selectPriceTotal($idPedido_Json['idPedido']);
	        	$rdoCantidad = DAOperfil::selectCantidad($idPedido_Json['idPedido']);

	        }catch (Exception $e) {
	            $jsondata['redirect']="index.php?module=503";//canviar a module
				header('HTTP/1.0 400 Bad error');
				echo json_encode($jsondata);
				exit;
	        }
	            
	        if (!$rdo || !$rdoPrice || !$rdoCantidad){
				$jsondata['redirect']="index.php?module=503";
			    header('HTTP/1.0 400 Bad error');
			   	echo json_encode($jsondata);
			    exit;

			}else{
				foreach ($rdo as $row) {
					$jsondata["media"] = $row['media'];
					$jsondata["name"] = $row['name'];
					$jsondata["brand"] = $row['brand'];
					$jsondata["price"] = $row['price'];
					foreach ($rdoPrice as $row) {
						$jsondata["precio_total"] = $row['precio_total'];

					}
					foreach ($rdoCantidad as $row) {
						$jsondata["cantidad"] = $row['sum(cantidad)'];

					}
					array_push($display_json, $jsondata);
					
				}

			    echo json_encode($display_json);
			    exit;

			}
		}
	break;
	case 'upload':
		include($_SERVER['DOCUMENT_ROOT']."/productos/Module/Perfil/Model/removeFileUpdate.php");
		$jsondata = array();
	    $result_avatar = upload_files();
	    $_SESSION['result_avatar'] = $result_avatar;
	    if ($result_avatar['resultado']){
	    	//tirar a BD
	    	try {
	    		//carregar-se la img que tenia l'usuari
	    		$rdoMedia = DAOperfil::selectDatosUser($_SESSION['name']);
	    		$rdo = DAOperfil::updateAvatar($_SESSION['name'],$result_avatar['datos']);

	    	} catch (Exception $e) {
	    		header('HTTP/1.0 400 Bad error');
		    	exit;
	    	}
	    	foreach ($rdoMedia as $value) {
	    		$media = $value['media'];

	    	}

	    	if ($rdo) {
	    		//echo json_encode($media);
		    	//exit;
	    		$remove = removeFile($media);
	    		$jsondata['success']=true;
		    	echo json_encode($jsondata);
		    	exit;

	    	}else{
		    	header('HTTP/1.0 400 Bad error');
		    	exit;

	    	}
	    }else{
	    	$jsondata['error']=$result_avatar['error'];
	    	header('HTTP/1.0 400 Bad error');
	    	echo json_encode($jsondata);
	    	exit;
	    }
	    
	break;
	case 'delete':
	    $result = remove_file();
	    if($result === true){
	        $_SESSION['result_avatar'] = array();//tornem a resetejar
	        echo json_encode(array("res" => true));
	    }else{
	        echo json_encode(array("res" => false));
	    }
	    
	break;
	//getLikesProd
	case 'getLikesProd':
	   // echo json_encode("getLikesProd");
	    //exit;
		$jsondata = array();
		$display_json = array();

        try{
        	$rdo = DAOperfil::selectLikes($_SESSION['name']);

        }catch (Exception $e) {
            $jsondata['redirect']="index.php?module=503";
			header('HTTP/1.0 400 Bad error');
			echo json_encode($jsondata);
			exit;
        }
            
        if (!$rdo){
			$jsondata['redirect']="index.php?module=503";
		    header('HTTP/1.0 400 Bad error');
		   	echo json_encode($jsondata);
		    exit;

		}else{
			foreach ($rdo as $row) {
				$jsondata["prod"] = $row['prod'];
				array_push($display_json, $jsondata);
				
			}
		    echo json_encode($display_json);
		    exit;

		}
	break;
	case 'getdatosProd':
	    //echo json_encode("getdatosProd");
	    //exit;
	if ((isset($_POST['prod']))){
		//echo json_encode($_POST['prod']);
	    //exit;
		$jsondata = array();
		$display_json = array();
		$prodJson = json_decode($_POST["prod"], true);

        try{
        	$rdo = DAOperfil::selectDatosProd($prodJson['prod']);

        }catch (Exception $e) {
            $jsondata['redirect']="index.php?module=503";
			header('HTTP/1.0 400 Bad error');
			echo json_encode($jsondata);
			exit;
        }
            
        if (!$rdo){
			$jsondata['redirect']="index.php?module=503";
		    header('HTTP/1.0 400 Bad error');
		   	echo json_encode($jsondata);
		    exit;

		}else{
			foreach ($rdo as $row) {
				$jsondata["prod"] = $row['name'];
				array_push($display_json, $jsondata);
				
			}
		    echo json_encode($display_json);
		    exit;

		}

	}
		
	break;
	}