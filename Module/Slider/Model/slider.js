$(document).ready(function () {
    // SET THIS VARIABLE FOR DELAY, 1000 = 1 SECOND
var delayLength = 4000;
    
function doMove(panelWidth, tooFar) {
    var leftValue = $("#mover").css("left");
    
    // Fix for IE
    if (leftValue == "auto") { leftValue = 0; };
    
    var movement = parseFloat(leftValue, 10) - panelWidth;
    
    if (movement == tooFar) {
        $(".slide img").animate({
            "top": -200
        }, function() {
            $("#mover").animate({
                "left": 0
            }, function() {
                $(".slide img").animate({
                    "top": 20
                });
            });
        });
    }
    else {
        $(".slide img").animate({
            "top": -200
        }, function() {
            $("#mover").animate({
                "left": movement
            }, function() {
                $(".slide img").animate({
                    "top": 20
                });
            });
        });
    }
}

$(function(){
    var $slide1 = $("#slide-1");

    var panelWidth = $slide1.css("width");
    var panelPaddingLeft = $slide1.css("paddingLeft");
    var panelPaddingRight = $slide1.css("paddingRight");

    panelWidth = parseFloat(panelWidth, 10);
    panelPaddingLeft = parseFloat(panelPaddingLeft, 10);
    panelPaddingRight = parseFloat(panelPaddingRight, 10);

    panelWidth = panelWidth + panelPaddingLeft + panelPaddingRight;
    
    var numPanels = $(".slide").length;
    var tooFar = -(panelWidth * numPanels);
    var totalMoverwidth = numPanels * panelWidth;
    $("#mover").css("width", totalMoverwidth);

    $("#slider").append('');

    sliderIntervalID = setInterval(function(){
        doMove(panelWidth, tooFar);
    }, delayLength);
    
    $("#slider-stopper").click(function(){
        if ($(this).text() == "Stop") {
            clearInterval(sliderIntervalID);
            $(this).text("Start");
        }
        else {
            sliderIntervalID = setInterval(function(){
                doMove(panelWidth, tooFar);
            }, delayLength);
            $(this).text("Stop");
        }
         
    });

});
    //pintem el slider
    $(".header_bottom_right").append('<div class="slider"><div id="slider"><div id="mover"><div id="slide-1" class="slide"><div class="slider-img"><a href="preview.html"><img src="View/images/slide-1-image.png" alt="learn more"/></a></div>'
        +'<div class="slider-text"><h1>Clearance<br><span>SALE</span></h1><h2>UPTo 20% OFF</h2><div class="features_list"><h4>Get to Know More About Our Memorable Services Lorem Ipsum is simply dummy text</h4></div>'
        +'<a href="preview.html" class="button">Shop Now</a></div><div class="clear"></div></div><div class="slide"><div class="slider-text"><h1>Clearance<br><span>SALE</span></h1><h2>UPTo 40% OFF</h2><div class="features_list">'
        +'<h4>Get to Know More About Our Memorable Services</h4></div><a href="preview.html" class="button">Shop Now</a></div><div class="slider-img">'
        +'<a href="preview.html"><img src="View/images/slide-3-image.jpg" alt="learn more" /></a></div><div class="clear"></div></div><div class="slide"><div class="slider-img">'
        +'<a href="preview.html"><img src="View/images/slide-2-image.jpg" alt="learn more" /></a></div><div class="slider-text"><h1>Clearance<br><span>SALE</span></h1><h2>UPTo 10% OFF</h2>'
        +'<div class="features_list"><h4>Get to Know More About Our Memorable Services Lorem Ipsum is simply dummy text</h4></div><a href="preview.html" class="button">Shop Now</a></div>'   
        +'<div class="clear"></div></div></div></div><div class="clear"></div></div>');
});