<?php
include($_SERVER['DOCUMENT_ROOT']."/productos/Module/Login/Model/DAOLogin.php");
@session_start();
switch($_GET['logop']){
	case "ingresar";
		include($_SERVER['DOCUMENT_ROOT']."/productos/Utils/validatePasswd.php");

		if(isset($_POST['ingresar'])){
			//echo json_encode("entra en ingresar login");
            //exit;
			$jsondata = array();
            $usuJson = json_decode($_POST["ingresar"], true);
            //print_r($usuJson);
            //exit;
			//obtinguem el password encriptat del usuari que ens estan passant
			try {
				$rdo=DAOLogin::selectPasswd($usuJson['nameUsu']);

			}catch (Exception $e) {
				$jsondata['redirect']="index.php?module=503";//canviar a module
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;
			}
			if (!$rdo) {
				$jsondata['redirect']="index.php?module=503";
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;

			}else{
				$countP = mysqli_num_rows($rdo);
				foreach ($rdo as $row) {
					@$p=$row['password'];//encripta
					//print_r($p);
				}

				//si existeix comprovem que el password encriptat i el password que ens pasa el usuari son iguals
				$rdopsswd=validatePasswd($countP,$usuJson['passwordUsu'], @$p);
				//print_r($rdopsswd);
				//exit;
				if ($rdopsswd['resultPsswd']){
					//comprobem que el nom i el password son correctes
					try {
						$rdo=DAOLogin::checkUser($usuJson['nameUsu'],$rdopsswd['password']);
						//print_r($rdo);
						//exit;
					}catch (Exception $e) {
						$jsondata['redirect']="index.php?module=503";
		                header('HTTP/1.0 400 Bad error');
		                echo json_encode($jsondata);
		                exit;
					}
					if (!$rdo){
						$jsondata['redirect']="index.php?module=503";
		                header('HTTP/1.0 400 Bad error');
		                echo json_encode($jsondata);
		                exit;
					}else{
						$countU = mysqli_num_rows($rdo);
						//print_r($countU);
						//exit;
						//si existex l'enviem a index per mostrar el CRUD
						if($countU == 1){
							//consulta sql del admin si es 0 o 1
							try {
								$rdo=DAOLogin::lookAdmin($usuJson['nameUsu'],$rdopsswd['password']);
								$rdoUsu=DAOLogin::lookUser($usuJson['nameUsu'],$rdopsswd['password']);
								//print_r($rdo);
								//exit;
							}catch (Exception $e) {
								$jsondata['redirect']="index.php?module=503";
				                header('HTTP/1.0 400 Bad error');
				                echo json_encode($jsondata);
				                exit;
							}
							foreach ($rdo as $row) {
								$p=$row['admin'];

							}
							//guardem el nom del usuari loguejat
							foreach ($rdoUsu as $row) {
								$u=$row['name'];

							}
							//print_r($u);
							//exit;
							if($p==1){//eres admin
								$_SESSION['login_admin'] = 1;
								$_SESSION['name'] = $u;
								//print_r($_SESSION['name']);
								//exit;
									
								$jsondata['success']=true;
								$jsondata['redirect']="index.php";
								echo json_encode($jsondata);
				                exit;

							}else{
								$_SESSION['login_client'] = 2;
								$_SESSION['name'] = $u;
									
								$jsondata['success']=true;
								$jsondata['redirect']="index.php";
								echo json_encode($jsondata);
				                exit;

							}
								
						}
					}//end else $rdo db true
						
				}else{
					$jsondata["error"] = $rdopsswd['error'];
					header('HTTP/1.0 400 Bad error');
	                echo json_encode($jsondata);
	                exit;
	                    
				}

			}//end else

		}//end post ingresar
			
    include($_SERVER['DOCUMENT_ROOT']."/productos/Module/Login/View/ingresar.php");
	break;
	case "registrar";
		include($_SERVER['DOCUMENT_ROOT']."/productos/Module/Login/Model/validateName.php");

		if(isset($_POST['registrar'])){
			$jsondata = array();
            $usuJson = json_decode($_POST["registrar"], true);
			
			try {
				$rdo=DAOLogin::checkName($usuJson['nameR']);
			    //print_r($rdo);
            	//exit;
			} catch (Exception $e) {
				$jsondata['redirect']="index.php?module=503";
				header('HTTP/1.0 400 Bad error');
				echo json_encode($jsondata);
				exit;

			}				
			$countN = mysqli_num_rows($rdo);
			$rdoName = checkName($countN);
			//print_r($rdoName);
			//exit;
				
			if ($rdoName['resultName']){
				//el nom i el passwd ha pasat tot el filtre i ara insertarem l'usuari a la BD amb passwd encriptat
				$_SESSION['name'] = $usuJson['nameR'];

				try{
					$rdo=DAOLogin::insertUser($usuJson['nameR'],$usuJson['passwordR']);
						
				}catch (Exception $e) {
					$jsondata['redirect']="index.php?module=503";
					header('HTTP/1.0 400 Bad error');
					echo json_encode($jsondata);
					exit;

				}
				if(!$rdo) {
					$jsondata['redirect']="index.php?module=503";
					header('HTTP/1.0 400 Bad error');
					echo json_encode($jsondata);
					exit;

				}else{
					$jsondata["success"] = true;
                    $jsondata['redirect']= 'index.php?module=Login&view=resultsUser';
                    echo json_encode($jsondata);
                    exit;

				}	

			}else{
				$jsondata["error"] = $rdoName['error'];
	            //print_r($jsondata);
	            //exit;
	            header('HTTP/1.0 400 Bad error');
				echo json_encode($jsondata);
	            exit;
			}
						
		}

		//////////////////////////// load prod to listUser///////////////////////////
        if (isset($_GET["load"]) && $_GET["load"] == true){
            $jsondata = array();
            //print_r(@$_SESSION['name']);
            //exit;
            if (isset($_SESSION['name'])) {
                $jsondata["name"] = $_SESSION['name'];
                    
            }
                
            echo json_encode($jsondata);
           	exit;
        }

		include($_SERVER['DOCUMENT_ROOT']."/productos/Module/Login/View/registrar.php");
	
	break;
	case "logout";
	   session_start();
	   if(session_destroy()) {
	      header("Location: index.php");
	   }
	break;
	default;
        header("location: index.php");
	break;
}
?>