<link href="View/css/formlogin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="Module/Login/Model/validateLoginClient.js"></script>
   <!-- Main-Content -->
   <section class="wrap fondo">
      <div class="main-w3layouts-form">
         <h2>Login to your account</h2>
         <form name="for1" method = "post">
           <!--fields-w3-agileits-->
            <div class="">
               <span class="fa fa-user" aria-hidden="true"><input type="text" name="nameUsu" id="nameUsu" title="Escribe su usuario" /></span>
            </div>
            <div class="">
               <span class="fa fa-key" aria-hidden="true"><input type="password" name="passwordUsu" id="passwordUsu" title="Escribe su password" /></span>
            </div>
            <div class="remember-section-wthree">
               <ul>
                  <li> <a href="index.php?module=Login&logop=registrar">¿Aun no estas registrado?</a> </li>
               </ul>
               <div class="clear"> </div>
            </div>
            <input type="button" name="createUser" id="createUser" value="Login"/>
         </form>
         <!-- Social icons -->
         <div class="footer_grid-w3ls">
            <h5 class="sub-hdg-w3l">or login with your social profile</h5>
            <ul class="social-icons-agileits-w3layouts">
               <li><a href="#" id="buttonFacebook" class="fa fa-facebook"></a></li>
            </ul>
         </div>
         <!--// Social icons -->
      </div>
   </section>