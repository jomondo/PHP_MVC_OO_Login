   <link href="View/css/formlogin.css" rel="stylesheet" type="text/css" />
   <script type="text/javascript" src="Module/Login/Model/validateRegistClient.js"></script>
   <script type="text/javascript" src="Module/Login/Model/LoginFacebook.js"></script>
   
   <section class="wrap fondo">
      <div class="main-w3layouts-form">
         <h2>Register your account</h2>
         <form name="for2" method = "post">
           <!--fields-w3-agileits-->
            <div class="">
               <span class="fa fa-user" aria-hidden="true"><input type="text" name="nameR" id="nameR" title="Escribe su usuario"/></span>
            </div>
            <div class="">
               <span class="fa fa-key" aria-hidden="true"><input type="password" name="passwordR" id="passwordR" title="Escribe su password"/></span>
            </div>
            <div class="remember-section-wthree">
               
               <div class="clear"> </div>
            </div>
            <input type="button" id="regist" value="Register"/>
         </form>
         <!-- Social icons -->
         <div class="footer_grid-w3ls">
            <h5 class="sub-hdg-w3l">or login with your social profile</h5>
            <ul class="social-icons-agileits-w3layouts">
               <div id="fb-root"></div>
               <fb:login-button scope="public_profile,email" onlogin="checkLoginState();" class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="true" data-use-continue-as="true" id="buttonFacebook">
               </fb:login-button>
               <h5 class="sub-hdg-w3l"><div id="status"></div></h5>
               
            </ul>
         </div>
         <!--// Social icons -->
      </div>
   </section>