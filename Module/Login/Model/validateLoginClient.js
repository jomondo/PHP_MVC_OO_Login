$(document).ready(function () {

        var namereg = /^[A-Za-z\s]{4,15}$/;
        var passreg = /^[a-z0-9]{6,20}$/;
        
        //fadeOuts errors
            $("#nameUsu").keyup(function(){
                if ( $(this).val() != "" && namereg.test($(this).val()) ){
                    $(".error").fadeOut("slow");
                    return false;
                }
            });
            
            $("#passwordUsu").keyup(function(){   
                if ( $(this).val() != ""  && passreg.test($(this).val()) ){
                    $(".error").fadeOut("slow");
                    return false;
                }
            });
       
        $("#createUser").click(function(){
            var rdo = true;
            
            var namereg = /^[A-Za-z\s]{4,15}$/;
            var passreg = /^[a-z0-9]{6,20}$/;
            
            
            var nameUsu = document.getElementById('nameUsu').value;
            var passwordUsu = document.getElementById('passwordUsu').value;

            $(".error").remove();
            //comprovem camps buits i patterns

            if( $("#nameUsu").val() == ""){
                $("#nameUsu").focus().after("<span class='error'>Ingrese su nombre</span>");
                rdo=false;
                return false;
            }else if(!namereg.test($("#nameUsu").val())){
                $("#nameUsu").focus().after("<span class='error'>Mínimo 4 carácteres y maximo 15 para el nombre.</span>");
                rdo=false;
                return false;
            }if( $("#passwordUsu").val() == ""){
                $("#passwordUsu").focus().after("<span class='error'>Ingrese su password</span>");
                rdo=false;
                return false;
            }else if(!passreg.test($("#passwordUsu").val())){
                $("#passwordUsu").focus().after("<span class='error'>6 a 20 caracteres</span>");
                rdo=false;
                return false;
            }
     
            if (rdo) {
                console.log("el rdo es true en js");
                var data={"nameUsu":nameUsu,"passwordUsu":passwordUsu};
                var data_usu_JSON = JSON.stringify(data);
                console.log("json debug: "+data_usu_JSON);

                $.post('Module/Login/Controller/controller_Login.class.php?logop=ingresar',{ingresar: data_usu_JSON},function (response){
                    alert(response);
                    var json = JSON.parse(response);//important!!
                    //alert(json.success);
                    //alert(data_prods_JSON);
                    console.log("success: "+json.success);
                    if (json.success) {
                        window.location.href = json.redirect;
                    }
                    
                   }).fail(function(xhr){
                        xhr.responseJSON = JSON.parse(xhr.responseText);
                        $.amaran({
                            'message'           :'Ha habido algun error en el servidor',
                            'cssanimationIn'    :'tada',
                            'cssanimationOut'   :'rollOut',
                            'closeButton'       :true,
                            'closeOnClick'      :true
                        });

                        if (xhr.responseJSON.error.namePassword){
                            $("#nameUsu").focus().after("<span  class='error1'>" + xhr.responseJSON.error.namePassword + "</span>");
                            
                        }

                        if (xhr.responseJSON.error.password){
                            $("#passwordUsu").focus().after("<span  class='error1'>"+xhr.responseJSON.error.password+"</span>");
                         
                        }

                        if (xhr.responseJSON.error.redirect){
                            window.location.href = xhr.responseJSON.error.redirect;
                         
                        }


                    });//end fail

            }//end rdo

        });//end click

   
    
    });//end read