$(document).ready(function () {
    var namereg = /^[A-Za-z\s]{4,15}$/;
        var passreg = /^[a-z0-9]{6,20}$/;
    //fadeOuts errors
    $("#nameR").keyup(function(){
        if ( $(this).val() != "" && namereg.test($(this).val()) ){
            $(".error").fadeOut("slow");
            return false;
        }
    });
            
    $("#passwordR").keyup(function(){   
        if ( $(this).val() != ""  && passreg.test($(this).val()) ){
            $(".error").fadeOut("slow");
            return false;
        }
    });
    $("#regist").click(function(){
        var rdo = true;
            
        var namereg = /^[A-Za-z\s]{4,15}$/;
        var passreg = /^[a-z0-9]{6,20}$/;
            
            
        var nameR = document.getElementById('nameR').value;
        var passwordR = document.getElementById('passwordR').value;
        
        $(".error").remove();
        //comprovem camps buits i patterns

        if( $("#nameR").val() == ""){
            $("#nameR").focus().after("<span class='error'>Ingrese su nombre</span>");
            rdo=false;
            return false;
        }else if(!namereg.test($("#nameR").val())){
            $("#nameR").focus().after("<span class='error'>Mínimo 4 carácteres y maximo 15 para el nombre.</span>");
            rdo=false;
            return false;
        }if( $("#passwordR").val() == ""){
            $("#passwordR").focus().after("<span class='error'>Ingrese su password</span>");
            rdo=false;
            return false;
        }else if(!passreg.test($("#passwordR").val())){
            $("#passwordR").focus().after("<span class='error'>6 a 20 caracteres</span>");
            rdo=false;
            return false;
        }
     
        if (rdo) {
            console.log("el rdo es true en js");
            var data={"nameR":nameR,"passwordR":passwordR};
            var data_usu_JSON = JSON.stringify(data);
            console.log("json debug: "+data_usu_JSON);

            $.post('Module/Login/Controller/controller_Login.class.php?logop=registrar',{registrar: data_usu_JSON},function (response){
                alert(response);
                var json = JSON.parse(response);
                //alert(json.success);
                //alert(data_prods_JSON);
                console.log("success: "+json.success);
                if (json.success) {
                    window.location.href = json.redirect;
                }
                
               }).fail(function(xhr){
                    xhr.responseJSON = JSON.parse(xhr.responseText);
                    $.amaran({
                        'message'           :'Ha habido algun error en el servidor',
                        'cssanimationIn'    :'tada',
                        'cssanimationOut'   :'rollOut',
                        'closeButton'       :true,
                        'closeOnClick'      :true
                    });

                    if (xhr.responseJSON.error.nameR){
                        $("#nameR").focus().after("<span  class='error1'>" + xhr.responseJSON.error.nameR + "</span>");
                        
                    }

                    if (xhr.responseJSON.error.passwordR){
                        $("#passwordR").focus().after("<span  class='error1'>"+xhr.responseJSON.error.passwordR+"</span>");
                     
                    }
                    
                    if (xhr.responseJSON.error.redirect){
                        window.location.href = xhr.responseJSON.error.redirect;
                         
                    }


                });//end fail

            }//end rdo

        });//end click
});//end read