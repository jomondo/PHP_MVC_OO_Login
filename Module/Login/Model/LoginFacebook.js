
  function statusChangeCallback(response) {
    if(response.status === 'connected') {
      testAPI();
    }else{
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }

  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }
  

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '2100061583613679',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.8' // use graph api version 2.8
    });

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });

  };

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.12&appId=2100061583613679&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

  function testAPI() {
    FB.api('/me', function(response) {
      //alert(response.name);
      console.log('Successful login for: ' + response.name);
      /*document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';*/
      $('#nameR').val(response.name);
    });
  }

  function salir(){
    FB.logout(function(response) {
     //alert(response.status);
     checkLoginState();

    });
  }
  
  $(document).on('click', '#salirFacebook', function(event) {
    salir();
  });