function initMap() {
	var uluru = {lat: 38.810882, lng: -0.6041189000000031}; //insti
	var map = new google.maps.Map(document.getElementById('map'), { zoom: 16, center: uluru }); 
	var marker = new google.maps.Marker({ position: uluru, map: map });//delete??

	var contentString = '<div id="content">'+
                      '<div id="siteNotice">'+
                      '</div>'+
                      '<h1 id="firstHeading" class="firstHeading">IES Lestacio</h1>'+
                      '<div id="bodyContent">'+
                      '<p><b>IES Lestacio</b>, Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor '+
                      'incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse '+
                      'cillum dolore eu fugiat nulla pariatur.</p>'+
                      '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
                      'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
                      '(last visited June 22, 2009).</p>'+
                      '</div>'+
                      '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        var marker = new google.maps.Marker({
          position: uluru,
          map: map,
          title: 'IES Lestacio (Ayers Rock)'
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });

}
