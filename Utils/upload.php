<?php 
function upload_files() {
	$error = "";
	$extensiones = array('jpg', 'jpeg', 'gif', 'png', 'bmp');
	$filename=$_FILES["file"]["name"];
	$filetype = $_FILES["file"]["type"];
	$filesize = $_FILES["file"]["size"];

    if(!isset($_FILES)) {
        $error .=  'No existe $_FILES <br>';
    }
    if(!isset($_FILES['file'])) {
        $error .=  'No existe $_FILES[file] <br>';
    }

    $error_fitxer=$_FILES['file']['error'];

    if ($error_fitxer>0) { //error 0 = fitxer correcte
        switch ($error_fitxer){
            case 1: $error .=  'Fitxer major que upload_max_filesize <br>'; break;
            case 2: $error .=  'Fitxer major que max_file_size <br>';break;
            case 3: $error .=  'Fitxer només parcialment pujat <br>';break;
            
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    if ($filesize > 155000 ){
        $error .=  "Large File Size <br>";
    }

    if ($filename !== "") {
        ////////////////////////////////////////////////////////////////////////////
        @$extension = strtolower(end(explode('.', $_FILES['file']['name']))); // Obtenemos la extensión, en minúsculas para poder comparar
        if( ! in_array($extension, $extensiones)) {
            $error .=  'Sólo se permite subir archivos con estas extensiones: ' . implode(', ', $extensiones).' <br>';
        }
        
        if (!@getimagesize($_FILES['file']['tmp_name'])){
            $error .=  "Invalid Image File... <br>";
        }
        ////////////////////////////////////////////////////////////////////////////
        list($width, $height, $type, $attr) = @getimagesize($_FILES['file']['tmp_name']);
        if ($width > 500 || $height > 500){
            $error .=   "Maximum width and height exceeded. Please upload images below 500x500 px size <br>";
        }
    } 


///////////////////////////////////////////////////////////////////////////////////
	if($error==""){
        //moguem el arxiu a la ruta especificada
        $id=rand();
		$ruta="/productos/media/dropzone/";
        $nom_file=$ruta.$id.$filename;

		if($filename && move_uploaded_file($_FILES["file"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'].$nom_file)){
            $_SESSION['name_file']=$nom_file;
			return $return=array('resultado'=>true,'error'=>"",'datos'=>$nom_file);

		}else{
            return $return=array('resultado' => true, 'error' => "", 'datos' => '/productos/media/dropzone/img_default.jpg');
        }

	}else{
		return $return=array('resultado'=>false,'error'=>$error,'datos'=>"");

	}
}

function remove_file(){
    if(file_exists($_SERVER['DOCUMENT_ROOT'].$_SESSION['name_file'])){
        unlink($_SERVER['DOCUMENT_ROOT'].$_SESSION['name_file']);
        return true;
    }else{
        return false;
    }
}