<?php
	ob_start();
	session_start();
	
	include("View/Include/header.html");
	 
	//menu
	if(@$_SESSION['login_admin'] == 1){
	    include("View/Include/menuAdmin.php");
		
	}elseif(@$_SESSION['login_client'] == 2){
		include("View/Include/menuClient.php");
		
	}else{
		include("View/Include/menuUser.php");
	}

	//modules
	if((isset($_GET['module'])) && (isset($_GET['view']))){
	    include("Module/".$_GET['module']."/View/".$_GET['view'].".php");
	    
	}else if ((!isset($_GET['module'])) && (!isset($_GET['view']))){//al iniciar la web
		include("Module/Home/View/resultsDummiesHome.php");

	}else if ((isset($_GET['module'])) && (!isset($_GET['view']))){
	    require_once("Module/".$_GET['module']."/Controller/controller_".$_GET['module'].".class.php");
	    
	}

	//footer
	include("View/Include/footer.html");
	ob_end_flush();
