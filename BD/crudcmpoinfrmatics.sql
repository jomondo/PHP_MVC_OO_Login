-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-03-2018 a las 20:50:11
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crudcmpoinfrmatics`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cmpoinfrmatics`
--

CREATE TABLE `cmpoinfrmatics` (
  `id` int(5) NOT NULL,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fabricationDate` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `discounts` int(2) NOT NULL,
  `color` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `brand` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `availability` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `quantity` int(4) NOT NULL,
  `agency` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `price` float NOT NULL,
  `description` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `media` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cmpoinfrmatics`
--

INSERT INTO `cmpoinfrmatics` (`id`, `name`, `model`, `fabricationDate`, `discounts`, `color`, `brand`, `availability`, `quantity`, `agency`, `price`, `description`, `tipo`, `media`) VALUES
(97, 'placa base', 'M0212', '22/12/2017', 0, 'Rojo', 'MSI', 'Stock', 22, 'Correos:MRW:', 181, 'Buen producto', 'Placas base', 'media/placabase.jpg'),
(99, 'memoria RAM', 'T9842', '25/10/2015', 0, 'Rojo', 'Kinstong', 'Stock', 8, 'Correos:MRW:DHL:', 85, 'Buen producto', 'Memorias Ram', 'media/ram.jpg'),
(138, 'Tv plusX', 'J3856', '11/01/2017', 4, 'Plata', 'Benq', 'Stock', 7, 'MRW:DHL:', 250, 'buena tv', 'TV', 'media/pantalla.jpg'),
(139, 'Raton optico', 'J3856', '11/01/2017', 4, 'Azul', 'MSI', 'Stock', 4, 'Correos:MRW:', 26, 'sdsd', 'Ratones', 'media/raton.jpg'),
(150, 'pantalla msi', 'F4468', '11/01/2017', 4, 'Negro', 'MSI', 'Stock', 6, 'MRW:DHL:', 26, 'sdfdf', 'Monitores', 'media/pantalla.jpg'),
(151, 'ventilador cpu', 'X4564', '07/01/2015', 4, 'Plata', 'Kinstong', 'Fuera de stock', 4, 'Correos:DHL:Seur:', 55, 'ventilador duradero', 'Ventilacion', 'media/ventiladorCpu.jpg'),
(152, 'mando tv', 'F4468', '11/01/2017', 0, 'Plata', 'Benq', 'Stock', 5, 'Correos:MRW:', 44, 'mando tv de calidad', 'TV', 'media/mandoTv.jpg'),
(153, 'placa base kj', 'X4564', '11/01/2017', 23, 'Negro', 'LG', 'Stock', 4, 'Correos:MRW:', 44, 'sdsdsd', 'Placas base', 'media/placabase.jpg'),
(154, 'raton wireless', 'X4564', '11/01/2017', 0, 'Negro', 'LG', 'Stock', 7, 'MRW:DHL:', 26, 'raton que se conecta por wifi', 'Ratones', 'media/ratonTrust.jpg'),
(169, 'carcasa msi', 'X4564', '11/01/2017', 4, 'Rojo', 'MSI', 'Stock', 4, 'Correos:MRW:', 115, 'Carcasa duradera atx', 'Carcasas', 'media/carcasa.jpg'),
(170, 'disco duro hdd', 'C4567', '11/01/2017', 4, 'Negro', 'LG', 'Stock', 4, 'Correos:MRW:', 26, 'capacidad de lectura buena', 'Discos duro', 'media/discoSsd.jpg'),
(172, 'carcasa', 'C4567', '11/01/2017', 4, 'Negro', 'LG', 'Stock', 4, 'Correos:MRW:', 44, 'sdsd', 'Carcasas', 'media/carcasa.jpg'),
(173, 'boligrafo', 'X4569', '11/01/2017', 4, 'Negro', 'LG', 'Stock', 4, 'Correos:MRW:', 26, 'dsfdf', 'Accesorios electronicos', 'media/boligrafoCamara.jpg'),
(175, 'targeta grafica', 'C4567', '11/01/2017', 4, 'Negro', 'MSI', 'Stock', 4, 'MRW:DHL:Seur:', 145, 'targeta grafica con dirext 12', 'Tarjetas graficas', 'media/tarjetaGrafica.jpg'),
(176, 'targeta msi', 'C4567', '11/01/2017', 0, 'Negro', 'MSI', 'Stock', 3, 'Correos:MRW:', 26, 'targeta grafica con buena ventilacion', 'Targetas graficas', 'media/tarjetaGrafica.jpg'),
(184, 'Philips cascos', 'J3856', '11/01/2017', 4, 'Negro', 'Philips', 'Stock', 12, 'Correos:MRW:', 150, 'headphones con buena salida de audio', 'Accesorios', 'media/cascos.jpg'),
(185, 'Trust teclado', 'F4567', '11/01/2017', 0, 'Negro', 'LG', 'Stock', 7, 'Correos:MRW:', 44, 'sdsd', 'Accesorios', 'media/teclado.jpg'),
(190, 'Placa base asus', 'F5678', '26/08/2017', 0, 'Negro', 'MSI', 'Stock', 45, 'MRW', 140, 'Placa base gaming', 'Placas base', 'media/placabaseAsus.jpg'),
(191, 'Placa base asus ATX', 'F5648', '26/10/2017', 0, 'Plata', 'MSI', 'Fuera de stock', 35, 'MRW', 120, 'Placa base gaming', 'Placas base', 'media/placabaseAsus.jpg'),
(192, 'Placa base msi', 'F9678', '23/08/2017', 0, 'Negro', 'MSI', 'Stock', 5, 'MRW', 85, 'Placa base gaming para juegadores', 'Placas base', 'media/placabaseAsus.jpg'),
(193, 'Placa base msi ATX', 'F3678', '20/11/2017', 0, 'Plata', 'MSI', 'Fuera de stock', 35, 'MRW', 126, 'Placa base gaming', 'Placas base', 'media/placabaseAsus.jpg'),
(194, 'Monitor Samsung 19\'\'', 'G3678', '10/09/2017', 0, 'Plata', 'Samsung', 'Stock', 35, 'MRW', 126, 'Monitor con alta frequencia', 'Monitores', 'media/monitor.jpg'),
(195, 'Monitor LG 18\'\'', 'L3778', '10/01/2018', 0, 'Negro', 'LG', 'Stock', 15, 'MRW:Correos:DHL', 216, 'Monitor con alta frequencia', 'Monitores', 'media/monitor.jpg'),
(196, 'Monitor Benq 20\'\'', 'L4678', '10/09/2017', 0, 'Plata', 'Benq', 'Stock', 5, 'DHL', 286, 'Monitor con alta frequencia', 'Monitores', 'media/monitor.jpg'),
(197, 'Raton Kinstong optico', 'R4678', '15/09/2017', 0, 'Verde', 'Kinstong', 'Stock', 105, 'DHL:Seur', 186, 'Raton con alta frequencia', 'Ratones', 'media/ratonTrust.jpg'),
(198, 'Raton optico', 'R5678', '15/09/2017', 0, 'Verde', 'Sarp', 'Stock', 85, 'Seur', 86, 'Raton con alta respuesta', 'Ratones', 'media/ratonTrust.jpg'),
(199, 'Raton Xiaomi', 'R0678', '11/05/2017', 0, 'Violeta', 'Xiaomi', 'Stock', 75, 'Seur', 66, 'Raton con alta respuesta', 'Ratones', 'media/ratonTrust.jpg'),
(200, 'Carcasa Kinstong Up', 'R0678', '11/05/2017', 0, 'Violeta', 'Kinstong', 'Stock', 75, 'Seur', 66, 'Raton con alta respuesta', 'Carcasas', 'media/carcasa.jpg'),
(201, 'Carcasa later', 'R0678', '11/05/2017', 0, 'Violeta', 'Kinstong', 'Stock', 75, 'DHL:Seur:', 145, 'carcasa alto nivel robusta', 'Carcasas', 'media/carcasa.jpg'),
(202, 'Carcasa Kinston', 'R0678', '11/05/2017', 0, 'Oro', 'Kinstong', 'Stock', 75, 'Correos:MRW:DHL:Seur', 66, 'Raton con alta respuesta', 'Carcasas', 'media/carcasa.jpg'),
(203, 'Monitor', 'F5689', '11/01/2017', 0, 'Negro', 'Benq', 'Stock', 6, 'Correos:MRW:', 145, 'dilluns mati 12', 'Monitores', 'media/monitor.jpg'),
(204, 'disco duro ssd', 'F6799', '11/01/2017', 3, 'Plata', 'Samsung', 'Stock', 7, 'Correos:MRW:DHL:Seur', 69, 'disco duro ssd con buena capacidad y rapidez', 'Discos duro', NULL),
(210, 'monitor', 'J3856', '31/01/2016', 0, 'Negro', 'Sarp', 'Stock', 44, 'Correos:MRW:', 185, 'monitor sarp con buena calidad imagen', 'Monitores', NULL),
(212, 'tarjeta px', 'F4468', '18/01/2017', 23, 'Rojo', 'MSI', 'Stock', 88, 'Correos:MRW:', 164, 'tarjeta grafica 4gb', 'Tarjetas graficas', 'media/tarjetaGrafica.jpg'),
(215, 'memoria ram x', 'G4560', '15/01/2009', 0, 'Oro', 'Samsung', 'Fuera de stock', 3, 'Correos:MRW:DHL:Seur', 39, 'asdfsfsfsfsfdf', 'Tarjetas graficas', NULL),
(217, 'disco duro', 'D3456', '15/01/2014', 0, 'Negro', 'Kinstong', 'Stock', 57, 'Correos:MRW:', 54, 'buena capacidad de almacenaje', 'Discos duro', 'media/discoHdd.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `id_comentario` int(10) NOT NULL,
  `id_usuario` char(25) NOT NULL,
  `id_prod` char(50) NOT NULL,
  `mensaje` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`id_comentario`, `id_usuario`, `id_prod`, `mensaje`) VALUES
(76, 'ricardo', '97', 'primer comentario ricardo'),
(77, 'ricardo', '97', 'segundo comentario ricardo'),
(78, 'ricardo', '99', 'primer comentario ricardo'),
(79, 'ricardo', '138', 'primer comentario ricardo'),
(80, 'ricardo', '139', 'primer comentario ricardo'),
(81, 'ricardo', '150', 'primer comentario ricardo'),
(82, 'ricardo', '151', 'primer comentario ricardo'),
(83, 'joanet', '97', 'primer comentario joanet'),
(84, 'joanet', '99', 'primer comentario joanet'),
(85, 'joanet', '138', 'primer comentario joanet'),
(86, 'joanet', '139', 'primer comentario joanet'),
(87, 'joanet', '150', 'primer comentario joanet'),
(88, 'joanet', '151', 'primer comentario joanet'),
(89, 'joaquim', '97', 'primer comentario joaquim'),
(94, 'joaquim', '139', 'primer comentario joaquim'),
(95, 'joaquim', '138', 'primer comentario joaquim'),
(96, 'joaquim', '99', 'primer comentario joaquim'),
(97, 'joaquim', '152', 'primer comentario joaquim');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_pedido`
--

CREATE TABLE `detalle_pedido` (
  `id_pedido` int(10) NOT NULL,
  `id_prod` char(10) NOT NULL,
  `precio` double NOT NULL,
  `cantidad` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_pedido`
--

INSERT INTO `detalle_pedido` (`id_pedido`, `id_prod`, `precio`, `cantidad`) VALUES
(4296, '151', 26, 1),
(4296, '99', 85, 2),
(5238, '151', 26, 1),
(7717, '138', 250, 5),
(7717, '154', 26, 2),
(7717, '169', 26, 4),
(7717, '97', 181, 1),
(9321, '139', 26, 3),
(9321, '99', 85, 1),
(15907, '139', 26, 2),
(16179, '138', 250, 1),
(16179, '152', 44, 2),
(16179, '153', 44, 1),
(17625, '151', 26, 1),
(17625, '99', 85, 2),
(18091, '139', 26, 4),
(18091, '153', 44, 2),
(18091, '169', 26, 3),
(19746, '151', 26, 1),
(19746, '154', 26, 2),
(19746, '169', 26, 1),
(21147, '151', 26, 2),
(21147, '154', 26, 2),
(21147, '99', 85, 1),
(23921, '97', 181, 2),
(23921, '99', 85, 4),
(24104, '138', 250, 4),
(24383, '151', 26, 2),
(24383, '152', 44, 5),
(25844, '152', 44, 2),
(25844, '99', 85, 1),
(30407, '152', 44, 1),
(30407, '169', 26, 1),
(30407, '99', 85, 3),
(30714, '193', 126, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dummies`
--

CREATE TABLE `dummies` (
  `id` int(5) NOT NULL,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fabricationDate` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `discounts` int(2) NOT NULL,
  `color` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `brand` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `availability` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `quantity` int(4) NOT NULL,
  `agency` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `price` float NOT NULL,
  `description` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `media` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `dummies`
--

INSERT INTO `dummies` (`id`, `name`, `model`, `fabricationDate`, `discounts`, `color`, `brand`, `availability`, `quantity`, `agency`, `price`, `description`, `media`) VALUES
(85, 'Smartphone BQ', 'H4467', '20/07/2016', 0, 'black', 'BQ', 'Stock', 45, 'MRW', 165, 'Buen Smartphone', 'View/Imagenes/bq.jpg'),
(86, 'Memoria RAM KG', 'G4278', '26/07/2016', 6, 'gold', 'Kingston', 'Stock', 45, 'Correos', 65, 'Buen Smartphone', 'View/Imagenes/ram.jpg'),
(87, 'Placa Base Dark', 'G9275', '26/07/2016', 6, 'red', 'MSI', 'Stock', 45, 'MRW', 105, 'Buen Smartphone', 'View/Imagenes/placabase.jpg'),
(88, 'Ventiladores Cpu', 'V4578', '14/02/2017', 0, 'black', 'Cooler Master', 'Stock', 22, 'MRW', 42, 'Buen Smartphone', 'View/Imagenes/ventiladorCpu.jpg'),
(89, 'Teclado cherry XONE', 'T4578', '11/03/2017', 0, 'white', 'Cherry', 'Fuera de stock', 0, 'MRW', 65, 'Buen Tecladito', 'View/Imagenes/teclado.jpg'),
(90, 'Cpu i9 ultimate generation', 'C1578', '11/03/2017', 0, 'white', 'Intel', 'stock', 70, 'MRW', 350, 'Buena cpu', 'View/Imagenes/cpu.jpg'),
(91, 'pantalla hd', 'K1578', '22/05/2017', 0, 'black', 'Samsung', 'stock', 20, 'MRW', 285, 'Buena pantalla', 'View/Imagenes/pantalla.jpg'),
(92, 'Smartphone BQ', 'H4467', '20/07/2016', 0, 'black', 'BQ', 'Stock', 45, 'MRW', 165, 'Buen Smartphone', 'View/Imagenes/bq.jpg'),
(110, 'Smartphone BQ', 'H4467', '20/07/2016', 0, 'black', 'BQ', 'Stock', 45, 'MRW', 165, 'Buen Smartphone', 'View/Imagenes/bq.jpg'),
(111, 'Memoria RAM KG', 'G4278', '26/07/2016', 6, 'gold', 'Kingston', 'Stock', 45, 'Correos', 65, 'Buen Smartphone', 'View/Imagenes/ram.jpg'),
(125, 'Memoria RAM KG', 'G4278', '26/07/2016', 6, 'gold', 'Kingston', 'Stock', 45, 'Correos', 65, 'Buen Smartphone', 'View/Imagenes/ram.jpg'),
(127, 'Ventiladores Cpu', 'V4578', '14/02/2017', 0, 'black', 'Cooler Master', 'Stock', 22, 'MRW', 42, 'Buen Smartphone', 'View/Imagenes/ventiladorCpu.jpg'),
(128, 'Teclado cherry XONE', 'T4578', '11/03/2017', 0, 'white', 'Cherry', 'Fuera de stock', 0, 'MRW', 65, 'Buen Tecladito', 'View/Imagenes/teclado.jpg'),
(129, 'Cpu i9 ultimate generation', 'C1578', '11/03/2017', 0, 'white', 'Intel', 'stock', 70, 'MRW', 350, 'Buena cpu', 'View/Imagenes/cpu.jpg'),
(130, 'pantalla hd', 'K1578', '22/05/2017', 0, 'black', 'Samsung', 'stock', 20, 'MRW', 285, 'Buena pantalla', 'View/Imagenes/pantalla.jpg'),
(131, 'Smartphone BQ', 'H4467', '20/07/2016', 0, 'black', 'BQ', 'Stock', 45, 'MRW', 165, 'Buen Smartphone', 'View/Imagenes/bq.jpg'),
(132, 'Memoria RAM KG', 'G4278', '26/07/2016', 6, 'gold', 'Kingston', 'Stock', 45, 'Correos', 65, 'Buen Smartphone', 'View/Imagenes/ram.jpg'),
(133, 'Placa Base Dark', 'G9275', '26/07/2016', 6, 'red', 'MSI', 'Stock', 45, 'MRW', 105, 'Buen Smartphone', 'View/Imagenes/placabase.jpg'),
(134, 'Ventiladores Cpu', 'V4578', '14/02/2017', 0, 'black', 'Cooler Master', 'Stock', 22, 'MRW', 42, 'Buen Smartphone', 'View/Imagenes/ventiladorCpu.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id_pedido` int(10) NOT NULL,
  `id_usuario` varchar(30) NOT NULL,
  `precio_total` double NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id_pedido`, `id_usuario`, `precio_total`, `fecha`, `hora`) VALUES
(4296, 'ricardo', 196, '2018-02-23', '21:39:22'),
(5238, 'joanet', 26, '2018-02-21', '22:22:36'),
(7717, 'joaquim', 1587, '2018-02-26', '10:50:43'),
(9321, 'joaquim', 163, '2018-02-26', '10:39:28'),
(15907, 'joanet', 52, '2018-02-23', '20:17:25'),
(16179, 'joaquim', 382, '2018-02-19', '10:35:48'),
(17625, 'ricardo', 196, '2018-02-21', '11:32:39'),
(18091, 'Joan El Gran', 270, '2018-03-05', '19:36:22'),
(19746, 'ricardo', 104, '2018-02-19', '10:37:30'),
(21147, 'ricardo', 189, '2018-02-21', '11:36:44'),
(23921, 'joanet', 702, '2018-03-05', '18:45:17'),
(24104, 'ricardo', 1000, '2018-02-26', '11:59:19'),
(24383, 'ricardo', 272, '2018-03-05', '19:00:43'),
(25844, 'joanet', 173, '2018-02-23', '21:28:52'),
(30407, 'joanet', 325, '2018-02-23', '19:49:23'),
(30714, 'ricardo', 252, '2018-02-26', '11:29:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(16) NOT NULL,
  `password` varchar(255) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `media` varchar(125) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

Estructura de tabla para la tabla `likes`
--

CREATE TABLE `likes` (
  `id_usuario` varchar(45) NOT NULL,
  `prod` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `likes`
--

INSERT INTO `likes` (`id_usuario`, `prod`) VALUES
('joanet', 97),
('joanet', 99),
('ricardo', 97),
('ricardo', 99),
('ricardo', 152),
('ricardo', 154),
('ricardo', 170);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id_usuario`,`prod`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


INSERT INTO `user` (`id`, `name`, `password`, `admin`, `media`) VALUES
(37, 'ricardo', '$2y$10$b3JO6/CosJizgSp4TgceeeYcqt5UzCkj0dMI0E0O/WQOcLd9mgSmS', 1, '/productos/media/dropzone/34136mYMKGh - copia.jpg'),
(62, 'joaniu', '$2y$10$nMyPHpBvhQkVA8zTR.PdgeSr/NJRQXQYTETwHQU6wz6UCSYvpqUpu', 0, '/productos/media/dropzone/2151joan.jpg'),
(65, 'joanet', '$2y$10$qyZc59Q7RWXUjEew4WH52.YQB4EbM2/D1sUus2To2RRNQOejhUsPy', 0, '/productos/media/dropzone/32105joan.jpg'),
(66, 'joaquim', '$2y$10$w7mh.VrtUv.IOXRKNh4vbuXQh3cPvTvVtdR2c01mONGfObwFJ5AFG', 0, '/productos/media/dropzone/img_default.jpg'),
(70, 'oscar', '$2y$10$fDYkC6bFlSZdp4o0Jv9Ysum1B7JeyRnbShwLpEoSXoPwOX4qih8WK', 0, '/productos/media/dropzone/32694joan.jpg'),
(71, 'novitet', '$2y$10$FCKY0TVYbLFpG6W1GncmhO2wdSCGb51URLu488Tlmn.k46RpH4ln.', 0, '/productos/media/dropzone/264546mYMKGh - copia.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cmpoinfrmatics`
--
ALTER TABLE `cmpoinfrmatics`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id_comentario`,`id_usuario`,`id_prod`);

--
-- Indices de la tabla `detalle_pedido`
--
ALTER TABLE `detalle_pedido`
  ADD PRIMARY KEY (`id_pedido`,`id_prod`);

--
-- Indices de la tabla `dummies`
--
ALTER TABLE `dummies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id_pedido`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cmpoinfrmatics`
--
ALTER TABLE `cmpoinfrmatics`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;
--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id_comentario` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT de la tabla `dummies`
--
ALTER TABLE `dummies`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
