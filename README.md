Proyecto Componentes Informáticos
=================================

Proyecto entorno web 1º DAW 2018 desarrollado por Joan Montés Doria. Es una aplicación web basada en venta de productos sobre componentes informáticos, inspirada 
con webs como [Pccomponentes](https://www.pccomponentes.com) o [Amazon](https://www.amazon.es) . Lo primero que observamos en el home son nuevos productos que el usuario 
puede comprar, junto a productos que obtiene de la api de ebay (iphones). Posteriormente, en el menú observamos que podemos acceder al login, información y contacto.
Contiene una interfaz y funcionalidad diferente dependiendo del usuario que se registre. Cuando iniciamos la web somos usuarios normales, dónde solo podremos visualizar 
productos. Si nos registramos pasamos a ser clientes, tendremos la funcionalidad de poder ver, comprar y hacer comentarios de productos, 
acceder a nuestro perfil. Habrá un administrador, el cual podrá hacer el CRUD.
En la parte del registro, existe la posibilidad de registrarse con el nombre que tenemos en Facebook, ya que contiene el login de esta red social. La intención que se pretende 
es obtener datos para que el usuario lo tenga más fácil, aunque en esta aplicación solo obtenemos el nombre.

Tecnologías usadas
==================

- Bases de datos: SQL
- Almacenamiento y manipulación de datos: JSON

- Backend:
	1. PHP

- Fronted:
	1. Javascript
	2. Jquery

- Diseño web:
	1. HTML5
	2. CSS3
	3. Bootstrap

Mejoras
=======

Las diferentes mejoras que podemos encontrar son:

1. Los errores 404 y 503 tienen diseño y módulo propio.
2. Los controladores de cada módulo está programado con peticiones ajax, echo en la mayoría de ellos sin que el controlador lance la vista.
3. Los errores que vienen del backend PHP, los pinta jquery.
4. Los formularios no tienen action. Se programa con en el evento click en javascript para obtener valores y ser validados.
5. La gran mayoría de las vistas tienen asociado un controlador javascript para validar o pintar datos, por tanto el controller php solo lanza json.
6. En la parte de los formularios que se utiliza para crear y modificar productos, se sustituye el combobox por jqxDropDownList de jqwidgets.
7. Todos los productos tienen details.
8. Respecto al login, la contraseña se almacena encriptada con password_hash, y se compara con password_verify.
8. En la parte del registro podemos obtener el nombre por facebook.
9. Creación del módulo Perfil, dónde cada usuario podrá cambiar su password, nombre, avatar, eliminar su cuenta y ver la lista de pedidos con sus respectivas validaciones.
	Accedemos a nuestro perfil por un selectmenu de jqueryui, donde también podemos hacer un logout.
10. Incorporación dropzone para subir el avatar.
11. En la parte del carrito, solo puede comprar el cliente y el administrador, un usuario normal tiene que registrarse. Podemos comprar productos de nuestra base de datos, como
	también podemos cambiar las cantidades de los productos si volvemos atrás. Estemos donde estemos, podemos observar la cantidad de productos que llevamos comprados.
12. Implementación método de pago paypal. Cuando se termina el pago, se ingresa a base de datos.
13. Podemos hacer comentarios de todos los productos. Se utiliza un accordion de jqueryui para ver y subir comentarios. Cuando hacemos un comentario y se sube validado, podemos ver
	como un efecto de progreso, un progressbar de jqueryui. Los comentarios se suben dinámicamente, con su respectivo avatar, nombre y comentario del usuario. 
14. Filtraciones de precio, color y disponibilidad en la lista del autocomplete.
15. Utilizo un toaster llamado amaran, como método de alertas para el usuario.
