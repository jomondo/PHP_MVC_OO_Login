//Internacionalizacion json
frases = {
  "en": {
    "Inicio":"Home",
    "Entrar":"Login",
    "CRUD":"CRUD",
    "Ordenar":"Order",
    "Dummies":"Dummies",
    "Porque comprar":"Why buy?",
    "Entrega":"Delivery",
    "Quienes Somos":"About us",
    "Contactar":"Contact",
    "LISTA DE LA SEMANA":"LIST OF THE WEEK",
    "LISTA COMPONENTES DUMMIES":"COMPONENTS DUMMIES LIST",
    "Componentes":"Components",
    "Salir":"Logout",
    "Nombre":"Name",
    "Marca":"Brand",
    "Precio por unidad":"Price by unit",
    "LISTA COMPONENTES":"LIST COMPONENTS",
    "Opciones":"Options",
    "INFORMACIÓN COMPONENTE":"COMPONENT INFORMATION",
    "":"",
    "":"",
    "":"",
    "":"",
    "":"",
    "":"",
    "":"",
    "":"",
    "":"",
    "":"",
    "":"",
      },
  "val": {
    "Inicio":"Inici",
    "Entrar":"Entrar",
    "CRUD":"CRUD",
    "Ordenar":"Ordenar",
    "Dummies":"Dummies",
    "Porque comprar":"Perquè comprar?",
    "Quienes Somos":"Qui som?",
    "Entrega":"Entrega",
    "Contactar":"Contacte",
    "LISTA DE LA SEMANA":"LLISTA DE LA SEMANA",
    "Componentes":"Components",
    "Salir":"Eixida",
    "Nombre":"Nom",
    "Marca":"Marca",
    "Precio por unidad":"Preu per unitat",
    "LISTA COMPONENTES":"LLISTA COMPONENTS",
    "LISTA COMPONENTES DUMMIES":"LLISTA COMPONENTS DUMMIES",
    "Opciones":"Opcions",
    "INFORMACIÓN COMPONENTE":"INFORMACIÓ COMPONENT",
  }
};

function cambiarIdioma(lang) {
  // Guardar preferencia
  lang = lang || sessionStorage.getItem('app-lang') || 'es';
  sessionStorage.setItem('app-lang', lang);

  var elems = document.querySelectorAll('[data-tr]');
  for (var x = 0; x < elems.length; x++) {
    elems[x].innerHTML = frases.hasOwnProperty(lang)
      ? frases[lang][elems[x].dataset.tr]
      : elems[x].dataset.tr;
  }
}

window.onload = function(){
  cambiarIdioma();
  
  document.getElementById('btn-es').addEventListener('click', cambiarIdioma.bind(null, 'es'));

  document.getElementById('btn-en').addEventListener('click', cambiarIdioma.bind(null, 'en'));

  document.getElementById('btn-val').addEventListener('click', cambiarIdioma.bind(null, 'val'));

}